---
layout: page
title: "How to Use This Guide"
date: 2022-06-09
categories: about
lang: en
order: 3
---

## How to Use This Guide

This website is a step-by-step guide to set up a digital security help desk for civil society, with a holistic and intersectional approach. The goal of this resource is to enable smaller organisations and grassroots groups to set up a team to respond to the digital safety needs of the people they work and fight with. It can be read by techies who want to organise and join forces to help their movement, but also by managers and organisers, who can follow the steps outlined in the initial chapters to start planning the creation of a digital security help desk for civil society and then look for people with a technical background to staff their help desk during the implementation phase.

After a brief essay outlining what a digital security help desk for civil society is, the **first chapter** of this guide describes how to decide what your help desk or helpline is going to do, for whom, and which policies will be required to deliver these services.

**Chapter 2** outlines the various steps needed to make a realistic plan for your DSHCS - from getting funded to creating a secure office, up to providing the necessary infrastructure and creating and empowering your team.

**Chapter 3** illustrates the most important service of your team, incident handling, from preparation to detection and analysis, all the way to containment, eradication and recovery as well as post-incident activity, including best practices on the documentation of workflows and procedures.

Finally, **chapter 4** goes beyond the day-to-day operations of your own help desk or helpline and offers recommendations on how and why to collaborate with other digital security help desks and computer emergency teams, and on how to review your work to make sure you deliver excellent service to your beneficiaries.

Besides the guides, we have also included a series of templates you can use to create a framework and the necessary policies for your DSHCS. You can also download the full PDF of the guide to print it out or read it when you're offline.
