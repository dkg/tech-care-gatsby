---
layout: page
title: "Why this guide"
date: 2022-06-09
categories: about
lang: en
order: 1
---

## About

![About TechCare](/about.png)

Groups that offer digital safety support to civil society have multiplied in the last decade, as activists and human rights defenders, journalists, LGBTQIA+ organisations and other minorities under attack have started to see how ICT is becoming a crucial tool for their activities, while also a powerful weapon their adversaries could use to undermine their efforts, threatening them and their allies.

A decade has passed since the first half of the 2010s when some radical technology collectives began to offer technical support to activists and international organisations, launching projects which provided digital security consultancy and training to civil society groups and human rights defenders.

In 10 years those efforts have increased manifold, with digital security help desks being founded by small and large groups alike in every region of the planet and for the most diverse communities: journalists and human rights defenders, international organisations and grassroots movements, LGBTQIA+ groups, women's shelters and so on.

In 2015 these help desks also started networking in a more systematic way with the foundation of CiviCERT. Since then this federation of help desks and infrastructure providers for civil society has grown from the initial 5 founders to over 30 members, including local and international organisations, as well as some individuals who offer digital safety assistance in their country or region.

Still, as digital attacks on civil society and vulnerable minorities increase both in number and intensity, new digital security help desks are being created all over the world - not only for civil society and movements for social and political change, but also to better protect women, LGBTQIA+ people and other groups targeted by ICT-enabled violence and private surveillance.

This is why in the last few years CiviCERT members have received several requests to support the creation of new non-profit help desks. In some cases, it was possible to join forces among established computer emergency response teams and infrastructure providers for civil society and offer guidance, training and infrastructure to the newly-founded help desk. In other cases, when just initial orientation was needed, it became clear that the available documentation - created either for commercial and governmental Computer Security Incident Response Teams that had no familiarity with the context of civil society or for humanitarian hotlines that had no experience with digital security emergencies - did not match the actual needs of a help desk offering digital safety support to civil society groups.

What we lacked was a set of simple instructions that could help navigate the workflows and procedures of a digital security helpline, while focusing on the specific demand of offering a service to groups that are often underfunded, understaffed, non-hierarchical and exposed to disproportionate threats and the consequent risk of post-traumatic stress disorder among their staff or volunteers.

In publishing this guide we aim to fill this gap, enabling smaller organisations and grassroots groups to set up a team to respond to the digital safety needs of the people they work and fight with. It can be read by techies who want to organise and join forces to help their movement, but also by managers and organisers, who can follow the steps outlined in the initial chapters to start planning the creation of a digital security help desk for civil society and then look for people with a technical background to staff their help desk during the implementation phase.

This guide would not have been possible without the existence of CiviCERT and the contributions of many of its members, who shared their documentation, drafted new content, offered interviews and reviewed the final outcome. We would like to thank in particular:

- [Access Now Helpline](https://www.accessnow.org/help/) for the strong support they have offered to the creation of this guide, for generously sharing their public and confidential documentation with the curators and for organising an internal write sprint to offer great advice on how to manage a DSHCS. Many thanks in particular to: Michael Carbone, Maggie Haughey, Mohamed Chennoufi, Rogelio López, Daniel Bedoya Arroyo and Beatrice Martini. Particular acknowledgment goes to Hassen Selmi, Access Now Helpline's Incident Response Coordinator, for his interview which was the basis of the section on the Incident Handling Process (Chapter 3).
- Daniel Ó Cluanaigh, who helped tell the story of the Digital Defenders Partnership's Fieldbuilding project for community capacity building.
- Etienne Maynier, who inspired many sections on information management and communications with beneficiaries through his interview on the publication of restricted information at [Amnesty Tech](https://www.amnesty.org/en/tech/).
- Farhanah, Digital Protection Facilitator at the [Digital Defenders Partnership](https://digitaldefenders.org), for drafting the initial version of the section on procedural documentation and gathering information for other sections.
- Grégoire Pouget and Jean-Marc Bourguignon from [Nothing2Hide](https://nothing2hide.org) for contributing to the sections on secure communications and ticketing systems, and for the enthusiasm with which they jumped into this project as soon as they joined CiviCERT.
- [Luchadoras](https://luchadoras.mx/), for the interview they granted us on their feminist helpline.
- Lu Ortiz, Co-Founder and Executive Director of [Vita Activa](https://vita-activa.org/) for her interview on Vita Activa's staff-caring approach.
- Mario Felaco from [Conexo](https://conexo.org), Jannat Fazal and Shmyla Khan from the [Digital Rights Foundation Pakistan](https://digitalrightsfoundation.pk/), Harlo Holmes from the [Freedom of the Press Foundation](https://freedom.press/), and Carlos Guerra from [Internews](https://internews.org/) for helping envision and coordinate the development of this guide.
- All the feminist helplines who joined the series of webinars ["Building feminist infrastructure: Feminist helplines for people facing gender-based violence in digital spaces"](https://www.digitaldefenders.org/feministhelplines/) organised by the Digital Defenders Partnership in 2021.
- All the organisations, both members and non-members of CiviCERT, that filled in the long questionnaire we drafted to gather use cases for this guide.

We hope this guide will support the creation of digital security help desks for civil society all over the world. Please send feedback and suggestions for improvement to: tech-care@digitaldefenders.org. You can also suggest improvements by creating an issue or submit a merge request on this Gitlab repository: [https://gitlab.com/rarenet/tech-care-gatsby](https://gitlab.com/rarenet/tech-care-gatsby).
