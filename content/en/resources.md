---
layout: page
title: "Additional resources"
date: 2022-10-13
categories: about
lang: en
order: 4
---

# Additional resources

## Templates

- [Code of Practice](/06-code-of-practice-template)
- [Framework](/06-framework-template)
- [Incident Response Plan](/06-incident-response-plan-template)
- [Information Management Policy](/06-information-management-policy-template)
- [Vetting Policy](/06-vetting-process-template)

## Download TechCare

- [Download this guide as a PDF](/TechCare_Guide_en.pdf)

## Contribute to this guide

- [Gitlab repository to create issues](https://gitlab.com/rarenet/tech-care-g)
- Email us at tech-care@digitaldefenders.org
