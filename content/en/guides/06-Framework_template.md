---
layout: post
title:  "Framework template"
date:   2022-06-09
categories: templates
lang: en
---

# Organisation name
# Framework template

## 1. Our Constituency

- Who are the beneficiaries of your DSHCS? Detail activity, field, gender, age, etc. The more accurately your constituency is defined the better you will be able to identify their needs and increase the quality of the service provided.
    - 
- What is the geographical scope of your constituency?
    - 
- Are there other DSHCSs providing services to the same constituency in your region? In such case what unattended needs will your DSHCS cover?
    - 

## 2. Our Constituency's Needs

If you decide, e.g., to support indigenous communities from a certain region, you could make a SWOT analysis to characterise them. For example:

||Helpful | Harmful |
| --- | --- | --- |
| **Internal** | ***Strengths*** <br> _They are very well organised and have access to a wide range of resources._ | ***Weaknesses*** <br> _Vertical structure centralises decision making._  |
| **External** | ***Opportunities*** <br> _Many International Cooperation Agencies are allocating funds to indigenous communities._ | ***Threats*** <br> _Increasing criminalisation of indigenous communities._ |

Now try using the same table for your beneficiaries:

||Helpful | Harmful |
| --- | --- | --- |
| **Internal** | ***Strengths*** <br> _Describe your constituency's strengths_ | ***Weaknesses*** <br> _Describe your constituency's weaknesses_  |
| **External** | ***Opportunities*** <br> _Describe your constituency's opportunities_ | ***Threats*** <br> _Describe the threats affecting your constituency_ |

You can complement the SWOT framework with a PESTLE analysis of the context in which your constituency operates.

| Constituency | Political | Economical | Socio-Cultural | Technological | Legal | Environmental |
| --- | --- | --- | --- | --- | --- | --- |
| _Indigenous communities from X region_ | ... | ... | ... | ... | ... | ... |

### Our Threat Model

#### Threat Matrix

| Likelihood / Impact | Low | Medium | High |
| --- |  --- | --- | --- |
| **Likely** | ... | ... | ... |
| **Less Likely** | ... | ... | ... |
| **Unlikely** | ... | ... | ... |

#### Threat Inventory

Fill in a table for each threat identified in the threat matrix.

| Title |   |
| --- | --- |
| **Description**| _Brief characterisation of the threat._|

<br />

| What | Target | Adversary | How | Where |
| --- | --- | --- | --- | --- |  
| _The impacts the threat would cause_ | _What or who is the target_ | _Who do you think is behind the threat?_ | _The means by which the threat can come to fruition_ | _What are the physical spaces the threat can manifest?_ |
| ... | ... | ... | ... | ... |

## 3. Our Mission

What are your helpline's goals? Your mission statement should define your constituency, the situation you want to help overcome and how you plan to do it, as well as what services your helpline will provide.

<br />

| Organisation Name - Mission |
|-----------------------------|
| *Describe your mission here:* <br /><br /> .........<br /> .........<br /> ......... <br /> ......... |



## 4. Setting

- Will you be part of a larger organisation or constitute an independent project?
    - 
- Will you be volunteer-based or hire a team?
    - 
- How will your DSHCS will be funded?
    - 

## 5. Core Services

| Service type | Service | Requirements |
| --- | --- | ---- |
| _Reactive_ | _Equipment replacement_ | _Access to funding. Maybe we can refurbish old equipment for emergencies._ |
| _Preventative_ | _In-Person Digital Security Training_ | _Need to find trainers in the area where the trainings will happen_ |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |

## 6. Communication with your Constituency

### Decide How Your Constituency Can Get in Touch

| Channel | Advantages | Disadvantages | Accessibility for our constituency | Are we going to use it? |
| --- | --- | --- | --- | --- |
| _Form in website_ | _Easy to install. Can be encrypted_ | _Risk of spam_ | _Accessible_ | _Yes_ |
| _Telephone_ | _Everybody can access a phone to call us_ | _SIM cards need to be registered with an ID._ | Accessible | No |
| ... | ... | ... | ... | ... |
| ... | ... | ... | ... | ... |
| ... | ... | ... | ... | ... |

### Declare Your Availability and Response Time

- Operation hours:
    - 
- How will support requests outside of operational hours be attended to?
    - 
- How will the DSHCS prevent burnout of team members on call?
    - 

### Decide How to Communicate with Your Constituency

- Will operators have an individual pseudonym or will they use a collective one?
    - 
- Does one operator always lead the communication with the person involved in a case? And if it is shared, is the conversation always conducted under the same pseudonym or does it change with each operator?
    - 
- Will the DSHCS adopt an informal tone or will operators keep their distance?
    - 

## 7. Policies

| Policy | Description | Development and implementation | Responsible | Due date |
| --- |  --- |  --- |  --- |  --- |
| _1. Information Management Policy_ | _Procedures on how to manage and protect information._ |  _Yes_ |  ... | ... |
| _2. Incident Response Plan_ | _Roadmap for implementing the DSHCS' incident response capability._  |  _Yes_ |  ... | ... |
| _3. Vetting Policy_ |  _Steps to verify new beneficiaries._ |  Yes |  ... | ... |
| _4. Code of Practice_ |  _Description of what is expected of the operators' behaviour._ |  _Yes, but in a second stage_ |  ... | ... |
| _5. Standard Operating Procedures_ |  _Steps for responding to requests, making referrals, etc._ | _Yes_ |  ... | ... |
| _6. Funding Policy_ |  ... |  ... |  ... | ... |
| _7. ..._ |  ... |  ... |  ... | ... |
