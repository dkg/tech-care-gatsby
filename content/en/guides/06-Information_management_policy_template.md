---
layout: post
title:  "Information Management Policy Template"
date:   2022-06-09
categories: templates
lang: en
---

# Information Management Policy Template

## [Organisation's Name]
## Information Management Policy

### 1. Information Classification

[Org Name] supports the Information Sharing Traffic Light Protocol.

Data classification description

- **PUBLIC** – This information is deemed to be non-sensitive (meaning that it excludes personal data and details on collaborations with third parties and internal procedures) and can be distributed to anyone in any context. The information is intended for public consumption. It may have already been reported on, and/or is available to be reported on.
- **CONFIDENTIAL** – Information marked as confidential can be shared to other [Org Name] teams, as well as to trusted third parties on a need-to-know basis, i.e. only if a specific case can only be addressed by sharing information with them. By default the information is only shared among [Org Name] staff members. There should be no assumption about sharing the information to third parties, and this kind of information should only be shared on a need-to-know basis with third parties that have signed a non-disclosure agreement which includes minimum standards around data storage and retention that aligns with [Org Name]'s Retention Policy (see below).
- **RESTRICTED: [group / entity / list of individuals]** – Any information classified as restricted must also include a group, entity, or list of individuals that the information is to be restricted to. When the information is restricted to groups or entities, any individual’s membership in that group or entity means that that individual has access to the information. Restricted information can also be shared with third parties on a need-to-know basis. So for “RESTRICTED: [Org Name] tech team” these other parties may be the beneficiary, as well as another third party that needs to be involved to solve a case. Some information is so sensitive that it should only be shared with individuals on a must-need-to-know basis. In that case, this information will be labelled as “RESTRICTED: [list of individuals]”. This information is never shared with groups, so group membership never grants access to this classification of information. When emailed, only the named recipients of the email are considered the “need-to-know” individuals, and the information must not be shared beyond those named recipients.

| Color	| Classification	| Scope	| Examples |
|-------|----------------|--------|----------|
| RED	| RESTRICTED [list of individuals]	| Named individuals	| <ul><li>legal requests</li><li>trust issues</li></ul> |
| AMBER	| RESTRICTED [entity]	| Membership of [entity] and need-to-know 3rd parties |	<ul><li>requests by beneficiaries</li><li>case history data (which may include personal data necessary for the delivery of services)</li><li>personal contacts (if necessary and justified by a request from members of entity and third-parties)</li><li>documentation on internal infrastructure</li><li>documentation on internal procedures</li></ul> |
| GREEN	| CONFIDENTIAL	| [Org Name ] members; need-to-know 3rd parties | <ul><li>civil society threat intelligence</li><li>documentation on escalations and procedures</li></ul> |
| WHITE	| PUBLIC	| All	| <ul><li>general security recommendations</li><li>blog posts</li><li>website content</li><li>social media posts |

### 2. Information Protection

- **Public information** can be widely distributed and takes the form of newsletters, website content, social media posts, information on public malware information sharing platforms, etc. [Org Name]’s infrastructure hosting public information is hardened and protected against the compromise of the integrity of such information. All public data is backed up against loss of such information.
- **Confidential information** is stored in platforms that are only accessible by [Org Name] staff and can be shared with third parties on a need-to-know basis. Access to these platforms is password-protected and 2-factor authentication is required whenever possible. This kind of information is also stored in work devices with full-disk encryption. This kind of information is only transferred through end-to-end encrypted channels of communication.
- **Restricted information**
    - **Information restricted to single [Org Name] teams** is only stored on password-protected platforms and in work devices with full-disk encryption. This kind of information is only transferred through end-to-end encrypted channels of communication.
    - **Information restricted to individuals** is only stored on password-protected platforms and is only transferred through end-to-end encrypted channels of communication.

Individual staff members’ GPG private keys are only stored in their devices laptops with full-disk encryption or in fully encrypted external storage media.

The security measures put in place for the protection of information are minimum standards and, as they are also evolving, may change in the future.

### 3. Information dissemination

Information that may not be shared:

- Incoming information restricted to specific individuals will not be shared beyond those named recipients.

Limitations to this policy:

- Information may be shared in compliance with national and international legal obligations, including in response to requests from law enforcement that compel [Org Name] to produce records. [Org Name] will vigorously challenge legal orders or other requests that infringe on human rights and will exercise whatever power we have to protect our beneficiaries, partners and staff.

### 4. Access to Information

Legal requests by law enforcement authorities, communications on trust issues and other critical information may be labelled as RESTRICTED to single individuals and disclosed only on a need-to-know basis, until it becomes possible to lower the confidentiality level of this information.

Changing the classification of data

Information classified as CONFIDENTIAL can become public only after removing all sensitive information or under explicit authorisation of the individuals and groups mentioned in the information. If information is classified as confidential to grant exclusive publication rights, it will automatically become public as soon as it has been published.

### 5. Cooperation with other teams

This policy defines the process followed by [Org Name] to engage in formal or informal cooperation with other CERTs and digital security response teams for civil society.

- [Org Name] can share information classified as “PUBLIC” in any forum where any CERT or digital security response team for civil society have access.
- If [Org Name] determines the best action on behalf of a beneficiary is to engage with a specific other CERT or digital security response team for civil society, those communications and associated data should be considered “CONFIDENTIAL”, or “RESTRICTED: [list of entities]” (see “Information Classification” above).
- In such cases, [Org Name] will secure the permission of the beneficiary in order to pursue resolution of their case through the services provided by another CERT or digital security response team for civil society.
    - In some instances [Org name ] will not secure the permission of the beneficiary as that permission can be assumed, for example in cases like the following:
        - If the beneficiary has instructed the CERT or digital security response team for civil society to pursue the takedown of a phishing content host (note that before taking such cases towards takedown resolution, our processes relating to threat intelligence coordination, and pursuing the most beneficial actions for the largest number of interested parties in such circumstances, should be followed first);
        - Account recovery or deactivation (assuming that vetting of the beneficiary has occurred) with the CERT of the platform involved;
        - A shutdown complaint, if that shutdown has affected the general public.
- Circumstances where we absolutely require a permission from the beneficiary include:
    - C&C server takedown;
    - Malware analysis, particularly where a device is to be examined;
    - Censorship resolution.

### 6. Record Retentions

All information shared within [Org Name] is stored in [Org Name]'s own servers, for which the following policy applies:

#### Retention Policy

All information in [Org Name]’s infrastructure, including threat information, requests from beneficiaries and partners and internal documentation, is stored for as long as necessary in [Org Name]’s servers, for the purpose of information sharing and delivery of services and for compliance with national and international legal obligations, including the prevention of criminal offences and the enforcement of civil law claims.

All information in [Org Name]’s infrastructure, except for public information which is non-sensitive and does not include any personal data, is stored on password-protected platforms and in work devices with full-disk encryption. This kind of information is only transferred through end-to-end encrypted channels of communication. These are the minimum standards in place and, as they are also evolving, may change in the future.

#### Data Breach Policy

In case of a personal data breach which is likely to result in a risk to the rights and freedoms of the data subjects, [Org Name] shall notify the data subjects without undue delay and also notify the supervisory competent authority, without undue delay and where feasible, no later than 72 hours after having become aware of the breach, in accordance with the EU’s General Data Protection Regulation.

In addressing data security breaches, [Org Name] shall take measures to mitigate damage, investigate, conduct remedial action and comply with regulatory requirements for information security.

### 7. Data destruction process

#### Physical documents

Printing should be limited to a minimum. Printed documents should be stored only as long as needed and it is recommended not to cross borders with confidential or restricted printed papers.

Physical paper documents containing CONFIDENTIAL information should be destroyed in a ribbon or cross-cut shredder, while any other physical documents containing RESTRICTED information must be shredded with a cross-cut shredder.

#### Storage devices

Hard drives, USB thumb drives, and other portable storage devices containing CONFIDENTIAL or RESTRICTED information should be securely erased with a single pass of clearing process: random data (/dev/urandom) before they are thrown out. Write-once CDs should be broken into pieces or destroyed in the shredder before being thrown out.

#### Digital data

Digital files containing CONFIDENTIAL information can be simply deleted, while for RESTRICTED data a minimum of one clearing process must be done over the file.

### 8. Appropriate usage of work devices

All [Org Name] members make sure that the devices they access [Org Name] information with are protected with full-disk encryption and used according to sound judgement, with regular updates of the system and software and other measures to prevent infection or unauthorised access to the system and accounts.

### 9. [Org Name]’s Communications and PGP Policy

“RESTRICTED” information should only be shared with other [Org Name] staff members over strongly encrypted channels, such as PGP-encrypted email, Signal, or similar.

[Org Name] supports PGP-encrypted communications and communicates over an encrypted channel.

It is mandatory for all [Org Name] staff members to use PGP/GnuPG for encrypting every email communications with:

- other team members
- third parties, when exchanging confidential and restricted information

It is recommended that PGP key pairs used for communicating with [Org Name] team members and third parties have the following settings:

- Algorithm: RSA
- Key length: 4096
- Expiration Date: 5 years
- Private key protected by a strong password, consisting of at least 20 characters, including lower- and upper-case letters, numbers and symbols, or a passphrase created with the diceware method, with at least 6 words

Should a device containing a PGP private key be stolen, or should a PGP key pair be otherwise compromised, the key pair will be revoked as soon as possible and [Org Name] management will be notified.
