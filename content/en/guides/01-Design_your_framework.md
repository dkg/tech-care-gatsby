---
layout: post
title: "Design Your Framework"
date: 2022-06-09
categories: guide
lang: en
order: 1
---

# 1. Design Your Framework

![collage suculent](/chap_1.png)

By creating a framework for your Digital Security Help Desk for Civil Society (DSHCS), you will describe in detail what your help desk or helpline will do, for whom, in which setting and in cooperation with whom. This exercise will also help you understand which resources you will need to assist your beneficiaries. You should invest enough time in designing your framework as it will lay the foundations of your help desk.

This chapter will go over how to identify your constituency, analyse your beneficiaries' needs, define your mission and setting, establish your core services, set your communication parameters and specify your policies. Digital Security Help Desks for Civil Society can be very different from each other, so each one will have a different framework but the elements described in this chapter will apply to every project.

You can download a [framework template](/06-framework-template) to help you think through this stage.

## 1.1 Define Your Constituency

This guide will use the term "constituency" to define the whole set of beneficiaries served by a DSHCS. At the same time, a single individual or organisation will be called a "beneficiary" or "constituent".

When starting up a DSHCS it is essential to have a clear view of who your beneficiaries are and what kind of environment your services will be developed for. Your decision on whom to serve or not to serve may be based on your organisation's mission, funders' policies, legal considerations, or even a changing political scenario that puts a specific group of people at risk.

In general terms a DSHCS's constituency is supposed to be civil society or a part of it. Still, given the broad and varied definition of "civil society" any single group can have, it may be helpful to offer a list of the kinds of beneficiaries currently being served by the DSHCSs that are members of CiviCERT:

- Activists
- Non-governmental organisations
- Human rights defenders
- Human rights organisations at risk
- Independent media
- Indigenous organisations
- Journalists
- Land defenders
- LGBTQIA+ groups
- Women
- Youth

> We work with women all over the country. There is a problem of access, which is basically a class and age problem. In general, younger and middle-class women don't write to us much because they manage to solve their own problems of gender violence enabled by ICT. Then, there are older adults, children and adolescents from rural areas who are just starting their relationship with ICT. We also support people with a public profile that are constantly being assaulted. We need specific strategies for them as well. So, there is a difference between the way we are working and confronting violence with these groups.<br><br>_SOS Digital (Haché, 2021)._

## 1.2 Analyse Your Constituency's Needs

Identifying and evaluating the needs and expectations of your constituency and the context they operate in will be essential for the success of your project. You should talk with your constituency about the real value your DSHCS could bring, analysing the threats your beneficiaries could face and their needs in response to digital security incidents and the prevention of threats. This could be done in-person or remotely, in groups or in one-on-one interviews, both publicly or privately.

There are a number of frameworks that can be used for this kind of analysis. The two most commonly used are:

- SWOT: to identify the strengths, weaknesses, opportunities and threats.
- PESTLE: to incorporate political, economic, social, technological, legal and environmental dimensions in the contextual analysis.

After analysing your constituency's needs, a good practice is to carry out a threat modelling activity to characterise the political context you will be operating in, identify the help desk's vulnerabilities and threats, together with their likelihood, and specify the requirements for prevention and mitigation. This threat model must be taken into consideration when developing your help desk policies, technical procedures, documentation and training staff.

You will find some keys to conducting these analyses in the [framework template](/06-framework-template).

### Learn more

- Higson Smith, Craig, Ó Cluanaigh, Daniel, Ravi, Ali G., Steudtner, Peter (2016). Overall Framework for Context Analysis. _Holistic Security - A Strategy Manual for Human Rights Defenders_. Tactical Technology Collective. [https://holistic-security.tacticaltech.org/chapters/explore/2-1-overall-framework-for-context-analysis.html](https://holistic-security.tacticaltech.org/chapters/explore/2-1-overall-framework-for-context-analysis.html).
- Front Line Defenders (2011). Understanding Your Context. _Workbook on Security: Practical Steps for Human Rights Defenders at Risk_, pp. 61-7.
- Schulte, Jennifer (2018). Gender-Based Risk Model. _Cyberwomen: Holistic Digital Security Training Curriculum for Women Human Rights Defenders_. Institute For War And Peace Reporting. [https://iwpr.net/global-voices/print-publications/cyberwomen-holistic-digital-security-training-curriculum-women](https://iwpr.net/global-voices/print-publications/cyberwomen-holistic-digital-security-training-curriculum-women).

> We were producing knowledge about how women were experiencing violence in Mexico to identify it and characterize it. As part of this job, we worked to make this violence visible in the public sphere. Because of this, we became a reference, and support requests quickly began to arrive. The possibility of having a space to support women facing this violence then came up. So in March 2020, we decided to make the arrangements to accompany women asking for support and information.<br><br>_Luchadoras (Haché, 2021)._

## 1.3 Define Your Mission

After analysing your constituency's needs and context, your next planning step should be the drafting of a mission statement. By defining your mission you will clearly explain the purpose and function of your DSHCS and provide a brief overview of the core goals and objectives of your project.

As this will be the foundation of your work for some years it is good practice to avoid ambiguity by making the mission statement compact but not too short.

Here are two examples of mission statements by CiviCERT members:

**Access Now Digital Security Helpline**

Access Now defends and extends the digital rights of users at risk around the world. By combining direct technical support, comprehensive policy engagement, global advocacy, grassroots grantmaking, and convenings such as RightsCon, we fight for human rights in the digital age.

Access Now’s Digital Security Helpline provides technology solutions and real-time advice for users at risk in circumstances where communications are not open, free, or safe. Through our 24/7/365 Digital Security Helpline, we offer technical guidance and incident response to inform and support activists, journalists, human rights defenders, and civil society actors on the ground.

The core goals and objectives of Access Now Digital Security Helpline are:

- To provide direct technical support to users and organisations at risk so as to identify and address their digital security needs.
- To provide assistance to users and organisations at risk to secure their digital assets, communications and other activities online, and to help them circumvent censorship and obtain access to the services they need.
- To provide expertise, support and coordination to prevent and contain malware infections and to address vulnerabilities in systems and software.
- To update our constituency about any newly-emerged threats and vulnerabilities that need to be urgently addressed.
- To coordinate support to users at risk in case this support can be better provided by other CERTs.
- To become a recognised centre of information security excellence for national and international organisations to refer to.

**Mnemonic**

Mnemonic works globally to help human rights defenders effectively use digital documentation of human rights violations and international crimes to support advocacy, justice and accountability.

We aim to:

- Archive digital information to ensure that potential evidence is not lost and remains accessible and usable for future accountability mechanisms.
- Train human rights defenders to maximise the impact of digital information and empower those working with it.
- Reduce the impact of harmful content moderation policies by social media companies and governments by providing comprehensive, reliable data surrounding the takedowns of human rights documentation on social media platforms.
- Build and support the development of open source tools and methods to increase human rights defenders’ capacity to use digital information to advance social justice.

> We challenge and disrupt the surveillance industry and the targeting of activists through digital security support, research, advocacy and campaigning. At Amnesty, each team has to define the problem they want to tackle and create a strategy of change. So we ask ourselves: "How do we change this?". This question leads to many discussions, so we focus more on research. While Access Now and other organisations provide digital support to a broad public, we do so mainly for people who are or have been targeted by surveillance. We both conduct research and digital security support.<br><br>_Etienne Maynier, Amnesty Tech (Interview, December 2021)._

## 1.4 Define Your Setting

When you plan for the creation of a DSHCS, you will need to make decisions on your organisational structure: will you be part of a larger organisation, or will you be independently run? Will you need to fundraise on your own, or will a fundraising department provide you with resources?

Your DSHCS may adopt different organisational structures. Many DSHCSs are projects of larger non-profit organisations or work within Internet Service Providers for civil society. Others are independent and some are volunteer-based. It is also possible for multiple incident handling capabilities to exist within a single parent organisation, e.g. with one department serving the organisation's staff and addressing incidents on its infrastructure, while another serves an external constituency. Some national CERTs can include among their services handling digital security incidents that affect non-profit organisations.

## 1.5 Define Your Core Services

A DSHCS can offer many different services but as long as you provide some kind of digital security incident response you don't need to do everything and can focus on a small set of core services, for example, focusing on the security of social media accounts or on giving recommendations on how to circumvent censorship in a specific area.

Many DSHCSs provide both reactive services - i.e. response to digital security incidents - and preventative services - i.e. digital security education efforts to reduce the risk of incidents - but in most cases they will limit their list of services based on their capacity and will decide to refer to other teams for additional services.

What follows is a list of both reactive and preventative services offered by CiviCERT members, including services that are outside of the scope of digital security:

- **Reactive**
  - Initial triage
  - 24/7 digital support
  - Equipment replacement
  - Handling vulnerabilities and malware
  - Handling account hacking
  - Online harassment mitigation
  - Forensic analysis
  - Censorship circumvention
- **Preventative**
  - In-person training
  - Organisational security consultancies
  - Secure website hosting
  - Website protection
  - Denial of service protection
  - Assessing threats and risks
  - Securing communications
  - Device security
  - Web browsing security
  - Account security
- **Not strictly related to digital security**
  - Grants and funding
  - Relocation of individuals at risk
  - Physical security
  - Legal support
  - Psychosocial support
  - Public advocacy

> The helpline acts as a referral. We refer our cases to social media platforms when needed, as we have direct communication with Facebook and their Not Without My Consent pilot. We also refer to the Internet Watch Foundation's online pilot for online child pornography and escalate removal petitions to get harmful content taken down. Sometimes we support complainants filing a complaint with law enforcement in Pakistan. Or we link women and young girls wanting to get out of toxic families or abusive relationships with shelters in Pakistan. All of this goes beyond the digital security services and the legal assistance that the helpline provides.<br><br>_Digital Rights Foundation (Haché, 2021)._

> We are operating a helpline to attend to women who are experiencing digital violence in Mexico. This initiative came about from an increase in the requests for support on our social media outlets. The main services we offer are: providing comprehensive accompaniment; psychological first aid; detecting needs; providing information such as alternatives for action, forms of reporting, content on digital violence, digital security, or contacts of possible support networks; escalation of cases through reports on various platforms; channelling to specialized organisations and institutions for optimal support and monitoring.<br><br>_Luchadoras (Haché, 2021)._

## 1.6 Communicating with your Constituency

Defining the ways in which the DSHCS will establish communication with its constituency is a crucial element of this framework. You will have to decide how your constituency will get in touch, your availability and response time, and be confident about how to communicate with people who might be emotionally traumatised.

### Decide How Your Constituency Can Get in Touch

You will need to identify the best communication channels for your beneficiaries to get support from the helpline or help desk.

Among the things you will need to consider, based on the initial context analysis and threat model, is whether your beneficiaries may need end-to-end encryption or an anonymous intake mechanism to start with. If this is the case, think about all the possible methods to exchange any sensitive information through a secure channel.

We recommend setting up at least two different ways to reach out to your help desk:

- A channel accessible to everyone without any technical knowledge, such as an email address.
- A secure channel for people who have the necessary technical knowledge to use it, such as an encrypted email or a secure messaging app such as [Signal](https://signal.org/) or [Wire](https://wire.com/).

Ensuring the safety of communications is important. However, your priority is to ensure that your DSHCS is easily reachable. For this reason, it is important to offer several communication channels for your beneficiaries. Multiplying the communication channels is not necessarily a problem as long as your team is organised enough to share information.

Here is a list of all the communications tools offered by CiviCERT members to their beneficiaries as possible methods to establish first contact with their DSHCS:

- Anonymous web form
- Email
- PGP-encrypted email
- Phone
- Snail mail
- Signal
- Skype
- Telegram
- Web form
- WhatsApp
- Wire

> We have an email, a telephone line and WhatsApp. People also contact us through our social media and our website contact form. Also, through referrals from other organisations that receive cases and ask us for support.<br><br>_Luchadoras (Haché, 2021)._

Also consider that some of your beneficiaries might have gone through traumatic events and may need active listening and empathy, which is best achieved through a phone or video call.

There are also plenty of choices to maintain the relationship with beneficiaries and receive feedback, including:

- Forums
- Mailing lists and other community spaces
- Meetings, conferences, workshops, presentations (in-person and remote)
- Newsletters
- Social media

### Declare Your Availability and Response Time

You should clearly communicate your response times to your beneficiaries to avoid false expectations and to establish an adequate Service Level Agreement (SLA) with your constituency. Giving timely feedback to beneficiaries during incident handling is crucial, both for addressing the issue they are facing and for your DSHCS's reputation.

The availability and response time of your DSHCS will largely depend on the number of your staff members and on their working hours.

Unless your DSHCS is available 24/7, you will need to decide how incidents can be reported outside of office hours. You could just choose to go through all incoming messages on the next working day, or you could have a team member on call to monitor incoming requests and decide on their urgency.

It is important to consider your context when making this decision: for example, if you have limited funding you may not be able to pay an operator for working outside standard working hours. These are important considerations for the psychosocial security of your staff as well: if, for example, your DSHCS is operated by volunteers, they may be willing to accept requests at any hour of the day or night, but this could quickly lead to a burnout of your most dedicated operators.

### Learn more

- For a deeper insight on how to care for the DSHCS team's well-being, read the section on Staff Welfare Policies in [Chapter 2](/02-make-a-realistic-plan).

### Define the Tone and Protocols for the Conversation

Besides ensuring that communications with beneficiaries are confidential, when responding to support requests DSHCSs should always keep in mind that their beneficiaries can be emotionally traumatised by the attack they've been exposed to.

Here are some tips on how to establish communication with a person who has faced an attack:

- Record the communication, check if similar communications have already been recorded.
- Use a sensitive approach, with an intersectional perspective.
- Put the requester at the centre, defusing guilt, embracing their feelings.
- Avoid re-victimisation.
- Accept and agree with the decision they make in the situation in which they are living.
- Take into account the elements of diverse contexts. Open your own eyes to cases that may come to you, without judging.

> In terms of communication, the main thing for us is to put their needs at the centre, seeking to deactivate guilt and embracing their feelings, avoiding re-victimisation. Communication from Luchadoras is designed to make them feel as if they were a friend who responds, accepting and agreeing with the decision they make about the situation they are going through.<br><br>_Luchadoras (Interview, January 2022)._

> We brought the concepts of emotional and empathic support into our helpline. Our psychological first aid is based on three questions: "How are you doing with this stress?" to share our deep concern for their well-being; "Can you explain the problem to me?" to let them control their narrative; and "What do you want us to do for you? What is your desire?" to build solutions along with the person looking for support.<br><br>_Vita Activa (Haché, 2021)._

### Learn more

- [Access Now's protocol when an incident handler feels they're dealing with a paranoid requestor](https://communitydocs.accessnow.org/356-paranoia_protocol.html)

## 1.7 Define Your Policies

The policies of a DSHCS are a set of agreements and guidelines that organise its workflow and establish its standard procedures and protocols. Each DSHCS will need policies that meet its unique requirements linked to the group’s mission, size, structure and services. In this section you will find a description of the basic policies DSHCSs have adopted, together with templates you can use to set up your DSHCS:

- Information Management Policy
- Incident Response Plan
- Vetting Policy
- Code of Practice
- Standard Operating Procedures (SOPs) for DSHCS Operators

After they have been developed and implemented, policies should be reviewed regularly to make sure they still apply to the DSHCS's structure, procedures, needs and capabilities over time.

<a name="information-management"></a>

### Information Management Policy

Every DSHCS needs a policy on how to manage and protect information that takes into account internal operational and administrative processes and procedures, as well as legislation and standards. It is therefore best to involve a legal consultant in the development of your information management policy.

The most basic questions to be answered in your information management policy are:

1. **How is information "tagged" or "classified"?**

   Most DSHCSs, as well as CiviCERT, classify information based on the [Information Sharing Traffic Light Protocol (TLP)](https://www.first.org/tlp/):

   - **TLP:RED** = Not for disclosure, restricted to participants only.

     Sources may use TLP:RED when information cannot be effectively acted upon by additional parties, and could lead to impacts on a party's privacy, reputation, or operations if misused. Recipients may not share TLP:RED information with any parties outside of the specific exchange, meeting, or conversation in which it was originally disclosed. In the context of a meeting, for example, TLP:RED information is limited to those present at the meeting. In most circumstances, TLP:RED should be exchanged verbally or in person.

   - **TLP:AMBER** = Limited disclosure, restricted to participants’ organisations.

     Sources may use TLP:AMBER when information requires support to be effectively acted upon, yet carries risks to privacy, reputation, or operations if shared outside of the organisations involved. Recipients may only share TLP:AMBER information with members of their own organisation, and with beneficiaries who need to know the information to protect themselves or prevent further harm. Sources are at liberty to specify additional intended limits of the sharing: these must be adhered to.

   - **TLP:GREEN** = Limited disclosure, restricted to the community.

     Sources may use TLP:GREEN when information is useful for the awareness of all participating organisations as well as with peers within the broader community or sector. Recipients may share TLP:GREEN information with peers and partner organisations within their sector or community, but not via publicly accessible channels. Information in this category can be circulated widely within a particular community. TLP:GREEN information may not be released outside of the community.

   - **TLP:WHITE** = Disclosure is not limited.

     Sources may use TLP:WHITE when information carries minimal or no foreseeable risk of misuse, in accordance with applicable rules and procedures for public release. Subject to standard copyright rules, TLP:WHITE information may be distributed without restriction.

2. **How is information handled and secured?**

   You should define how you protect the information you store in your infrastructure and your communications, as well as for how long you keep information stored in your infrastructure and what happens in case of a data breach.

3. **What considerations are adopted for the disclosure of information, especially if incident-related information is passed on to other teams or requested by law enforcement authorities?**

4. **Are there legal considerations to take into account with regard to information handling?**

5. **Does your policy define how your technical infrastructure and equipment should be secured and used?**

> Amnesty has a strict consent policy, so we cannot publish anything without [the beneficiary's] consent, even anonymously. So we have to go through a strict policy, going through the risks with them and so on. And at this point - if the case is reactive and it's not coming from a project we'd started before - we start to think about the change strategy: What do we know? What can we prove? What can we do to change this? So generally we have a discussion where we figure out what would be the advocacy plan, if there's any angle for campaigning, if there's any work that should be done with the country team, what would be the risk of publishing, what would be the benefit of publishing.<br><br>_Etienne Maynier, Amnesty Tech (Interview, December 2021)._

> We try to publish as much data as possible: indicators, methodology, etc. For instance, with Pegasus, we also published detailed forensic traces. The more we publish, the more other people can build their own research on, but also the less we are pressured into giving more data, from governments, for example. We are then publishing detailed technical reports, so if a government comes to us, we can say: "Hey, everything is already public". That's one of Amnesty's policies: publishing evidence so that we're less prone to be asked to provide private information.<br><br> _Etienne Maynier, Amnesty Tech (Interview, December 2021)._

> It is important for us that we have certain security protocols in place. One essential protocol is that we have everyone who works on the helpline maintain a non-disclosure agreement so that confidentiality and privacy are ensured, and that any data we keep is not personally identifiable. In this way, nobody can understand that data except the one who is handling or storing it.<br><br>_Digital Rights Foundation (Haché, 2021)._

[Download the Information Policy Management template](/06-information-management-policy-template).

<a name="incident-response"></a>

### Incident Response Plan

The incident management procedures described in your incident response plan are among the key measures to be put in place as they will help everyone understand what is expected of them. Describe types of incidents by distinguishing between levels of impact and establish which steps your team should follow. Define which team members should be approached if an incident arises. List the required options for scaling up or escalating matters and arrange this with your team members. In other words, ensure expectations are managed appropriately within the organisation.

In order to have a documented and coordinated approach to responding to digital security issues, your incident response plan should include the following elements:

- How cases are received and assigned
- How cases are prioritised
- the DSHCS's intake, vetting and escalation workflow
- the DSHCS's incident response lifecycle
- How cases are closed

[Download the Incident Response Plan template](/06-incident-response_plan-template).

<a name="vetting"></a>

### Vetting Policy

Making sure that a DSHCS supports its real constituency is key to its reputation, so many helplines and help desks verify that requesters are really who they say they are before they start the incident handling process.

To define this verification process, it is a good practice to develop a vetting policy which describes the goals of the process, as well as the steps that operators need to take to verify the new beneficiary, get their informed consent on the process and protect their privacy as much as possible.

> So basically we start from people coming to us for a reason. Often they come to us because they have received a weird sms or email, something like that. We then try to gather information about the context, investigate the email or the sms they've received. But first we need to make sure that they are human rights defenders, and that is quite challenging because Amnesty can have a narrow definition of what's a human rights defender sometimes, and that depends on the country team. So Amnesty International works with 2 streams: country teams and topic teams - so, like in my team: we're working on technology, but every time we work in a country we need to work with the country team. When we receive a request we have to check who the requester is and if they are a human rights defender, which is challenging because we sometimes need to ask people who they are, then we need to check with the country team whether they are human rights defenders and make sure we have approval. And then if this is the case - often with journalists, for instance, it's quite easy - we do the investigation, we try to understand what happened.<br><br>_Etienne Maynier, Amnesty Tech (Interview, December 2021)._

Download the [Vetting Policy template](/06-vetting-process-template).

<a name="cop"></a>

### Code of Practice

A Code of Practice is a document that sets out expectations for DSHCS operators with regard to how they will behave toward beneficiaries. Important elements of an effective Code of Practice include:

- Specific descriptions of common but unacceptable behaviour (sexist comments, etc.).
- Reporting instructions with contact information.
- Information about how it may be enforced.
- A clear demarcation between unacceptable behaviour (which may be reported per the reporting instructions and may have severe consequences for the perpetrator) and community guidelines such as general disagreement resolution.

Codes of Practice which lack any one of these items tend to not have the intended effect.

Download the [Code of Practice template](/06-code-of-practice-template).

Other Code of Practice (also called Code of Conduct) templates and generators:

- [Berlin Code of Conduct](https://berlincodeofconduct.org/) (multi language).
- [Code of Conduct Generator](https://github.com/sindresorhus/conduct) - based on the [Contributor Covenant](https://www.contributor-covenant.org/version/2/0/code_of_conduct/) ([multi language](https://www.contributor-covenant.org/translations/)).
- [Mozilla's Diversity & Inclusion in Open Source repository](https://github.com/mozilla/inclusion), containing resources, templates and standards for making open projects more inclusive.

<a name="sop"></a>

### Standard Operating Procedures for DSHCS Operators

It is important for a DSHCS to have Standard Operating Procedures (SOPs), to define, for example, how operators should respond to requests, communicate with beneficiaries, ensure confidentiality and make referrals, but also to make sure they know how to behave in stressing situations and how to cope with stress.

> There are different good practices that we have been developing along the way. Starting with caring for the well-being of those that come to us, and also for the team. We all know that this can be exhausting, but we need to be well to accompany others best.<br><br>_Tecnoresistencias (Haché, 2021)._

### Learn more

- Access Now Digital Security Helpline Public Documentation includes a [section on their standard operating procedures](https://communitydocs.accessnow.org/tag_helpline_procedures.html).
