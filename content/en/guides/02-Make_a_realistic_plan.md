---
layout: post
title: "Make a Realistic Plan"
date: 2022-06-09
categories: guide
lang: en
order: 2
---

# 2. Make a Realistic Plan

![collage suculents folders](/chap_2.png)

After setting your DSHCS's framework it is time to establish a general plan which outlines how to carry it out; that is, to define its material aspects. This plan should include drafting a budget to cover all your operative needs: from staff, hardware and software to renting an office if needed.

It is essential to establish how you will raise the resources to cover this budget. Will you accept funds from governmental and private actors? Will you crowdsource your funds? Will you look for partnerships with public institutions? How your DSHCS achieves financial sustainability will mostly depend on the institutional setting you defined in your framework.

An important point at this stage is also defining what tooling the DSHCS will use, especially to track cases. There are many different ticketing systems with specific features, requirements and costs. You need to find the one that best suits your DSHCS's needs. In this chapter, we share a comparison of the most popular ones so that you can make an informed decision on which one to adopt.

Your plan should obviously include a strategy to secure your staff both on the digital and physical level. We have included some basic guidelines in this chapter, although we strongly recommend hiring a specialist to create a thorough security plan, especially if you have decided to create a physical office and to self-host your infrastructure.

Finally, you should reflect on how to create and nurture your team: when planning for your DSHCS, you will need to decide what basic skills your team members need to have to provide the services you want to offer, either by hiring the right people or by training them after they've joined your team. You will also need to reflect on their roles and responsibilities and on how to care for their well-being.

## 2.1 Create a Budget

Based on the decisions you have made for your DSHCS, especially with regard to your organisational structure and response time, as well as services that you want to offer that imply additional costs (for example, offering grants to replace stolen equipment or paying training for your staff), the cost of your helpline will vary a lot.

Your initial core budget, which will be spent to create the DSHCS and start offering services, should cover at least:

1. Initial staff salaries, unless you are a completely volunteer-based help desk.
2. Hardware and software costs: work devices for staff, office equipment (if you have an office), software licences, etc.
3. Cost of online services like a website, a file-sharing platform, a ticketing system, etc.
4. Salaries or fees for legal consultants, deployment of services, etc.
5. Training for staff.
6. Office rental (if you choose to operate out of an office).

You can draft a minimum budget, which contemplates the essential items to be operationally viable, and a maximum budget, in case you get all the required resources.

This budget should be reviewed after the design phase is complete and all operational decisions have been taken.

<div class="learn-more">
<img src="more-info.png" class="learn-more" alt="pages links">

### Learn more

- Read more on good practices to create a budget in ENISA (2020). "High-level roadmap and budget". In ENISA, _How to set up CSIRT and SOC - Good Practice Guide_ (pp. 16-18). [https://www.enisa.europa.eu/publications/how-to-set-up-csirt-and-soc](https://www.enisa.europa.eu/publications/how-to-set-up-csirt-and-soc).
</div>

## 2.2 Decide How Your DSHCS Will Get Funded

To ensure that your DSHCS can operate in the long term and become a reference point for your beneficiaries you will need to develop a reliable funding model to cover your costs.

If your DSHCS is part of a larger organisation, you can probably rely on your host organisation to think about how to cover your costs. But if you are an independent organisation, you should think of the following details:

- Where will the funds come from? Will you apply for grants, ask for donations, be funded by a consortium of organisations, or cover your costs by offering some paid services?
- Will you need to find several funding sources, or is one sufficient to guarantee your long-term operations?
- How secure is/are your funding sources?
- Is your funding source stable, or will it end at some point?
- Will you only look for financial resources, or will your DSHCS look to establish collaborative relationships with partner organisations and institutions?

> The challenges we've encountered so far are related to sustainability because the helpline is a funded project. So what if there's no funding? How do we maintain it? How do we manage it? And for that, we have created certain resources for people to access help. One of them is a portal of lawyers and professionals who can offer free legal services to women who are experiencing online harassment, for example.<br><br>_Digital Rights Foundation (Haché, 2021)._

### Funding Policy

Some organisations also choose to have a funding policy to decide whether a new funding source is consistent with their missions and goals. For example, some groups decide only to receive donations to remain completely independent, while others may not accept funds that would influence their priorities.

Here are some examples of funding policies by CiviCERT members:

#### Access Now

The majority of our support comes from foundations and development agencies, with the rest coming from companies, courts, individuals, and civil society organizations. To ensure the independence, and integrity of our organization, we accept support contingent upon the following non-negotiables:

- Access Now does not accept funding that places its staff, partners, supported communities, or mission at risk.
- Access Now does not accept funding that jeopardizes its relationship with its partners, stakeholders, or supported communities and networks.
- Access Now does not accept funding that compromises its organizational independence, including funding relationships that may influence its priorities, policy positions, advocacy efforts, regions of focus, or direct action work.
- Access Now does not accept funding that poses a risk to its reputation more broadly or with respect to specific programmatic areas of work.

#### Amnesty International

The overwhelming majority of our income comes from individuals the world over.

These personal and unaffiliated donations allow Amnesty International (AI) to maintain full independence from any and all governments, political ideologies, economic interests or religions.

We neither seek nor accept any funds for human rights research from governments or political parties and we accept support only from businesses that have been carefully vetted.

By way of ethical fundraising leading to donations from individuals, we are able to stand firm and unwavering in our defence of universal and indivisible human rights.

### Learn more

- [Access Now Funding Policy](https://www.accessnow.org/financials/)
- [Amnesty International Funding Policy](https://www.amnesty.org/en/about-us/how-were-run/finances-and-pay/)

## 2.3 Office and Physical Security

Whether you decide to set up a physical office or not, when creating your help desk, you should make a plan to protect your staff and volunteers, as well as your office and infrastructure, from physical attacks.

When thinking about physical security for your office, you should consider, for example:

- Protecting hardware that contains sensitive information: computers, external hard drives, servers, etc.
- Making sure nobody accesses the office without permission. For example, by installing a security camera at the entrance of your office or by creating awareness in your team on who can access their home offices and who can't.
- Protecting printers and printed documents from unauthorised access and destroying printed documents that are no longer in use.
- Securing routers and communications infrastructure.
- Reminding the team to never leave their computers unattended. And if they do, to always leave it locked.

### Learn more

- You can find physical security recommendations for your office or for volunteers working from their homes in [Access Now Helpline's Security Policy Templates](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/tree/master/templates/organisational_Security_Policies-Templates).
- Higson Smith, Craig, Ó Cluanaigh, Daniel, Ravi, Ali G., Steudtner, Peter (2016). *Holistic Security - A Strategy Manual for Human Rights Defenders*. Tactical Technology Collective - [https://holistic-security.tacticaltech.org](https://holistic-security.tacticaltech.org)
- Eguren, Enrique, y Cara, Marie (eds.) (2009). _New Protection Manual for Human Rights Defenders_. Protection International. [https://www.protectioninternational.org/wp-content/uploads/2012/04/Protection-Manual-3rd-Edition.pdf](https://www.protectioninternational.org/wp-content/uploads/2012/04/Protection-Manual-3rd-Edition.pdf)
- Tsunga, Arnold (ed.) (2007). _Protection Handbook for Human Rights Defenders_. Front Line, The International Foundation for the Protection of Human Rights Defenders. [https://www.frontlinedefenders.org/fr/file/1671/download?token=XHaqzSCK](https://www.frontlinedefenders.org/fr/file/1671/download?token=XHaqzSCK)

## 2.4 Network Security

If your team is working in an office, they should have a dedicated network to connect to the internet, as well as to printers, physical servers and so on. A separate guest network should be created if you want to let visitors use your WiFi.

If you can, consider hiring a system administrator to manage your office network as well as your own infrastructure, if you have decided to self-host your online tools and platforms.

## 2.5 Infrastructure and Tooling

In theory, you can start a DSHCS with just a computer for each of your staff members and a secure collaboration platform like a self-hosted NextCloud, Gitlab, or Discourse instance to share information among team members, organise tasks, keep track of contacts and so on. You could even rely on a [friendly service provider](https://communitydocs.accessnow.org/282-Secure_file_sharing_storage.html) to host such a platform for you where compatible with your threat model.

If your budget allows for it, when making plans for your digital security help desk you should consider using tools that have been created on purpose for handling incidents, identifying indicators of compromise, sharing information and analysing malware.

Many digital security help desks for civil society use the following tools to handle and analyse incidents:

- A ticketing system to manage support requests and cases (see below, section _Ticketing Systems_).
- A self-hosted or a federated instance of [MISP](https://www.misp-project.org/) for sharing, storing and correlating indicators of compromise.
- A malware analysis system like [Cuckoo Sandbox](https://cuckoosandbox.org/).
- Sometimes, a CRM for managing contacts like [CiviCRM](https://civicrm.org).

In general, when choosing a tool or service, it's best to follow these guidelines:

- The software should be free and open source.
- Ideally, it should be self-hosted or otherwise hosted by a trusted entity.
- If the service is hosted by a third party, it should be end-to-end encrypted, audited and offer a good level of security (e.g., 2-factor authentication).

### Ticketing Systems

As soon as a support request is received the person in charge of the case should start noting down every detail regarding the beneficiary, the incident and any action that has been taken to handle it. Recording all communications with the beneficiary, as well as every step taken to detect and address the incident, can facilitate collaboration with other team members and referrals to other organisations, help improve incident handling procedures and provide useful evidence that may be needed in a court of law.

Cases can be documented in many ways, even in text documents or on paper, but using a ticketing system will simplify this task as this kind of tool makes it possible to record and organise every communication and detail in a systematic way, allowing to track each support request, as well as beneficiaries and relevant third parties, and to review past cases for the sake of quality assurance and to obtain statistics, just to name a few critical features that can be helpful for a DSHCS's work.

Since communications pass through the ticketing system and are stored there, one of the essential features a DSHCS should look for when choosing the tool they will use to track requests is the possibility to secure communications and information on incidents. Therefore, it is recommended to choose a system that is compatible with encrypted communications tools such as GPG-encrypted emails or secure messaging apps like Signal.

Furthermore, a ticketing system should allow recording the following information:

- The beneficiary's name and email address.
- The beneficiary's vetting status (vetted, non vetted, rejected).
- The kind of constituency the beneficiary belongs to.
- The current status of the incident (new, open, closed, etc.).
- The urgency and priority of the incident.
- A summary of the incident.
- The kind of service requested to address the incident.
- All communications with the beneficiary.
- Evidence gathered during the incident investigation.
- Indicators related to the incident.
- Other cases related to this incident.
- Actions taken on this incident.
- Contact information for other involved parties (e.g. intermediaries, companies that have been reached out to for an escalation, partners who are helping handle the case, etc.).
- All communications with other involved parties.
- Comments by incident handlers.
- Next steps to be taken.

What follows is an overview of the ticketing systems most commonly used in the sphere of digital security support for civil society.

#### Zammad and CDR Link

[Zammad](https://zammad.com) is an open source web-based help desk and customer support system with features to manage customer communications via several channels like telephone, Facebook, Twitter, chat and email, among others.

In view of the specific needs of civil society and human rights defenders, the [Center for Digital Resilience](https://digiresilience.org/) has developed [CDR Link](https://docs.digiresilience.org/link/about/), a privacy- and security-focused ticketing system based on Zammad that features custom messaging plugins for Signal, WhatsApp, and GPG, to secure communications. CDR Link requires Google accounts to log in and is hosted in AWS.

Zammad can also be integrated with NextCloud (from version 20 onwards): the [Zammad integration app for NextCloud](https://apps.nextcloud.com/apps/integration_zammad) provides a dashboard widget with a Zammad ticket overview, support for finding Zammad tickets using NextCloud's unified search and notifications on status updates to tickets.

#### Request Tracker

[Request Tracker](https://bestpractical.com/request-tracker), or RT, is an open source issue tracking and workflow platform developed and supported by [Best Practical Solutions](https://bestpractical.com/).

RT can be self-hosted, but there are also options for managed hosting or cloud hosting with AWS. It can be integrated with PGP encryption and is one of the most commonly used ticketing systems for computer emergency support teams. Among these, it's worth noting how solutions documented while handling an incident can be promptly [turned into procedural documentation](https://rt-wiki.bestpractical.com/wiki/Articles#Extracting_an_Article) within RT itself.

#### Freescout

[Freescout](https://freescout.net/) is a free and open source ticketing system that can be easily deployed even on shared hosting. It can be self-hosted, but [managed hosting is also available](https://github.com/freescout-helpdesk/freescout/wiki/Cloud-Hosted-FreeScout).

Freescout offers [paid modules](https://freescout.net/modules/) that can extend its functionalities, including integration with [PGP](https://freescout.net/module/mail-signing/) (only signing and encryption), [WhatsApp](https://freescout.net/module/whatsapp/), [Telegram](https://freescout.net/module/telegram-integration/), and [Twitter](https://freescout.net/module/twitter/).

#### Trac

[Trac](https://trac.edgewall.org/) is an open source, web-based project management and bug tracking system that [can be used for tracking tasks, issues, and incidents](https://trac.edgewall.org/wiki/TracTickets) and is sometimes used by help desks to manage their cases. It can be self-hosted but [managed hosting is also available](https://trac.edgewall.org/wiki/CommercialServices).

#### GLPI

[GLPI](https://glpi-project.org/) (a French acronym for _Gestionnaire Libre de Parc Informatique_ or "Free IT Equipment Manager") is a free and open source tracking system and service desk system. It can be [self-hosted](https://glpi-install.readthedocs.io/en/latest/) but [managed hosting is also available](https://www.glpi-network.cloud/).

#### Primero and GBVIMS

[Primero](https://www.primero.org/) is an open source self-hosted software platform for social services case management and incident monitoring, including child protection and gender-based violence.

Based on Primero, the [Gender-Based Violence Information Management System (GBVIMS)](https://www.gbvims.com) is a data management system that enables those providing services to survivors of gender-based violence to effectively and safely collect, store, analyse and share data related to the reported incidents.


#### Others

There are many useful tools that can integrate the tickets of the handling process in your current workflow. As we mentioned before, if your organisation is already using NextCloud there is a specific [plugin](https://apps.nextcloud.com/apps/integration_zammad) for Zammad integration. If your organisation is using Discourse you can add a ticketing system using the [tickets](https://meta.discourse.org/t/tickets-plugin/97914) and [assign](https://meta.discourse.org/t/discourse-assign/58044) plugins.

A regular ticketing style tool may be overkill for small organisations. There are many project management tools that can do a great job and that are way easier to use. For instance, [NextCloud Deck](https://apps.NextCloud.com/apps/deck) is a simple and easy way to handle tickets. Its simplicity is a great asset for small organisations. In NextCloud Desk each ticket is a task, a task has a description, is qualified with tags, has a start and a due date and can be moved from one column to another in order to replicate the ticketing workflow.

## 2.6. Team Management

Any DSHCS requires a team of people with a specific set of skills to respond to support requests in a timely and accurate manner. The size and seniority of your team will depend on your organisational structure and funding situation and on your ability to create, manage and care for their professional development and well-being.

### Desired Skills

A DSHCS should define the skill set needed for it to perform its mission. Since the task of your help desk is responding to digital security requests, there is a core set of skills that the team should work on fulfilling.

The following is an indicative list of technical and soft skills that members of a DSHCS team should have. If your team does not have a needed skill, you will need to identify a third party to outsource the task for which the missed skill is needed.

#### Technical Skill Set

- Ability to perform GNU/Linux-UNIX system administration.
- Ability to perform web server administration.
- Familiarity with the most widespread operating systems: Windows, GNU/Linux, macOS, Android and iOS.
- Ability to work with at least one network analysis tool such as Wireshark, tcpdump, Zeek, Snort, etc.
- Working knowledge of the entire TCP/IP or OSI network protocol stack, including major protocols such as IP, Internet Control Message Protocol (ICMP), TCP, User Datagram Protocol (UDP), Simple Mail Transfer Protocol (SMTP), Post Office Protocol 3 (POP3), Hypertext Transfer Protocol (HTTP), File Transfer Protocol (FTP) and SSH.
- Working knowledge of popular cryptography algorithms and protocols such as Advanced Encryption Standard (AES), Rivest, Shamir and Adleman (RSA), Message-Digest Algorithm (5) (MD5), Secure Hash Algorithm (SHA), Kerberos, Secure Socket Layer/ Transport Layer Security (SSL/TLS) and Diffie-Hellman.
- Ability to perform vulnerability assessments and work with penetration assessment tools such as Kali Linux, Metasploit, etc.
- Scripting knowledge with languages and tools such as Python, bash, awk, sed, grep, etc.
- Familiarity with techniques and tactics of attacks.
- Solid understanding of end-user management of social media platforms like Facebook, Instagram, Twitter, Youtube, etc.
- For those working with malware reverse engineering, knowledge of assembly code in Intel x86 and work with various utilities that aid in malware analysis, such as SysInternals, as well as tool suites used to decompile and examine malware like IDA and Ghidra.

Certainly, candidates with a background in computer sciences and computer security studies would be best prepared to master these skills. However, people who are enthusiastic about such fields could earn and demonstrate these skills quickly as well. When hiring, you should assess studies, experience and level of enthusiasm to identify the right candidate to join your team.

Usually, while a help desk is being created, it is not always possible to have these skills as quickly as one may want. Management should therefore focus on identifying the capabilities that are absolutely needed to fulfil the DSHCS's mission and then fill gaps by training team members, conducting a targeted hiring campaign, or outsourcing certain tasks to other teams.

#### Soft Skills

Soft skills are as important as the technical ones, especially for the handlers who will be in direct contact with beneficiaries.

- Good written and oral communication in English, to coordinate with other partners, and in the local languages of the region they will be focusing on.
- Ability to thrive in high tempo, high-stress environments.
- Strong team player.
- Ability to provide on-the-job training and knowledge sharing to other team members.
- Self-initiative with strong time management.
- Solid sense of integrity and identification with the mission of the DSHCS.
- Strong cultural understanding and experience across the region of focus.
- Broad understanding of the DSHCS constituency's political context.
- Intersectional sensitivity in approach to beneficiaries.

### Roles and Responsibilities

There are many roles in a DSHCS team, each with different responsibilities. Having these in mind can help you design your team structure and identify which positions you need - or are able - to fill in according to your framework and budget.

- **First-line incident handlers** - These are the core members of the team. They are the ones who will be in touch directly with the beneficiaries and coordinate the multiple tasks that will lead to a successful response to the beneficiary's request. In addition to the communications skills needed to understand the beneficiary's needs and send them instructions, first-line incident handlers should be able to follow internal communications among team members, identify gaps in documentation and master the different resources that are available for them to perform incident handling such as (1) software like ticketing systems, secure communications software and tools to perform triage; and (2) human resources, whether internal, such as second level analysts, or external, such as external analysts, services providers and partners.
- **Shift manager** - In small help desks, this could be the director. Their role is to manage the incident handlers.
- **Second level analysts** - Their role is to provide technical support to incident handlers when dealing with challenging cases.
- **Operations manager** - They manage the finances of the team and their needs in terms of hardware, location, logistics, etc. Such tasks could be taken on by the director in small help desks, then more staff could be hired for this role, or it could be outsourced.
- **Documentation coordinator** - The role of the documentation coordinator is to manage the DSHCS's knowledge base. They should make sure that existing documentation is maintained and help identify gaps and opportunities to edit and create the necessary documentation for the help desk and especially for the first-line incident handlers to perform their task.
- **Sys admin** - Their role is to create and maintain the infrastructure that the help desk will use in their day-to-day work. The task could be outsourced or fulfilled by the director or the handlers in small help desks.
- **Legal counsellor** - A legal counsellor will help assess the legal security of the different interventions carried out by the help desk and make sure its policies comply with the existing legal framework (personal data protection, for instance).
- **Psycho-social counsellor** - A psycho-social counsellor can train DSHCS incident handlers to respond to requests for support without revictimising and develop the necessary skills for dealing with people under emotional distress.
- **Outsourcing tasks** - When outsourcing any task, make sure there is a high amount of trust with the team or person you are outsourcing to. Given the mission of a help desk working with civil society and human rights defenders, contracts and non-disclosure agreements are certainly necessary but not sufficient to guarantee the level of security needed by a DSHCS's constituency.

### Create Your Team

Staffing your help desk operation is a crucial process for the success of the project. In addition to their technical and soft skills, your DSHCS's team members should have a strong alignment with your team values and an honest commitment to supporting your constituency. When looking for new team members, consider the following aspects:

- Trust with the communities you support is more important than purely technical skills. Alignment with values and commitment to the team's mission should be among the top requirements for all positions.
- It is always possible to develop technical skills as long as there is time and there are resources to support the development pathway.
- Onboarding processes make a big difference to how quickly members of your help desk can start providing quality assistance to beneficiaries. When possible, create written guidelines on all the topics that should be covered and follow a mentoring approach.

You can decide to hire someone and then train them on skills they need to have when working for your DSHCS, or you can launch a community capacity-building campaign to train activists who may then collaborate with your help desk. If you use this approach, it will be best to base your training sessions on a [holistic approach](https://holistic-security.tacticaltech.org/trainers-manual.html) and on a framework like the Activity-Discussion-Inputs-Deepening-Synthesis – or ADIDS – model.

You can find further information on the ADIDS model in the following resources:

- [How to Approach Adult Learning](https://level-up.cc/before-an-event/levelups-approach-to-adult-learning/) – Level Up
- [How to Design Sessions Using ADIDS](https://level-up.cc/before-an-event/levelups-approach-to-adult-learning/) – Level Up
- [A training of trainers module on adult learning and ADIDS](https://www.fabriders.net/tot-adids/) – Fabriders

> Between 2019 and 2021, DDP implemented a project to build the capacities of trainers who could provide sustainable and holistic safety and security support to human rights defenders in South-East Asia, Latin America and Africa. This process took place in two phases: on the one hand, it aimed to strengthen the capacities of protection providers in general; on the other, it aimed to directly engage some of the participants to carry out accompaniment processes. When finished, DDP opened a call allowing participants to show their interest in becoming part of DDP's team of Digital Protection Facilitators. The project was a cornerstone of DDP's broader project of decentralising and strengthening the extent to which its team is embedded in, and driven by, human rights movements in the Global South.<br><br>Daniel Ó Cluanaigh, Digital Defenders Partnership.

### Training and Professional Development

In order to develop knowledge and capacity within the help desk team, different opportunities should be available as a single working strategy for everyone might not be possible or ideal.

Individual team members should work together with their direct managers to create training and development plans. These plans should be regularly consulted, and progress should be noted as actions take place or during performance reviews.

Actions on individual action plans can vary from following online training, to acquiring books and magazines to attending in-person training.

In addition to these personal plans, the help desk organisation should continuously share resources internally, especially on subjects that are relevant to all team members, such as technical subjects on digital safety and others, including self-care, psychological support and physical security.

#### Provide Materials and Encourage Self-Learning

Help desk managers should circulate resources within the team to increase their technical skills on different security-related subjects. This should be an open invitation to team members to review and use those resources, but there should not be follow-ups to ensure the information is being used. The main objective is to provide the team with information that suits their interests and develop a self-learning culture throughout the team.

In addition to online resources, books and magazines that are considered appropriate and beneficial to help desk work can also be provided to the team. The magazines and books will be provided on request by team members or when the help desk management considers they can have a positive impact on the team development.

#### Paid External Training

When appropriate and where resources allow it, help desk management should approve participation in technical training based on the team members' individual development plans.

#### Conferences

You should aim to provide your team members with at least one opportunity per year to attend a help desk-related event.

You should track upcoming conferences and try to attend events that are relevant to your work with civil society groups and other organisations and individuals who provide these digital security services.

One of the best ways to find relevant conferences and events is to ask your beneficiaries what events they go to or would like your team members to attend.

#### Example Conferences That May Be Relevant to a Civil Society-Focused Help Desk

| Event                                    | Frequency           | Website                                                                                                                                                | Content                                                                                           |
| ---------------------------------------- | ------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------- |
| Bread&Net                                | Annual              | [https://www.breadandnet.org/en/](https://www.breadandnet.org/en/)                                                                                     | Annual unconference that promotes and defends digital rights across Arabic speaking countries     |
| Chaos Communications Congress            | Annual              | [https://events.ccc.de/](https://events.ccc.de/)                                                                                                       | Technical knowledge acquisition, meet new beneficiaries or partners                               |
| Dublin Platform                          | Every two years     | [https://www.frontlinedefenders.org/en/programme/dublin-platform](https://www.frontlinedefenders.org/en/programme/dublin-platform)                     | Biennial gathering of human rights defenders                                                      |
| FIFAfrica                                | Annual              | [https://cipesa.org/fifafrica/](https://cipesa.org/fifafrica/)                                                                                         | Forum on Internet Freedom in Africa                                                               |
| Global Rapid Response Network meeting    | Annual              | [https://rarenet.org](http://rarenet.org)                                                                                                              | Meeting of Rapid Response Network members                                                         |
| Global Internet Governance Forum         | Annual              | [https://www.intgovforum.org](https://www.intgovforum.org)                                                                                             | A multistakeholder governance group for policy dialogue on issues of Internet governance          |
| ILGA World (& regional conferences)      | Annual              | [https://ilga.org/world-conferences](https://ilga.org/world-conferences)                                                                               | Global and regional gatherings of LGBTQIA+ changemakers                                           |
| MozFest                                  | Annual              | [https://www.mozillafestival.org/](https://www.mozillafestival.org/)                                                                                   | Tech conference organised by Mozilla                                                              |
| Regional Internet Governance Forums      | Annual              | [https://www.intgovforum.org/multilingual/content/regional-igf-initiatives](https://www.intgovforum.org/multilingual/content/regional-igf-initiatives) | regional conferences for policy dialogue on issues of Internet governance                         |
| Regional Rapid Response Network meetings | Throughout the year | [https://rarenet.org](https://rarenet.org)                                                                                                             | Meeting of Rapid Response Network members                                                         |
| RightsCon                                | Annual              | [https://rightscon.org](https://rightscon.org)                                                                                                         | Global meeting on digital rights organised by Access Now                                          |
| Stockholm Internet Forum                 | Annual              | [https://stockholminternetforum.se/](https://stockholminternetforum.se/)                                                                               | International forum advancing a free, open, and secure internet as a driver of global development |

##### Trusted Introducer Events

[CiviCERT](https://civicert.org) is a CERT accredited by [Trusted Introducer](https://www.trusted-introducer.org/), and as such CiviCERT members can participate in meetings and training sessions for security and incident response teams. Upcoming events are listed [here](https://www.trusted-introducer.org/events.html).

#### Online Platforms

You could also provide your team with access to courses on online platforms. These courses are usually less expensive than in-person courses and allow team members to take them at their pace and as the workload allows.

Some of these platforms could be:

- [Pluralsight](https://pluralsight.com/).
- [Udemy](https://www.udemy.com/).
- [Cybrary](https://www.cybrary.it/).
- [Coursera](https://www.coursera.org/).

#### Skill-Sharing Sessions

Team members who gain skills that could be useful to the rest of the team either while doing their work or after undertaking training should be encouraged to document them through articles and provide skill-sharing sessions for their colleagues. If the skill-sharing sessions are recorded they will serve as learning support for the team and new hires.

### Staff Welfare Policies

When working at a help desk, incident handlers deal with a wide range of distressing situations that can affect them. Caring for your team's psycho-emotional well-being prevents burnout and increases the quality of the attention provided.

It is a good idea to care for your team's well-being as much as you do for your beneficiaries. Therefore, you should allocate all kinds of resources - time, human resources, money, etc. - to adopt a psycho-emotional care approach in your team management strategies.

There are no universal ways to achieve this. They will depend on your team's needs and their notion of care. Some caring strategies to safeguard your team's well-being are:

- Even if your help desk is voluntary-based, provide good working conditions: allowance, benefits, holidays, etc.
- Allocate a percentage of your budget (up to 30%) to collective self-care activities.
- Plan short shifts to allow incident handlers to focus and provide better attention.
- Establish weekly meetings to discuss the cases and provide collective insight.
- Include the figure of a mental health advisor to support the incident handlers in avoiding burnout or stress.
- Provide a check-in instance every time an incident handler's shift begins to assess if they are in a good place to provide support. If they are not, a ringer incident handler with no specific shift assigned can replace them.
- Establish a set of parameters for when a call is too risky and requires escalation to a manager quickly. For instance, if the person calling is at imminent risk. Having these steps in mind will protect the incident handler from excessive stress.
- Plan an annual all-team meeting to discuss challenges, identify learnings and adjust the help desk's care strategies.

You can draft a staff caring policy that sets out all these strategies and to which the team and managers can refer for creating a healthy working environment. Also, consider a psycho-emotional approach when drafting your incident handling procedure - ensuring balanced triage and case assignment among the team members, for example -, code of practice, internal complaints mechanism and other policies.

> We prioritise a balanced workload among the caregivers. We rotate the reception of new cases from week to week in order to distribute them equally. At the same time, it is common for there to be weeks in which the number of cases that come to us increases, and when this happens and exceeds the possibilities of the accompanier, we look for strategies to reorganise the follow-up of cases and lighten the load.<br><br>_Luchadoras (Interview, January 2022)._
