---
layout: post
title: "Code of Practice Template"
date: 2022-06-09
categories: templates
lang: en
---

# Code of Practice Template

## [Org Name]
## Code of Practice

This code of practice applies to all [Org Name] spaces, either in online interactions or physical workspaces, associated events or social gatherings. Staff members and volunteers are responsible for knowing the values promoted by [Org Name], which are detailed in this document and abiding by the rules detailed below.

The mission of [Org Name] is [description of Org's mission]. [Org Name] is committed to providing a safe and welcoming environment for realising this mission. In particular, we aim to banish any shame or stigma surrounding digital security mistakes or hacking, so we encourage all those involved to approach interactions with open, listening and supportive attitudes, and to engage constructively with others at all times.

More specifically, [Org Name] spaces are committed to promoting the following values:

- **Confidentiality:** We will handle all incoming information confidentially and will not disclose it to third parties without consent. We will handle incoming information responsibly and protect it against inadvertent disclosure to unauthorised parties. The security of the methods of storing and transmitting information inside or outside [Org Name] will be appropriate to its sensitivity. We invite all staff members and volunteers to read the [Org Name] policy regarding how information should be classified, stored, shared and destroyed.

  Any remote coordination or online initiatives will happen through secure channels that run on free and open source software and, especially if not end-to-end encrypted, are managed and hosted by trusted parties, ideally by [Org Name] itself. Commercial or proprietary tools will be avoided, especially if they have a history of violating users’ privacy.

- **Collaboration:** We have a strong commitment to fostering solidarity, connection, cooperation and a sense of community in our spaces.

- **Inclusivity:** We believe in the importance of diversity in a way that fosters non-discrimination, free expression, participation and equality.

- **Do-No-Harm:** We are aware of how our actions, behaviours and ways of communicating can have a positive or negative effect on the people surrounding us and try to mitigate these as much as possible. We are aware of the elements affecting our own position of power and make space for acknowledging these structures within [Org Name] spaces. [Org Name] is dedicated to providing a harassment-free experience for everyone, regardless of gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, religion (or lack thereof), technology choices, skill set or level of knowledge. We do not tolerate harassment in any form. Anyone who violates this code of conduct may be sanctioned or expelled from these spaces at the discretion of [Org Name].

### Harassment

Harassment may occur online or in person. Examples of unacceptable behaviour include:

1. Offensive comments which reinforce social structures of domination and/or are related to gender, gender identity and expression, sexual orientation, disability, mental illness, neuro(a)typicality, physical appearance, body size, age, race or religion.
2. Offensive comments and flamewars about other people’s choices of recommended practices, skills, procedures and tools.
3. Unwelcome comments regarding a person’s lifestyle choices and practices, including those related to food, health, parenting, drugs and employment.
4. Deliberate misgendering or use of ‘dead’ or rejected names.
5. Gratuitous or off-topic sexual images or behavior in spaces where they’re not appropriate.
6. Physical contact and simulated physical contact (e.g., textual descriptions like “hug” or “backrub”) after a request to stop. Threats of violence.
7. Incitement to violence towards any individual, including encouraging a person to commit suicide or to engage in self-harm.
8. Deliberate intimidation.
9. Stalking or following.
10. Harassing photography or recording, including logging online activity for harassment purposes.
11. Sustained disruption of discussions, talks or other events.
12. Unwelcome sexual attention or physical contact.
13. Patterns of inappropriate social contact, such as requesting/assuming inappropriate levels of intimacy with others.
14. Continued one-on-one communication after requests to cease.
15. Deliberate “outing” of any aspect of a person’s identity without their consent, except as necessary to protect vulnerable people from intentional abuse.
16. Publication of non-harassing private communication.
17. Publishing another person's private information, such as physical or electronic addresses, without explicit permission.
18. Advocating for, or encouraging, any of the above behaviour.
19. Drugging food or drink.
20. Violating the privacy policy of an event in order to attract negative attention to an attendee.
21. Enlisting the help of others, whether in person or online, in order to target someone. We prioritise marginalised people’s safety over privileged people’s comfort.

Our team will not act on complaints regarding:

- ‘Reverse’ -isms, including ‘reverse racism,’ ‘reverse sexism,’ and ‘cisphobia’.
- Reasonable communication of boundaries, such as “leave me alone,” “go away,” or “I’m not discussing this with you.”
- Communicating in a ‘tone’ you don’t find congenial.
- Criticising racist, sexist, cissexist, or otherwise oppressive behaviour or assumptions.

Let someone leave a conversation that makes them uncomfortable, and do not follow people who asked to be left alone. If you discuss difficult topics that may be traumatic for participants, provide warnings so people may leave a conversation or plan coping strategies.

### Reporting

If you are being harassed, notice that someone else is being harassed, or have any other concerns, please notify us by sending an email to [dedicated email address]. Currently, there are [n.] persons receiving these emails: [Names]. Reports are confidential. You will not be asked to take actions that make you feel unsafe.

This code of practice applies to [Org Name] spaces but if you are being harassed by a person involved in [Org Name] outside our spaces we still want to know about it. We will take all good-faith reports of harassment seriously. This includes harassment outside our spaces and harassment that took place at any point in time.

The response team will contact the accused person in order to inform them about the process and give them an opportunity to respond. The response team reserves the right to exclude people from [Org Name] based on their past behaviour, including behaviour outside [Org Name] spaces. We will respect confidentiality requests for the purpose of protecting victims of abuse. At our discretion we may publicly name a person about whom we’ve received harassment complaints, or privately warn third parties about them, if we believe that doing so will increase the safety of partners or people involved with [Org Name]. We will not name harassment victims without their affirmative consent.

Harassment and other code of practice violations reduce the value of our community for everyone. We want you to be happy in our community as people like you make it a better place. If the person who is harassing you is part of the response team or [Org Name] management, they will recuse themselves from handling your incident. We will respond as promptly as we can.

### Consequences

Staff members or volunteers asked to stop any harassing behaviour are expected to comply immediately. If someone at [Org Name] engages in harassing behaviour, the response team may take any action they deem appropriate, up to and including dismissal and identification of a staff member or volunteer as a harasser to the general public.

### Licensing

This policy is licensed under the Creative Commons Zero licence. It is public domain, no credit and no open licensing of your version is required. This anti-harassment policy is based on the [example policy from the Geek Feminism wiki](https://geekfeminism.fandom.com/wiki/Community_anti-harassment/Policy), created by the [Geek Feminism community](https://geekfeminism.fandom.com/wiki/Geek_Feminism_Wiki).
