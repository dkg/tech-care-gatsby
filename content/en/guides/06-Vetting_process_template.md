---
layout: post
title:  "Vetting Policy Template"
date:   2022-06-09
categories: templates
lang: en
---

# Vetting Process Template

## [Org Name]
## Vetting Process

### Purpose of Vetting

The purpose of vetting beneficiaries is an exercise in reducing risk for [Org Name] and for users at risk.

Some of the risks mitigated by vetting include the risk to [Org Name] of reputational damage resulting from working with organisations that themselves do not uphold basic human rights, or are controversial for any other reason. There is the risk of being socially engineered by our adversaries into releasing information, or allowing our adversaries into getting a foothold onto our platforms, that would then allow them to perpetrate effective attacks on our operation. There is also the risk of adversaries
consuming our resources in fake incidents, thus denying capability to the people and organisations that really require our assistance.

Vetting is an exercise of doing adequate due diligence with the beneficiaries we assist to ensure they are truly part of our constituency.

To make sure this vetting process is properly recorded all communications needed to complete the  vetting process will be recorded in a chronological order in the case history.


### [Org Name] Vetting Process

The process used to vet all new beneficiaries consists of the following steps:

1. Initial evaluation
2. Identify/contact potential vettors
3. Evaluation of responses
4. Sign off and recording


#### 1. Initial Evaluation

A certain amount of groundwork can be done initially via information sources such as Google, Wikipedia, the requester's own website, Whois, PGP keyservers, etc., to make a determination of the validity of the organisation and individual/s in question. None of these sources alone should be considered reliable, but putting them together makes it possible to get some sense of the legitimacy of the organisation/individual.

#### 2. Identify/Contact Potential Vettors

Identifying potential vettors is the next step. We need to find someone we already know and trust that is prepared to vouch for the potential new beneficiary.

A good place to start is to look at the organisation's website, particularly any pages identifying members of the organisation's board. Board members are often high-profile people in the NGO space and are frequently known to staff at [Org Name] or to partners, so this presents an excellent way to identify potential vettors.

#### 3. Evaluation of Responses

What we are trying to establish is that the beneficiary is who they claim they are, and that they act rationally and with safety and respect for others. It is important that the beneficiary has the capacity to respect [Org Name]'s reputation if we are to involve our organisation in providing them assistance. For us this is largely what we are trying to determine.

This is never going to be a hard and fast ruling on "adequacy", as the nature of trust relationships will always be somewhat subjective. However as a general heuristic rule we can consider that if someone we trust implicitly vouches for a new beneficiary we can consider them vetted. If we cannot find someone that we trust implicitly, then we would need two acquaintances whose reputations we trust, that both vouch for the new beneficiary before we would consider the vetting adequate.

#### 4. Sign Off and Recording

Each vetting process needs to be signed off by the ([roles of team members in charge of signing off vetting processes in the organisation]).

The fact that the beneficiary has been through the vetting process and either been declined or successfully vetted is recorded in the case history.
