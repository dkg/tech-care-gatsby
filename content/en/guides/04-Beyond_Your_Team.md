---
layout: post
title: "Beyond Your Team: Networking and Quality Assurance"
date: 2022-06-09
categories: guide
lang: en
order: 4
---

# 4. Beyond Your Team: Networking and Quality Assurance

![collage computer](/chap_4.png)

Digital attacks are constantly evolving, broadening their scope of action and aggravating their impacts, as do the political contexts in which human rights defenders and activists operate. Keeping up with changes, understanding them and strategising responses can be time and resource consuming.

Creating and maintaining a community of practice is essential for a help desk as it enables collaboration. It also allows for more agile referral mechanisms, being aware of the realities of all regions, exchanging resources, learning about new approaches and producing collaborative research and investigations. Each help desk or CERT can specialise in a particular type of attack or constituency and the rest of the community members can benefit from that knowledge.

It is also important to keep constantly up-to-date, double-check that the procedures followed by incident handlers are appropriate, and receive continuous feedback from peers and beneficiaries to improve the help desk's workflow.

This chapter will focus on referrals, a common collaboration strategy among help desks and CERTs that allows scaling the response capacity towards civil society, and on quality assurance, with recommendations for different approaches and mechanisms that can be used to monitor, sustain and improve the quality of the services provided.

## 4.1 Create and Nourish Your Network of Partners

In order to have more referral options, it is important to build your web of trust, giving visibility to your work and knowing more about your peers' work. This can be achieved through participation in local and/or international events for instance. Whether online or at in-person events, you can present your work in booths, lightning talks, tech demos, community labs, workshops, etc. Check the conferences listed in [Chapter 2](/02-make_a_realistic_plan) (section on Training and Professional Development) to know which ones to attend.

Being part of mailing lists such as the encrypted mailing list of [CiviCERT](https://www.civicert.org/about/) is a huge asset, for example, when it comes to referrals. This community of digital security responders for civil society usually exchanges knowledge about referrals and rapid response expertise. Find more details about joining the network on their [website](https://www.civicert.org/apply-to-be-a-member/).

It goes without saying that being part of a network or community is a proactive commitment. Communities need to be taken care of and nourished. In other words, they require maintenance of their infrastructure and documentation, facilitation of meetings, resources, outreach, exchange and, above all, time. Consider this when planning your team's hours and activities.

## 4.2 Referrals

Sometimes a DSHCS gets a request that they cannot handle, whether it falls outside their mandate or requires a set of skills the help desk lacks. The DSHCS may refer the case to another actor that can provide the support needed in such situations.

Once the requester is vetted, and depending on their request, you may direct them to 4 sorts of contacts:

- **Another NGO or non-profit entity** - This can be a referral to a non-profit organisation that you trust, know about, or have worked with in the past.
- **Private companies** - This can also be a referral to a private company with an ethical privacy policy and an ethical business model - a company that is trusted or familiar with non-profit challenges and that tends to use transparent and auditable open source technologies.
- **Governmental entities** - You might need to redirect to governmental institutions such as a national CERT.
- **Freelance experts** - You could also refer a beneficiary to an individual from the community of digital security experts who are familiar with human rights defenders' specific needs.

> The detection of needs in the requests for support we received led us to create bridges with online platforms in the search to put a stop to the violence. We approached the platforms directly, and they opened their doors to us partly because there was information that interested them or because they wanted to connect with organisations that could mediate. We had to be strategic and set limits on what information we shared because they used it to their advantage. If what they offered us was not intended to improve the care conditions for the women, we weren't interested. Even though they are big companies, we expect reciprocity in collaborations.<br><br>_Luchadoras (Haché, 2021)._

Below is a list of potential support you can find within or close to the digital security community once the need of the requester is identified. Vetting the requester is good practice when making a referral to a partner organisation's website but it is not mandatory in this case.

- **CERT teams (governmental institutions)**
  - [FIRST Teams](https://www.first.org/members/teams/)
- **DDOS Attacks Prevention**
  - [Deflect](https://deflect.ca/)
  - [CloudFlare Galileo](https://www.cloudflare.com/galileo/)
  - [Google Project Shield](https://projectshield.withgoogle.com)
- **Domains and Hosting**
  - Advice for hosting can be found in [Access Now Helpline Community Documentation](https://accessnowhelpline.gitlab.io/community-documentation/88-Advice_Hosting.html).
- **Emergency support**
  - [Digital First Aid Kit](https://digitalfirstaid.org/en/support/), an intake mechanism to reach out to members of the CiviCERT community
  - [Committee to Protect Journalists](https://cpj.org/emergency-response/how-to-get-help/), for journalists
- **Funding and Free Licenses**
  - [DDP Incident Emergency Fund](https://www.digitaldefenders.org/funding/incident-emergency-fund/)
  - [OTF Funds](https://www.opentech.fund/funds/)
  - [TechSoup](https://www.techsoup.org/)
  - [Google Nonprofits](https://www.google.com/nonprofits/)
- **Human Rights Violation Documentation**
  - [Huridocs](https://huridocs.org/contact/)
  - [Witness](https://www.witness.org/)
- **Legal Support**
  - [Media Defence](https://www.mediadefence.org/)
- **Physical Security and Relocation**
  - [Protect Defenders](https://protectdefenders.eu/protecting-defenders/)
  - [Umbrella](https://secfirst.org/umbrella/) (mobile app)
- **Security Assessments and Penetration Testing**
  - [OTF’s Red Lab](https://www.opentech.fund/labs/red-team-lab/)
- **Training**
  - Training resources and references can be found in [Access Now Helpline Community Documentation](https://accessnowhelpline.gitlab.io/community-documentation/301-Training_Resources.html).

### Referral Process

#### Vetting

Vetting the requester and their organisation is mandatory when referring someone directly to another team. During this process, you will need to ensure that you have verified the veracity of the request and the authenticity of the beneficiary's work and request. Vetting an organisation or individual is an opportunity to expand your and the community's web of trust.

#### Triage

Before referring to the appropriate organisation or individual, it is important to assess the requester's needs by starting to analyse their threat model and the incident they are experiencing. You may want to use [a threat modelling or risk assessment approach](https://accessnowhelpline.gitlab.io/community-documentation/200-Lightweight_Security_Assessment.html) as the entry point. This initial triage will help you figure out whom to refer the beneficiary to.

#### Identifying the Right Referral

The criteria of choice for a referral will be based on:

- The requester's needs.
- The language used by the beneficiary.
- The geopolitical context.
- The technical expertise needed.
- The cost of the referral.

#### Get Consent For the Referral

It is recommended that you inform your beneficiary from the beginning about your intention to refer them to someone else. To comply with the confidentiality agreement between you and the requester, you should formally ask for their approval to share details with others (security assessment details, the beneficiary's identity and contact details, etc.).

Sharing with the requester key factors regarding the expertise of the entity you would like to refer them to and the main reasons you are opting for that specific entity, as well as the main reasons why you cannot fulfil the request, can help the beneficiary make their decision. The ability of the referral to provide pro-bono services or not is key and should also be mentioned in your communication with the beneficiary.

#### Get Consent From the Third Party You're Referring the Beneficiary To

Once the appropriate entity for your referral has been identified, and once the requester has approved the referral, you may share details with the third party you are referring them to regarding:

- The assessment of the requester's needs.
- The requester's threat model.

The goal is to make sure the third party you are referring the requester to has enough information to take the lead in handling the request. If the third party cannot provide a pro-bono service, the following details will need to be determined:

- What service will be offered.
- The cost of the service.

For a referral to be successful, these details will need to be discussed beforehand so that the beneficiary's and the referral's expectations match.

#### Establish the Connection

When putting the requester and referral in touch, try to identify a secure communication channel they are both familiar with. If they use PGP, consider sharing their PGP keys when introducing them to each other.

#### Follow-Up

After some time - depending on your workload, this may be a couple of weeks or some months - it is a good practice to check both with the beneficiary and the third party you referred them to in order to make sure their case was solved and their needs are fulfilled.

#### Referring an Open Case

It may happen that you have already begun helping a user at risk but for a variety of reasons you are no longer able to provide assistance to your beneficiary.

In such situations it is best to refer the beneficiary to a trusted partner. The referral process will follow the steps detailed in this section but should include an extra step to hand over the incident and an extra communication effort to manage expectations.

- When updating your partner about your intention to forward them the request:
  - Note that in this case, expectation management is key.
  - Preferably have a list of referrals ready for these cases.
  - Clarify financial details.
- When agreeing with the requester to refer them to a partner:
  - Explain why you need to refer them to someone else.
  - Give them details about your partner's expertise.
- During handover:
  - Hand over to your partner all the information you have collected and your technical assessment.

## 4.3 Threat Information Sharing

It is a good practice to regularly share anonymised information on the cases you have handled with your community. This exchange will allow other members to understand and identify digital attack patterns and trends that may also affect their beneficiaries.

At CiviCERT information on each member's work is shared bi-yearly based on the following questions:

- **Threat information**
  - What kind of rapid response cases have you dealt with in the past period of time?
  - What was the nature of the attacks?
  - Who was targeted?
  - What trends do you observe in your work?

* **Threat Intelligence**
  - What recent threats and/or trends are you most concerned about?
  - Has anything changed about the attacks you have been monitoring/addressing?
  - What should other rapid responders be paying attention to?

Keep in mind that the anonymisation of this information has to be done in such a way that it is not possible to trace back the case in any way.

## 4.4 Quality Assurance

This process describes the recommended quality standards to be upheld by a help desk. It also presents recommendations for different angles and mechanisms that can be used to monitor, sustain and improve the quality of the services provided.

### Quality Standards

In order to measure and evaluate the quality of your work it is important to define the expectations and standards that your DSHCS will use. The following guidelines should be considered when assessing the quality of the service:

- Create a space where beneficiaries and intermediaries feel welcome, understood, safe and secure.
- A help desk service should be clear, reliable and practical. Provide excellent customer service to your beneficiaries and deliver it in a manner that shows understanding and empathy to the needs of the beneficiaries and their situation.
- Treat beneficiaries with dignity, respect and confidentiality to ensure that they feel safe and secure and that their issue is being addressed.
- Listen first. Be patient and always listen to what beneficiaries have to say before jumping to conclusions and providing technical advice.
- Use clear, inclusive and gender-neutral language when writing or speaking.
- Incident handlers should adopt an approach informed by adult learning strategies. Whenever possible the aim of interactions should be to educate and empower the beneficiaries through knowledge and advice.
- Interactions with beneficiaries should be clear and concise. They may come from diverse backgrounds and have varying levels of technical proficiency.
- Define what will be your [Service Level Agreement](https://en.wikipedia.org/wiki/Service-level_agreement) (SLA), which includes a maximum response time for incoming requests. For example, "respond to every request within the first two hours after it is received during work hours on a week-day, and within 24 hours during the weekend".
- Provide timely responses while a case is open. If a beneficiary becomes unresponsive, check in regularly to ensure their needs are met.
- Use the existing documentation, escalation processes and other mechanisms to provide excellent, reasonable and well-thought technical solutions.

### Quality Assurance Mechanisms

#### Define Roles and Responsibilities

To be able to ensure the quality of the service, there are a few roles that need to be defined by the DSHCS.

- **Case owner**: The person leading the case and working to help the beneficiary with their request.
- **Reviewer**: The person in charge of reviewing the quality of the work. This may be the same person every time, or they may rotate among team members, depending on the size and structure of the DSHCS. For large organisations, there may be more than one reviewer.

#### Define a Time Frame

Case reviews need to be performed regularly to be effective. Define how often these reviews will be performed. The time range may vary from weekly to monthly, quarterly or even yearly reviews. The periodicity will depend on the size of the DSHCS, its staff capacity, the number of requests handled, the type of requests, etc.

#### Individual Case Reviews

##### Process

The reviewer will conduct a review of the cases closed by the case owners for the specified time period according to the DSHCS' needs.

Reviewers should have flexibility on when the review happens, but at the end of every period all closed cases should have been reviewed.

To facilitate the reviewers' work and get more valuable and useful reviews case owners should document each case thoroughly. Consider adding specific metadata to the case documentation to keep track of comments and feedback for each case. Also take note of any feedback or improvements that can be made to the review process itself.

The DSHCS should take review results into account to improve its policies, incident handling process, care protocols and team skills.

##### Criteria

The review will consider the following aspects:

- **Metadata**

  - Is all the case metadata complete?
  - Is the content of the metadata detailed and accurate?

- **Timeliness**

  - Was the first response sent within the SLA?
  - Were follow-ups made on a regular basis?
  - Did the beneficiary receive responses in a timely manner?

- **Language**

  - Were communications with the beneficiary clear?
  - Is the structure, spelling and grammar of communications adequate?
  - Was the language used non-violent and gender-neutral? Was an intersectional approach adopted?

- **Proficiency**

  - Did the beneficiary undergo a risk assessment prior to recommending a solution?
  - Was the solution used best suited to the beneficiary's context?

- **Documentation**

  - Was the case properly documented?
  - Was any additional data (screenshots, analysis, files) properly recorded in the case?

- **Customer Service**

  - Was there an effort to validate the beneficiary's account of their story?
  - Did the beneficiary feel empowered when the case was resolved?

- **Technical Recommendations**
  - Was the right technological solution applied in this case?
  - Was the technical information communicated in a way that matched the beneficiary’s capabilities and needs?

#### Feedback

A good way to constantly provide a good service to the beneficiaries is to implement some mechanism to gather feedback from them once the case has been resolved. This feedback can be collected in different ways, depending on your organisation's needs and capacities. Some examples are:

- Online forms.
- Follow-up messages.
- Debrief calls.

It is important to keep in mind that the information gathered during this stage may be sensitive, so there is a need to ensure that it is securely transmitted and stored.

##### Feedback Review Process

If feedback is collected the reviewer should consider it and find possible areas of improvement for the processes.

The reviewer should then find a suitable way to report back to the case owner about this feedback (either positive or negative), and determine whether further actions are needed for the case in question.
