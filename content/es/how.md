---
layout: page
title: "Cómo utilizar esta guía"
date: 2022-06-09
categories: about
lang: es
order: 3
---

## Cómo utilizar esta guía

Este sitio web es una guía paso a paso para crear una Línea de Atención sobre Seguridad Digital para la Sociedad Civil (LASDSC), con un enfoque holístico e interseccional. El objetivo de este recurso es que las organizaciones más pequeñas y grupos de base puedan crear un equipo que responda a las necesidades de seguridad digital de las personas con las que trabajan y luchan. Pueden leerlo las personas con perfil técnico que quieran organizarse y unir fuerzas para ayudar a su movimiento, pero también gestores y facilitadoras, que pueden seguir los pasos que se esbozan en los capítulos iniciales para empezar a planificar la creación de una Línea de Atención sobre Seguridad Digital para la Sociedad Civil y luego buscar personas con formación técnica para dotar de personal a su línea de atención durante la fase de implementación.

Luego de un breve ensayo en el que se esboza qué es una Línea de Atención sobre Seguridad Digital para la Sociedad Civil, el **primer capítulo** de esta guía describe cómo decidir qué va a hacer línea de atención, para quién y qué políticas serán necesarias para prestar estos servicios.

El **capítulo 2** describe los distintos pasos necesarios para elaborar un plan realista para tu LASDSC: desde la obtención de financiamiento hasta la creación de una oficina segura, pasando por la dotación de la infraestructura necesaria y la creación y capacitación del equipo.

El **capítulo 3** ilustra el servicio más importante de su equipo, la gestión de incidentes, desde la preparación hasta la detección y el análisis, pasando por la contención, la erradicación y la recuperación, así como la actividad posterior al incidente, incluidas las mejores prácticas sobre la documentación de flujos de trabajo y procedimientos.

Por último, el **capítulo 4** va más allá de las operaciones cotidianas de la línea de atención y ofrece recomendaciones sobre cómo y por qué colaborar con otras líneas de atención sobre seguridad digital y equipos de emergencia informática, y sobre cómo evaluar el trabajo para asegurarte de que prestas un servicio de calidad a tus beneficiarios.

Además de las guías, también hemos incluido una serie de plantillas que puedes utilizar para crear un marco de trabajo y las políticas necesarias para su LASDSC. También puedes descargar el PDF completo de la guía para imprimirlo o leerlo cuando no estés conectado.
