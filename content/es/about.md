---
layout: post
title: "Sobre CuidaTech"
date: 2022-06-09
categories: about
lang: es
order: 1
---

## Introducción

![Sobre CuidaTech](/about.png)

Las organizaciones que ofrecen apoyo a la sociedad civil en materia de seguridad digital se han multiplicado en la última década. Las personas defensoras de derechos humanos, activistas, periodistas, organizaciones LGTBQIA+ y otras minorías que sufren ataques constatan cómo las TIC se convierten en una herramienta crucial para sus actividades y a su vez en un arma poderosa para minar su labor, lo que puede suponer una amenaza tanto para ellas como para sus aliadas.

Han pasado diez años desde que en la primera mitad de la década de 2010 algunos colectivos tecnológicos radicales empezaran a brindar apoyo técnico a activistas y organizaciones internacionales a través de proyectos que proporcionaban asesoramiento y formación en seguridad digital a grupos de la sociedad civil y a personas defensoras de los derechos humanos.

En estos diez años dicho empeño se ha multiplicado. Organizaciones grandes y pequeñas, en todas las regiones del planeta, han creado líneas de atención sobre seguridad digital para las comunidades más diversas: periodistas y personas defensoras de derechos humanos, organizaciones internacionales y movimientos de base, grupos LGTBQIA+, centros de acogida para mujeres, etc.

En 2015, estas líneas de atención también comenzaron a trabajar en red de forma más sistemática a través de la creación de CiviCERT. Desde entonces, esta federación de líneas de atención y proveedores de infraestructuras para la sociedad civil ha pasado de las cinco organizaciones fundadoras a más de treinta, entre las que hay organizaciones locales e internacionales y personas que ofrecen asistencia en materia de seguridad digital en su país o región a nivel individual.

Además, en un contexto en el que los ataques digitales hacia la sociedad civil y las minorías vulnerables no cesan de aumentar, tanto en cantidad como en intensidad, se están creando nuevas líneas de atención sobre seguridad digital en todo el mundo, no solo para la sociedad civil y los movimientos por el cambio social y político, sino también para proteger a las mujeres, a las personas LGTBQIA+ y a otros grupos que son objeto de violencia y de vigilancia focalizada que habilitan las TIC.

Por ello, en los últimos años CiviCERT ha recibido una diversidad de solicitudes para apoyar la creación de nuevas líneas de atención sin fines de lucro. En algunos casos, fue posible aunar esfuerzos entre equipos de respuesta a emergencias informáticas ya establecidos y proveedores de infraestructura para la sociedad civil, y ofrecer orientación, formación e infraestructura a las líneas de atención recién establecidas. En otros casos, cuando únicamente hacía falta una orientación inicial, se constató que la documentación existente para satisfacer las necesidades concretas de una línea de atención que ofrezca apoyo en materia de seguridad digital a grupos de la sociedad civil, era insuficiente. Casi toda la documentación disponible ha sido creada bien para equipos comerciales y gubernamentales de respuesta a incidentes de seguridad informática que no están familiarizados con el contexto de la sociedad civil o bien para líneas de atención humanitaria que no tienen experiencia en emergencias de seguridad digital.

Faltaba un conjunto de instrucciones sencillas que permitieran navegar por los flujos de trabajo y procedimientos de una línea de atención sobre seguridad digital centrada en las necesidades específicas de grupos que a menudo carecen de fondos y de personal, que no funcionan de forma jerárquica y que están expuestos a amenazas desproporcionadas y al consiguiente riesgo de trastorno de estrés postraumático entre su personal o voluntariado.

Con la publicación de esta guía queremos cubrir ese vacío de modo que las organizaciones más pequeñas y colectivos de base puedan conformar equipos que respondan a las necesidades de seguridad digital de las personas con quienes trabajan y luchan. No solo está dirigida a _techies_ que quieran organizarse y aunar fuerzas para apoyar a su movimiento, sino también a personas que quieran montar una línea de atención y que, siguiendo los pasos indicados en los capítulos iniciales, puedan planificar su creación y posteriormente buscar a otras personas con formación técnica para dotarla de personal durante la fase de implementación.

El primer capítulo de esta guía describe, a partir de buenas prácticas identificadas entre los equipos de respuesta a emergencias informáticas, cómo decidir qué servicios va a ofrecer la línea de atención, a quién estará dirigida y qué medidas serán necesarias para prestar dichos servicios.

El capítulo 2 detalla los pasos necesarios para elaborar un plan realista para la línea de atención sobre seguridad digital para la sociedad civil (LASDSC): desde su financiación hasta la creación de una oficina segura, pasando por la dotación de la infraestructura necesaria y la creación y capacitación del equipo.

El capítulo 3 explica el servicio más importante que debe brindar el equipo: la gestión de incidentes. Este incluye desde la preparación hasta la detección y el análisis, pasando por la contención, la erradicación y la recuperación, así como la actuación post-incidente y las buenas prácticas de documentación de los flujos de trabajo y los procedimientos.

Por último, el capítulo 4 va más allá del funcionamiento cotidiano de la línea de atención y ofrece recomendaciones sobre cómo y por qué colaborar con otras líneas de atención sobre seguridad digital y equipos de emergencia informática, y sobre cómo evaluar el trabajo para asegurarse de que se presta un servicio de calidad a las beneficiarias.

También hemos incluido una serie de plantillas que se pueden utilizar para crear el marco de trabajo y las medidas necesarias para la línea de atención.

Esta guía no habría sido posible sin la existencia de CiviCERT y las aportaciones de sus integrantes que compartieron documentación, redactaron nuevos contenidos, nos concedieron entrevistas y revisaron el resultado final. Nos gustaría agradecer especialmente a:

- [Access Now Helpline](https://www.accessnow.org/help/), por el gran apoyo que nos han ofrecido, por compartir generosamente su documentación, tanto la pública como la confidencial y por organizar un _sprint_ interno de redacción para ofrecer excelentes consejos sobre cómo gestionar una LASDSC. Muchas gracias en especial a: Michael Carbone, Maggie Haughey, Mohamed Chennoufi, Rogelio López, Daniel Bedoya Arroyo y Beatrice Martini. Un reconocimiento especial a Hassen Selmi, coordinador de respuesta a incidentes de la Línea de Ayuda de Access Now, por la entrevista que nos concedió y que sirvió de base para la sección sobre el proceso de gestión de incidentes (capítulo 3).
- Daniel Ó Cluanaigh, que ayudó a contar la historia del proyecto _Fieldbuilding_ del [Programa de Defensoras Digitales](https://digitaldefenders.org) para el desarrollo de capacidades comunitarias.
- Etienne Maynier, cuya entrevista sobre la publicación de información restringida en [Amnesty Tech](https://www.amnesty.org/en/tech/) inspiró muchas secciones sobre la gestión de información y la comunicación con las personas beneficiarias.
- Farhanah, facilitadora de protección digital en el [Programa de Defensoras Digitales](https://digitaldefenders.org), por redactar la versión inicial de la sección sobre documentación de procedimientos y por recopilar información para otras secciones.
- Grégoire Pouget y Jean-Marc Bourguignon, de [Nothing2Hide](https://nothing2hide.org), por su contribución a las secciones sobre comunicación segura y gestores de tickets, y por el entusiasmo con el que se sumaron a este proyecto en cuanto se incorporaron a CiviCERT.
- [Luchadoras](https://luchadoras.mx/), por la entrevista que nos concedieron sobre su línea de ayuda feminista.
- Lu Ortiz, cofundadora y directora ejecutiva de [Vita Activa](https://vita-activa.org/), por la entrevista que nos concedió sobre el modelo de cuidados del equipo de Vita Activa.
- Mario Felaco, de [Conexo](https://conexo.org), Jannat Fazal y Shmyla Khan de [Digital Rights Foundation Pakistán](https://digitalrightsfoundation.pk/), Harlo Holmes, de [Freedom of the Press Foundation](https://freedom.press/), y Carlos Guerra, de [Internews](https://internews.org/), por su ayuda en la concepción y coordinación de esta guía.
- Todas las líneas de atención feministas que participaron en la serie de seminarios web [“Construir infraestructura feminista: Líneas de atención feministas para personas que enfrentan violencia de género en espacios digitales”](https://www.digitaldefenders.org/feministhelplines/) organizados por el [Programa de Defensoras Digitales](https://www.digitaldefenders.org/) en 2021.
- Todas las organizaciones, tanto integrantes como no integrantes de CiviCERT, que completaron el largo cuestionario que redactamos para recopilar casos prácticos para esta guía.

Esperamos que esta guía apoye la creación de líneas de atención sobre seguridad digital para la sociedad civil en todo el mundo. Por favor, envíanos tus comentarios y sugerencias sobre cómo mejorarla a: tech-care@digitaldefenders.org.
