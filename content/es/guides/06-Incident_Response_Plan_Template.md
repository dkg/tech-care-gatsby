---
layout: post
title: "Plantilla de plan de respuesta a incidentes"
date: 2022-09-06
categories: templates
lang: es
order: 8
---

# Plantilla de plan de respuesta a incidentes
## [Nombre de la organización]
## Plan de respuesta a incidentes

Este proceso describe la forma en que [Nombre de la Organización] recibe y responde a incidentes de seguridad informática. El proceso incluye la forma en que los incidentes se asignan, analizan, gestionan, elevan, cierran y revisan para obtener aprendizajes.

### Recepción y asignación de incidentes

Cada vez que se recibe un incidente, la persona que lo gestiona es la responsable de proporcionar una respuesta inicial y garantizar el seguimiento del mismo. Esta primera respuesta debe proporcionarse lo antes posible y siempre debe producirse dentro de [plazo definido en el acuerdo de nivel de servicio de la LASDSC].

La persona designada para acompañar el caso es la responsable del análisis y la respuesta al incidente. Los criterios para decidir quién acompaña el caso deben tener en cuenta:

- Prioridad del caso.
- Idioma del caso / idiomas hablados por las gestoras de incidentes.
- Carga de casos de las gestoras de incidentes.
- Ubicación geográfica de la persona beneficiaria / zona horaria.
- Conocimientos necesarios para resolver el incidente.

Las personas responsables de cada turno se encargan de equilibrar la carga de trabajo dentro de las oficinas y entre ellas. Si lo precisa, una gestora puede solicitar que un caso que esté acompañando se transfiera a otra persona que pueda tener más herramientas para atenderlo. Esto debe hacerse con el acuerdo de ambas. Si es necesario, la persona beneficiaria también debe ser informada del cambio.

### Asignación de prioridades

La prioridad del caso es un valor que se asigna a cada caso. Las prioridades ayudan a las personas que acompañan los casos y, en general, a la dirección de la LASDSC a asignar una cantidad adecuada de recursos a cada caso. También definen el orden en que deben resolverse los mismos. La prioridad refleja la respuesta organizativa necesaria para cada solicitud.

Entre las variables que se tienen en cuenta a la hora de priorizar, el impacto y la urgencia son las más relevantes.

#### 1. Impacto para la beneficiaria

En los casos en los que la persona beneficiaria está en peligro físico o digital y las consecuencias de no actuar pueden ser graves, debe ser abordado teniendo en cuenta las posibles consecuencias y efectos del problema y la solución a plantear. Existen tres categorías para definir el impacto de un caso: alto, moderado y bajo.

Para establecer el impacto de un caso, puedes consultar la siguiente tabla orientativa:

| Categoría       | Descripción                                                                                                                                                                                                                                                                                                                                                                                                                                 |
|----------------|-------------------------------------------------------------------------------|
| **Alto (A)**  | - Una persona ha resultado herida o corre el riesgo de resultar herida.<br />- La persona beneficiaria está ante una situación reactiva/peligrosa.<br />- Existe un alto riesgo de exposición de información sensible.<br />- Es probable que la información personal de varias beneficiarias se vea expuesta.<br />- Si la situación no se gestiona adecuadamente, puede dañarse la reputación de la línea de atención.<br />- La persona beneficiaria puede ser alguien de perfil alto.<br />|
| **Medio (M)** | - Las consecuencias del incidente pueden definirse como un valor intermedio, entre bajo y alto.<br />|
| **Bajo (B)**  | - El objetivo del caso es evitar un futuro incidente de seguridad para la organización.<br />- El asesoramiento brindado no pone a la persona beneficiaria en riesgo físico inminente. |

#### 2. Impacto para la LASDSC

A veces los casos pueden tener un impacto sobre la línea de atención y su reputación. Estos deben tratarse con especial cuidado, implicando al equipo directivo en su resolución.

#### 3. Urgencia

Se define como la medida de retraso que se puede tolerar y la rapidez con la que se requiere una solución. Los casos pueden clasificarse como muy urgentes, moderadamente urgentes y no urgentes, depende de varios factores, entre ellos los distintos factores de tiempo y el nivel de amenaza si no se actúa en un plazo determinado.

Para establecer la prioridad del caso, quienes gestionan los incidentes también deben tener en cuenta lo que diga la persona beneficiaria cuando se abre el caso. A veces especifican que el caso es urgente por un motivo concreto. Para establecer la urgencia del caso, ofrecemos a continuación una tabla orientativa:

| Categoría       | Descripción                                                           |
|----------------|-----------------------------------------------------------------------|
| **Alta (A)**  | - Las consecuencias causadas por el incidente aumentan rápidamente con el paso del tiempo.<br />- Se puede evitar que un incidente menor se convierta en uno mayor si se actúa inmediatamente.<br />- El caso se abrió de forma reactiva, por una beneficiaria que buscaba ayuda inmediata.<br />- ¿Es un DDoS? ¿Se está produciendo una filtración de datos sostenida en el tiempo?<br />|
| **Media (M)** | - Las consecuencias causadas por el incidente aumentan lentamente con el tiempo.<br />|
| **Baja (B)**  | - Las consecuencias de no resolver el caso no aumentan con el tiempo.<br />- El objetivo del caso es evitar un futuro incidente de seguridad para la organización. |


#### 4. Prioridad

Mediante la combinación de los factores mencionados (urgencia e impacto), la persona que está gestionando el incidente puede evaluar la correspondiente prioridad del caso. La siguiente tabla resume la prioridad:

![](media/priority.png "Priority Chart" )


* NOTA: Si existe alguna duda sobre la urgencia o el impacto de un caso, siempre es mejor pecar de prudencia y no correr ningún riesgo.*

Como norma general, si un caso tiene una prioridad mayor, también tiene un impacto relevante para la línea de atención y para la persona beneficiaria. Hay que tener en cuenta que, independientemente de la prioridad del caso, si la persona que gestiona el incidente no está segura del consejo que debe dar, debe solicitar el apoyo de sus compañeras.

### Ciclo de vida de la respuesta a incidentes

Lo que viene a continuación es nuestro flujo de trabajo de respuesta a incidentes. Ofrece una visión general de cómo deben gestionarse los incidentes de seguridad digital. No contiene consejos sobre cómo abordar incidentes específicos. Para consejos específicos sobre cómo gestionar distintos tipos de incidentes, véase nuestra documentación de procedimientos.

El ciclo de vida de respuesta a incidentes que ha seguido la línea de atención está basado en: [Paul Cichonski, Tom Millar, Tim Grance, Karen Scarfone, *Computer Security Incident Handling Guide*, NIST, 2021. https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-61r2.pdf](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-61r2.pdf).

Cuando una organización se pone en contacto con nuestra LASDSC para solicitar apoyo preventivo, generalmente se está **preparando** y está ajustando sus prácticas para evitar que se produzcan incidentes de seguridad digital. Este es el escenario ideal, en el que ayudamos a nuestras beneficiarias a mitigar el riesgo de que su seguridad o sus datos se vean afectados.

A veces, la solicitud de la beneficiaria es que se investigue un posible incidente o intento de incidente. Este tipo de informes no cuentan con pruebas claras de que ya se haya producido un incidente, por lo que requieren una investigación inicial para verificar el informe y confirmar si ya se ha producido un incidente o no. Esta fase se denomina **detección** y la gestora puede requerir más pruebas para su investigación hasta que quede claro que el suceso realmente ha puesto en peligro la seguridad de la beneficiaria y que no se trata de un suceso sin consecuencias. Nuestra documentación de procedimientos debe ayudar a quien acompaña el caso a entender qué pruebas o información son útiles para investigar los distintos tipos de incidentes. Este tipo de casos normalmente son de urgencia media o alta, especialmente cuando aún se está determinando si se ha producido un incidente o no.

Sin embargo, a menudo las personas beneficiarias se ponen en contacto con nuestra línea de atención cuando ya se ha producido un incidente. Es decir, que ya se han producido una o varias agresiones que han dañado o intentan dañar intencionadamente el sistema, la red o los datos de la persona beneficiaria. Algunos ejemplos son: indisponibilidad del sistema, fuga de datos, secuestro del dispositivo, exposición de la cuenta, etc. Por lo tanto, normalmente damos prioridad a la **contención** de ese incidente para evitar que el daño se extienda a otras partes.

Las acciones que solemos adoptar para **contener los incidentes** incluyen: solicitar a una plataforma en línea que suspenda una cuenta expuesta, aislar de la red un sistema expuesto, suspender un sitio web desfigurado, eliminar datos que se están filtrando, etc. En la mayoría de los casos, la contención debe ser rápida y hay que priorizarla. En algunos casos, dependeremos de las acciones de la persona beneficiaria para retirar su sistema de la red o aislarlo mientras proporcionamos las instrucciones técnicas a través de comunicaciones remotas. La urgencia de estos casos suele ser alta cuando se trata de contener un incidente que se está produciendo.

Cuando se consigue la contención, normalmente es importante que quien acompaña el caso dedique algún tiempo a analizar la causa que ha originado el problema. Esto suele dar lugar a una investigación que por lo general tiene lugar en la fase de **detección y análisis**. En función de la categoría del caso, se pueden solicitar pruebas adicionales a la persona beneficiaria para realizar el **análisis**, como el origen del correo electrónico malicioso recibido hace poco, el enlace para descargar una aplicación maliciosa, capturas de pantalla de las alertas antivirus, etc. El objetivo es determinar si se debe tomar alguna medida más para garantizar que la recuperación con respecto al incidente es plena. De nuevo, la persona que gestiona el caso debe consultar nuestra documentación de procedimiento para saber qué otra información puede ser útil para realizar su análisis.

La **erradicación** consiste en limpiar los sistemas expuestos para garantizar una recuperación plena. Puede ser tan sencillo como instalar y ejecutar una aplicación antivirus o, en algunos casos, puede requerir instalar de nuevo el sistema. Sin embargo, en todos los casos, es fundamental averiguar y documentar qué ha dejado el ente atacante (si es que ha dejado algo) y limpiarlo. Esto sirve para vigilar la posible reaparición del ataque y para detectar otros ataques similares contra otros sistemas o personas beneficiarias. En el caso de sistemas donde no es posible una nueva instalación o donde no se puede restablecer la configuración de fábrica, cabe eliminar de forma manual los artefactos del ente atacante identificados en la fase de análisis: véanse las tareas de inicio o cron para relanzar una puerta trasera. En casos como los ataques DDoS, la **erradicación** no es posible porque en estos incidentes el ataque no deja ningún artefacto y su origen está tan distribuido que derribar cada host implicado en el ataque no es posible ni razonable. Sin embargo, en los casos en los que la infraestructura del ente atacante no está distribuida, el desmantelamiento de esta infraestructura maliciosa debe considerarse parte de la fase de erradicación. Denunciar una cuenta que esté filtrando información o suspender una dirección de correo electrónico que esté enviando correos de _phishing_ también podría considerarse **erradicación**. La erradicación normalmente no suele ser urgente, ya que la amenaza en general ya debería haberse contenido en esta fase. Sin embargo, si el sistema sigue vivo o conectado (según la preferencia de la persona propietaria) y el análisis ha descubierto artefactos que permiten que el ataque reaparezca pronto o inmediatamente, hay que asignarle al caso una urgencia alta.

La distinción entre la fase de recuperación y la de erradicación no siempre es clara, ya que la recuperación viene tras erradicar el artefacto del ente atacante: por ejemplo, cuando se elimina la dirección de correo electrónico o el número de teléfono de quien ha hackeado la cuenta y se asocia de nuevo a los datos legítimos o cuando se utiliza un antivirus para limpiar un gusano no persistente. Sin embargo, para garantizar la recuperación es importante, en algunos casos, vigilar cualquier posibilidad de reaparición del ente atacante. Esta tarea es especialmente importante cuando descubrimos que el ataque está muy focalizado y la amenaza es persistente. En estos casos, los entes atacantes no dudarán en volver a atacar utilizando la misma vulnerabilidad/debilidad o buscando otras. Según lo factible que sea hacerlo, se puede ayudar a una persona beneficiaria a monitorizar cualquiera de los artefactos que ya se han encontrado y eliminado en la fase de erradicación.

La labor **posterior al incidente** consiste en actividades preventivas que se pueden llevar a cabo tras el ataque. Puede tratarse de formación, refuerzo del sistema, pruebas de penetración, una auditoría de seguridad y una evaluación de la organización de la persona beneficiaria, entre otras cosas. Sin embargo, parte de este trabajo preventivo podría realizarse en una fase anterior del incidente para garantizar también una recuperación plena. Por ejemplo, una persona beneficiaria cuya cuenta haya sido hackeada podría recibir ayuda para crear una nueva dirección de correo electrónico protegida con una contraseña fuerte y una autentificación de dos factores para poder recuperar su cuenta. En casos de exposición del sistema, se podría realizar un escaneo de vulnerabilidad en el sistema para eliminar cualquier vulnerabilidad que permita nuevos ataques antes de que este sistema vuelva a estar operativo. En los casos de acoso, la investigación de inteligencia de código abierto podría ayudar a la víctima a recuperarse de un acoso anterior e identificar cualquier información disponible en línea que pudiera ser utilizada de nuevo por el ente atacante. La actividad posterior al incidente necesaria para recuperarse del mismo ¡no debe figurar nunca con una urgencia baja asignada!

#### Consideraciones importantes

A la hora de responder a las solicitudes se debe tener en cuenta lo siguiente:

* Tras la recepción de un caso, además de la respuesta automática enviada por el sistema de gestión de tickets, la persona que esté de turno debe responder personalmente a la persona solicitante, explicándole que se encargarán del caso y poniéndose a su disposición para cualquier cuestión que pueda surgir.

* El proceso de verificación de una nueva persona beneficiaria puede llevar cierto tiempo. Mientras se lleva a cabo este proceso, la acompañante del caso debe comenzar a trabajar en la solución, teniendo en cuenta que hasta que no se verifique a la beneficiaria, se debe tener un cuidado especial en cuanto a la información que se comparte y a las acciones que se llevan a cabo, ya que aún no se ha confirmado el vínculo de confianza.

* Cuando busquen soluciones para los casos, tengan siempre en cuenta las siguientes recomendaciones:
    * Busquen documentación de procedimiento relacionada.
    * Deriven o recurran a otras organizaciones o personas afines para casos específicos.
    * Consideren la posibilidad de acudir a CiviCERT.
    * En caso de que se encuentren en un callejón sin salida, eleven siempre el caso y consúltenlo con la dirección.


### Razones para cerrar un caso

A la hora de cerrar un caso, la persona que lo gestiona debe registrar el motivo por el que lo cierra. Las opciones posibles son:

- **Resuelto con éxito**: Se ha logrado cumplir el objetivo del caso.
- **Falta de respuesta de la persona solicitante**: La persona beneficiaria no ha respondido después de varias comunicaciones.
- **Futura mejora**: El objetivo del caso no se ha cumplido en su totalidad y se llevarán a cabo otras acciones en el futuro.
- **Solución fallida**: No se ha logrado cumplir el objetivo del caso.
- **Petición de la persona solicitante**: La persona solicitante pidió explícitamente que el caso se cierre.
- **Cancelación interna del caso**: El caso fue cancelado a petición de una persona del equipo interno.

Un caso solo debe cerrarse por falta de respuesta de la persona beneficiaria si esa falta de respuesta nos impide responder a la incidencia. Si la persona que está gestionando la incidencia ha cumplido los requisitos para concluir el caso, este debe etiquetarse como resuelto con éxito, independientemente de que tengamos o no noticias de la persona beneficiaria.
