---
layout: post
title: "Proceso de gestión de incidentes"
date: 2022-09-06
categories: guide
lang: es
order: 3
---

# 3. Proceso de gestión de incidentes

![collage red suculentas](/chap_3.png)

Una parte fundamental del trabajo de muchas LASDSC es la respuesta a incidentes. Toda línea de atención debe clarificar de antemano los pasos y recursos necesarios para atender una solicitud de apoyo.

El proceso de gestión de incidentes es un proceso continuo y generalmente consta de las siguientes cuatro etapas:

- Fase 1: Preparación.
- Fase 2: Detección y análisis.
- Fase 3: Contención, erradicación y recuperación.
- Fase 4: Actividad posterior al incidente.

El proceso de gestión de incidentes no debe limitarse a la fase de contención, erradicación y recuperación: es necesario dar otros pasos, por ejemplo, en la fase de preparación y en la actividad posterior al incidente. Este proceso debe documentarse y organizarse de modo que las personas encargadas de la gestión de incidentes tengan siempre presente cada paso en cada fase del proceso y sean capaces de reducir las posibilidades de que se repita el mismo incidente.

> Es muy importante que todo el personal de la LASDSC esté familiarizado con el flujo de trabajo, que todo el equipo participe en las distintas tareas y sepa qué hacer cuando se produce un incidente: cómo se maneja y qué hacer en cada fase. Esto debe redactarse, no puede ser una rutina que se dé sin más, porque si no, puede conducir a errores. Este flujo de trabajo no solo lo utilizamos en nuestra línea, sino también otros CERT y está consensuado; además, está estructurado de forma que tiene en cuenta no solo el tipo de beneficiarias a los que ayudamos, sino también nuestras capacidades. Así pues, ayuda a las gestoras de incidentes a saber cómo actuar (desde la detección hasta la recuperación) pero también a prepararse para ello. <br><br> _Hassen Selmi, Access Now Digital Security Helpline (Entrevista, noviembre de 2021)._

Abajo puedes ver un diagrama de flujo que describe el flujo de trabajo de la Línea de Ayuda de Seguridad Digital de Access Now:

[![Flujo de trabajo de la gestión de incidentes de la línea de atención de Access Now](media/an-helpline-workflow-ir-30percent.png)](media/an-helpline-workflow-ir.png)

El proceso de gestión de incidentes también debe adaptarse a las necesidades y al modelo de amenazas del público de una línea de ayuda. Por ejemplo, una LASDSC deberá tener en cuenta que los ataques dirigidos a la sociedad civil suelen ser sofisticados y estar dirigidos a personas que no están preparadas para ellos. Por eso, es importante dedicar tiempo a las fases de preparación y actividad posterior al incidente, para ir más allá de la simple recuperación y convertir un incidente en una oportunidad para prevenir ataques similares en el futuro.

> Hemos atendido un mayor nivel de amenazas con un nivel de seguridad y autoridad bajo: nuestras líneas de atención no tienen autoridad sobre las beneficiarias. Podemos recomendar cosas, aconsejarles que hagan otras, pero son ellas quienes deciden si las hacen o no. La mayoría de las veces se trabaja a distancia y es realmente difícil llevar a cabo exactamente lo que nos gustaría hacer, como otros CERT de otros sectores, así que también tenemos que adaptarnos a ello. <br><br> _Hassen Selmi, Access Now Digital Security Helpline (Entrevista, noviembre de 2021)._

### Para saber más

- Un ejemplo de un plan de respuesta a incidentes [NIST (2012). Computer Security Incident Handling Guide] (https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-61r2.pdf). Véase páginas 21-44 para más información sobre el proceso de gestión de incidentes.
- Kral, Patrick (2021). *Incident Handler's Handbook*. SANS Institute. https://www.sans.org/reading-room/whitepapers/incident/incident-handlers-handbook-33901.

## 3.1 Preparación

El proceso de gestión de incidentes comienza cuando se recibe una solicitud: lo primero que hace la persona que está gestionando es anotar la información básica del caso, asignándole una prioridad y una persona que lo acompaña y acusando recibo de la solicitud a la persona solicitante. A continuación, se realiza la verificación obligatoria, para asegurarnos de que tanto la persona solicitante está en la lista de beneficiarias de la LASDSC, como que el servicio solicitado puede ser prestado.

Si la solicitud no entra dentro de las competencias de la LASDSC, se cierra el caso, posiblemente enviando a la solicitante una lista de recursos alternativos disponibles. Si, por el contrario, la solicitud sí entra dentro de las competencias de la LASDSC, se recomienda que el segundo paso preliminar sea la verificación de la beneficiaria. Cada LASDSC debe tener una [política de verificación](/es/06-vetting-process-template) y aplicarla en este paso, para asegurarse de que cada beneficiaria es quien dice ser antes de tramitar su solicitud.

Una vez comprobadas las competencias y realizada la verificación, se inicia el proceso de gestión de incidentes, que también debe prepararse. La fase de preparación implica igualmente la creación de una documentación adecuada que facilite una respuesta rápida a una serie de incidentes identificados, así como la formación del personal para que siga estas instrucciones (véase el apartado 3.5 Documentación de los procedimientos).

> Si observamos el diagrama del flujo de trabajo, parece que comienza cuando se inicia el incidente, pero en realidad y en la práctica, la fase de preparación debe desarrollarse de forma continua antes de que se produzca cualquier incidente: es una fase proactiva.  <br><br> _Hassen Selmi, Access Now Digital Security Helpline (Entrevista, noviembre de 2021)._

La fase de preparación también incluye campañas de divulgación y formación para las beneficiarias que quieran mejorar su seguridad o la de su organización. Durante esta fase, una línea de atención también puede hacer un trabajo de tejer redes y establecer relaciones con proveedores de servicios o asociarse con otras líneas de ayuda y defensoras para reforzar su capacidad de derivar casos para los que puedan necesitar colaborar con otras entidades.

El trabajo que se invierta en la fase de preparación determinará la rapidez, eficacia y calidad de la respuesta de una LASDSC.

## 3.2 Detección y análisis

Lo ideal es que el proceso de respuesta a incidentes comience en la etapa de preparación. Sin embargo, siendo realistas, a menudo comienza en la segunda etapa del proceso de gestión de incidentes, cuando se recibe una solicitud de apoyo.

El primer paso de esta etapa es la detección, cuyo objetivo es asegurarse de que lo que la beneficiaria está observando es un verdadero incidente de seguridad digital, es decir, que el comportamiento que ha observado es anormal.

La persona que gestiona el incidente debe pedir a la solicitante toda la información disponible: archivos de registro del sistema y de la red, capturas de pantalla, mensajes de error, informes de antivirus, correos electrónicos sospechosos, síntomas percibidos de cambios en el comportamiento normal y otras pruebas que puedan indicar que un caso es un incidente de seguridad. Las gestoras de incidentes deben estar abiertas a cualquier posibilidad y no dejar pasar por alto ningún incidente de seguridad digital.

Si la persona que solicita el apoyo padece un trastorno emocional, la recopilación de la información necesaria para determinar de qué tipo de incidente se trata puede resultar revictimizante. En estos casos, se pueden recoger todos los datos con la ayuda de una persona designada por ella.

El siguiente paso en esta etapa es analizar el incidente para comprender mejor qué ocurre y cuáles son las causas. Cuantas más pruebas haya, mejor será la interpretación que pueda hacer la persona que se ocupe de gestionar el incidente.

Distintas pruebas pueden constituir un síntoma del mismo incidente concreto o de otros diferentes. Establecer correlaciones entre las pruebas de forma equivocada puede llevar a una interpretación errónea de los hechos. Una forma útil de evitarlo es llevar a cabo el análisis de forma colectiva en reuniones periódicas de discusión de incidentes.

Una herramienta para empezar a analizar algunas de las cuestiones de seguridad digital más comunes que afectan a la sociedad civil es el [Kit de Primeros Auxilios Digitales](https://digitalfirstaid.org), un recurso gratuito para ayudar a las encargadas de la respuesta rápida a solucionar las emergencias digitales más frecuentes.

Al igual que en la etapa anterior, las personas que gestionan los incidentes deben acordarse de registrar toda la información relevante y documentar todos los pasos realizados.

## 3.3 Contención, erradicación y recuperación

Una vez confirmado que la beneficiaria está ante un incidente de seguridad digital, la persona encargada de gestionarlo pasará a la fase de contención, erradicación y recuperación. El primer paso es la contención: una intervención para contener el daño y asegurar que el ente atacante ya no pueda acceder a los activos digitales de la beneficiaria. La gestora de incidentes debe proporcionar rápidamente las instrucciones de contención para limitar los daños.

Por supuesto, el procedimiento adecuado para la contención depende del tipo de activo atacado. Para más información sobre los distintos procedimientos, dos buenos recursos de libre acceso son [Access Now Helpline's Community Documentation](https://communitydocs.accessnow.org) y [FemBloc](https://docs.fembloc.cat/).

La erradicación consiste en eliminar todo lo que el ente atacante pueda haber añadido. Esto no siempre es fácil porque los agentes maliciosos suelen ser muy creativos a la hora de idear nuevas formas de atacar.

Posteriormente, la etapa de recuperación tiene como objetivo restaurar los sistemas afectados y tomar las medidas necesarias para evitar nuevos incidentes. La supervisión, por tanto, es fundamental para detectar otros métodos que pueda utilizar un ente atacante y cualquier otra exfiltración de datos. Debido a que las líneas de ayuda de la sociedad civil a menudo no pueden supervisar directamente los activos de las personas beneficiarias, se puede sustituir este paso formando a las beneficiarias para que lo puedan hacer ellas mismas.

A veces, quien acompaña un caso carece de tiempo o capacidad para poder cerrarlo. En estos casos, se puede requerir la participación de otras personas del equipo para externalizar la gestión del caso o parte de ella, en especial el análisis. Esta es una de las situaciones en las que [la creación de redes y la colaboración entre las LASDSC](/es/04-beyond-your-team) puede ser especialmente útil.


## 3.4 Actividad posterior al incidente

La última etapa del proceso de gestión de incidentes tiene como objetivo recopilar lo que la persona que ha gestionado el incidente ha observado mientras trabajaba en el caso. Aunque es posible que se hayan detectado oportunidades para mitigar incidentes de seguridad digital habituales y se hayan transmitido a la beneficiaria, también es posible que hayan surgido nuevas formas de abordar un problema, que deben documentarse.


> Los últimos pasos también forman parte del proceso. Si a las personas que gestionan los casos se les ocurre una nueva solución o se dan cuenta que las instrucciones que aparecen en la documentación no son tan eficaces como quisieran para gestionar ese incidente, se les pide que propongan soluciones a partir de lo que han observado. A veces también se puede sugerir antes del propio proceso, cuando te das cuenta de que un procedimiento no va a funcionar realmente y que es preciso mejorarlo. <br><br> _Hassen Selmi, Access Now Digital Security Helpline (Entrevista, noviembre de 2021)._

Estas lecciones aprendidas mejorarán la documentación de la línea de atención gracias a un planteamiento más creativo y preciso de la gestión de incidentes. Se recomienda no retrasar el proceso de documentación una vez cerrado el caso, pues los pequeños detalles tienden a olvidarse.
A veces, un incidente puede estar conectado con una serie de ataques de los que hay que advertir a otros grupos: por ello, en esta fase, es importante la divulgación y la creación de redes para difundir las alertas públicas y avisar de este tipo de incidentes a otros objetivos potenciales.


## 3.5 Documentación de los procedimientos

El término "documentación" es bastante amplio. Puede referirse a distintas cosas y, si no se define claramente, puede dar pie a la confusión. En la respuesta a incidentes, hay dos tipos distintos de documentación, ambas igual de importantes: la documentación de casos y la de procedimientos.

La documentación de los casos suele realizarse a través de un gestor de tickets (véase la sección 2.5 Infraestructura y herramientas en el [Capítulo 2](/es/02-make-a-realistic-plan)) u otras plataformas seguras. Consiste en anotar todas las comunicaciones con la beneficiaria, así como la solución técnica que se adoptó para resolver el caso, las pruebas que se recogieron, los motivo por los que se plantearon esas soluciones y los recursos que se consultaron. Esto permite rastrear cómo se ha resuelto un caso y, si se descubre una nueva solución, tiene lugar el segundo tipo de documentación.

El segundo tipo de documentación, que se desarrolla durante la fase de preparación del proceso de gestión de incidentes y se revisa durante todo el ciclo, es la documentación de procedimientos. Se trata de la documentación técnica que contiene las estrategias para atender los incidentes que sufre nuestro público.

En el trabajo de una LASDSC, la documentación de los procedimientos es fundamental para asegurarse de que los incidentes se gestionan correctamente y se garantice la calidad. Gracias a ella, el equipo puede contar con una base de conocimientos actualizada permanentemente que servirá para agilizar su respuesta. Por lo tanto, la información de la que disponen las gestoras de incidentes debe ser precisa, estar actualizada y ser de fácil acceso.

> En la fase de preparación del proceso de gestión de incidentes procuramos tener un conjunto de documentos o guías que nos permitan responder a una serie de incidentes que tenemos identificados, que entendemos o que han ocurrido en nuestra línea de ayuda o en otras organizaciones o CERT. Por eso, intentamos tenerlos siempre a mano, formamos al personal para que los siga y cuando se produce un incidente que cumple los criterios recogidos en estos, los consultamos. Si existe documentación para un determinado tipo de incidente, la persona que está gestionando el caso debe seguirla. <br><br> _Hassen Selmi, Access Now Digital Security Helpline (Entrevista, noviembre de 2021)._

Este capítulo se centrará en los distintos aspectos a tener en cuenta a la hora de crear la documentación de los procedimientos de gestión de incidentes: los principios rectores, la planificación, las plataformas y los formatos, las estrategias de colaboración y las guías de estilo.

### Principios básicos de la documentación técnica

La creación y el mantenimiento de una base de conocimientos técnicos de la LASDSC es una labor de colaboración continua tanto en los CERT y las líneas de ayuda específicas como entre la comunidad de organizaciones de seguridad digital para la sociedad civil. Esta labor colaborativa ha llevado a la adopción de algunas de las mejores prácticas establecidas en la industria tecnológica.

Tanto si se trata de una guía para usuarias finales de una app telefónica, como de una entrada para la base de conocimientos incluida en el gestor de tickets de una línea de ayuda de seguridad digital, cualquier tipo de documentación técnica debe ser:

- **Participativa**: debe incluir a todas las personas que la van a utilizar, por lo que tiene que ofrecer formas sencillas de contribuir y registrar todos los cambios.
- **Actualizada**: una documentación incorrecta puede inducir a más errores que la falta de documentación.
- **Única**: debe guardarse en un único lugar para evitar incoherencias entre versiones.
- **Localizable**: debe ser posible encontrar la documentación cuando se necesite.
- **Comprensible para las usuarias finales:** debe evitarse la jerga técnica.
- **Protegida de intentos no autorizados de modificar su contenido.**
- **Fácil de reproducir para otros proyectos.**
- **Fácil de utilizar en diferentes formatos**: sitios web, aplicaciones para móviles o archivos PDF, entre otros.

### Planifica la creación de nueva documentación

Una LASDSC puede documentar soluciones técnicas tanto para las personas que se encargan de gestionar los incidentes como para las beneficiarias, pero a veces también puede ser necesario escribir para colaborar con socias, realizar campañas de incidencia, comunicarse con los medios, etc. Especialmente en el caso de que una documentación se dirija a usuarias sin formación técnica, siempre conviene preguntarse si la solución específica que se quiere documentar no ha sido ya presentada por otros sitios web de seguridad digital de confianza. Si es así, en lugar de escribir desde cero, se puede, por ejemplo, enlazar un buen recurso a tu base de conocimientos.

Antes de empezar a escribir, una buena práctica es analizar la documentación existente, tanto para asegurarse de no duplicar esfuerzos como para tener una idea clara de las soluciones técnicas necesarias para resolver un incidente concreto.

Cuando tengas una idea clara de lo que quieres escribir, intenta elaborar la nueva documentación de forma que pueda utilizarse en otros casos y no sea específica de un caso que acabas de atender. Para ello, puedes responder a las siguientes preguntas:

- **¿A quién está dirigido?**

    ¿Vas a enviar esta documentación a cada una de las beneficiarias por correo electrónico o vas a publicar un aviso en tu sitio web para que todo el mundo pueda leerlo? También puede estar dirigido a gestoras de incidentes que trabajan en otras organizaciones, a alguien que esté llevando a cabo una campaña de incidencia política o incluso puede ser para una ponencia en una conferencia especializada.

- **¿Qué se quiere lograr?**

    ¿Quieres que las gestoras de incidencias dispongan de soluciones técnicas rápidas para los casos que tratan? ¿O estás redactando una plantilla para los mensajes que envías de forma habitual a tus beneficiarias? ¿Estás preparando un aviso de seguridad para advertir a todas las beneficiarias sobre un nuevo tipo de ataque digital? ¿O quieres preparar un informe público que pueda enviarse a los medios de comunicación?

- **¿Qué tipo de contenido responde mejor a las necesidades de tu público?**

   Ten en cuenta el contexto de tu público. ¿Son profesionales de la informática o usuarias sin formación técnica? ¿Necesitan información técnica precisa o unas instrucciones sencillas paso a paso con capturas de pantalla? ¿Necesitas añadir imágenes a tu guía o sería mejor crear un vídeo o una infografía?

- **¿Cómo encontrará tu público el contenido?**

    ¿Incluirás este contenido en tu gestor de tickets? ¿Se publicará en tu sitio web? ¿Estás creando un manual que se convertirá en un PDF imprimible o en una aplicación para dispositivos móviles? ¿El contenido estará disponible tanto en línea como fuera de línea?

- **¿Se traducirá o adaptará el contenido?**

    En función del público al que vaya dirigido, puede que quieras traducir el contenido a los idiomas y referencias culturales más utilizados por las personas a las que quieres llegar.

Estas preguntas te ayudarán a definir el contenido, el estilo y el formato de tu documentación. Por ejemplo:

- Si tienes que alertar a tu público sobre una nueva amenaza, escribe rápidamente y pule el mensaje más tarde.
- Si el presupuesto y los plazos son ajustados, se puede optar por compartir un texto sencillo con las personas implicadas lo antes posible y pensar en un formato más bonito cuando se disponga de recursos.
- Si el público es numeroso y el tema es complejo, quizá sea útil hacer un vídeo corto con subtítulos.
- Si estás escribiendo instrucciones técnicas para gestoras de incidentes, debes incluir detalles técnicos y publicar la documentación en la misma plataforma en la que las gestoras de incidentes documentan los casos para que esté disponible (por ejemplo, un gestor de tickets).
- Si estás escribiendo documentación que puede ser utilizada por otras organizaciones de la sociedad civil, utiliza un lenguaje sencillo, fácil de traducir y publícala con una licencia y en un formato que permitan la reutilización.

### Plataformas y formatos para la documentación técnica
La herramienta que más utiliza tanto la industria informática como el movimiento que ofrece protección digital a la sociedad civil para elaborar documentación bajo los principios orientativos mencionados, es git, una tecnología para el control de versiones. En la mayoría de los casos, se utiliza con [Markdown](https://daringfireball.net/projects/markdown/), un lenguaje de marcado sencillo y un generador de sitios estáticos para cargar el contenido en un sitio web con capacidad de búsqueda y fácil de usar.

#### Git para control de versiones

Git es el software de control de versiones más utilizado para escribir documentación técnica de forma colaborativa. Su principal característica es que permite hacer un seguimiento de los cambios realizados en cada uno de los archivos de una carpeta, por lo que existe un registro de cada una de las ediciones. También permite revertir los cambios a una versión específica, si es necesario.

Git facilita la colaboración porque permite fusionar los cambios realizados por varias personas en una sola fuente. Otra característica útil de este software es la posibilidad de proteger la identidad de quienes colaboran gracias a la opción de crear repositorios privados a los que solo puede acceder un grupo seleccionado de personas usuarias. Además, permite informar sobre las incidencias, gestionar las personas que colaboran, asignar diferentes roles, documentar el proceso, acceder a las estadísticas, etc.

La documentación gestionada en repositorios git suele estar alojada en plataformas de terceros como [Github](https://github.com) o [Gitlab](https://gitlab.com), o en instancias autogestionadas de Gitlab. Algunos ejemplos de documentación desarrollada de forma colaborativa por la sociedad civil utilizando git son:

- [Documentación para la comunidad de la línea de ayuda en seguridad digital](https://communitydocs.accessnow.org/), que se encuentra en [este repositorio de Gitlab.com](https://gitlab.com/AccessNowHelpline/community-documentation).
- [Digital First Aid Kit](https://digitalfirstaid.org), que se encuentra en [este repositorio de Gitlab.com](https://gitlab.com/rarenet/dfak).
- [FemBloc](https://docs.fembloc.cat/).
- [SAFETAG - Security Auditing Framework and Evaluation Template for Advocacy Groups](https://safetag.org/), que se encuentra en [ese repositorio de Github.com](https://github.com/SAFETAG/SAFETAG).

Hay muchos recursos en línea para aprender a usar git. Busca hasta encontrar el que mejor se adapte a tus necesidades de aprendizaje. La guía [git - la guía sencilla](http://rogerdudler.github.io/git-guide/index.es.html) puede ser un buen punto de partida. Aunque git no es complejo para una persona que colabora de forma habitual, se requiere cierta experiencia para familiarizarse con su lógica y sus comandos.

#### Markdown para escribir

En todos los ejemplos anteriores, los documentos están escritos en [Markdown](https://daringfireball.net/projects/markdown/), un lenguaje de marcado ligero creado por Aaron Swartz y John Gruber en 2004 para permitir que la gente "escriba utilizando un formato de texto plano fácil de leer y de escribir, con la opción de convertirlo a un XHTML (o HTML) estructuralmente válido".

Los documentos de Markdown pueden convertirse a muchos formatos diferentes, lo que permite crear sitios web, aplicaciones para móviles, libros electrónicos y PDF a partir de la misma fuente.

Es importante recordar que, aunque la mayoría de los proyectos impulsados por organizaciones de la sociedad civil utilizan Markdown, para la documentación técnica se emplean otros lenguajes de marcado, en especial [AsciiDoc](https://asciidoc-py.github.io/index.html) y [reStructuredText (reST)](https://www.sphinx-doc.org/en/master/usage/restructuredtext/).

Si es la primera vez que utilizas Markdown, puedes tener a mano la guía de sintaxis como referencia:
- [Guía de Markdown](https://www.markdownguide.org/basic-syntax/).
- [Documentos de Github](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax).

#### Generadores de sitios estáticos para publicar sitios web

Para convertir Markdown en sitios web con capacidad de búsqueda, se suelen utilizar generadores de sitios estáticos como [Jekyll](https://jekyllrb.com/), [Gatsby](https://www.gatsbyjs.com/) o [Metalsmith](https://www.metalsmith.io/).

Los generadores de sitios estáticos (SSG, por sus siglas en inglés) son una alternativa a los sistemas de gestión de contenidos como WordPress o Drupal, donde el contenido se gestiona y almacena en una base de datos en el servidor web. Es decir, en lugar de obtener el contenido de una base de datos cada vez que hay una solicitud de contenido web, los SSG generan todo el sitio web después de cada actualización y crean un árbol de archivos HTML listo para ser visitado.

Una gran ventaja de esta infraestructura basada en git es que es relativamente sencilla de mantener. Los sitios estáticos son resistentes al troleo y a los frecuentes ataques que se dan en plataformas como los wikis (sobre todo si permiten la edición por parte de cualquier persona usuaria) u otras aplicaciones web o sitios dinámicos que requieren mucho trabajo para mantener la seguridad y asegurarse de que el contenido no se edita de forma maliciosa o por error.

### Documentación colaborativa

La utilización de una infraestructura de documentación en git facilita que cualquier otra línea de ayuda o persona que tenga acceso a ese repositorio git pueda utilizar la misma base de conocimientos para crear su propio sitio web, aplicación móvil, libro electrónico, etc., y también para recibir y enviar actualizaciones a la misma.

Esto es posible gracias a la propia arquitectura de los hubs de alojamiento en git, como Gitlab o Github, que permite [hacer una copia de un proyecto](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) y enviarle solicitudes de fusión (o pull) después de que se haya modificado en la copia o "fork".
Ante la escasez de recursos disponibles en el ámbito de la sociedad civil para crear documentación técnica que se actualice constantemente, colaborar en recursos de documentación técnica compartidos se ha convertido en una práctica habitual. Para ello, hay que evitar los formatos que no son fáciles de descargar y duplicar y que no están sujetos al control de versiones, como los wikis, los sitios web, los documentos alojados en Google Drive o los PDF, y utilizar licencias de contenido que permitan la colaboración y la creación de obras derivadas.

El sistema de colaboración también permite evitar la duplicación de esfuerzos, ya que los recursos existentes pueden reutilizarse en lugar de tener que empezar a escribir desde cero más de una vez.

### Guías de estilo

La documentación de los procedimientos técnicos de las LASDSC debe redactarse en un lenguaje fácil de leer e inclusivo, teniendo en cuenta que quienes gestionan los incidentes en general no hablan inglés como lengua materna y que ninguna persona es experta en todo, especialmente en el ámbito de la sociedad civil.

En general, es bueno aplicar algunas normas básicas que se recomiendan a cualquiera que escriba en lenguaje técnico:

- Usa frases cortas que suenen naturales y sean sencillas.
- Utiliza palabras comunes en la medida de lo posible, no utilices jerga o acrónimos a menos que sea realmente necesario (y en ese caso, explícalos al menos una vez).
- Recuerda que debes incluir a todos los géneros utilizando palabras y pronombres de género neutro.
- Utiliza la voz activa (sujeto + verbo + objeto) en la medida de lo posible.
- Las listas son un buen recurso para visualizar la información de forma rápida.
- Enlaza recursos externos útiles en caso de que la beneficiaria necesite más información sobre un tema.

Existen muchos recursos sobre cómo redactar una buena documentación técnica, aquí mencionamos tan solo unos pocos:

- Una lista de recursos de escritura técnica en [Google's Technical Writing Courses for Engineers](https://developers.google.com/tech-writing/resources).
- [Microsoft Style Guide](https://docs.microsoft.com/en-us/style-guide/welcome/).
- [Tips on Bias-Free Communication](https://docs.microsoft.com/en-us/style-guide/bias-free-communication).
- [Writing Step-by-Step Instructions](https://docs.microsoft.com/en-us/style-guide/procedures-instructions/writing-step-by-step-instructions).
