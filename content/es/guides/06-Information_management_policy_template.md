---
layout: post
title: "Plantilla de política de gestión de la información"
date: 2022-09-06
categories: templates
lang: es
order: 9
---

# Plantilla de política de gestión de la información
## [Nombre de la organización]
## Política de gestión de la información

### 1. Clasificación de la información

[Nombre de la organización] apoya el protocolo de semáforo para el intercambio de información.

Descripción de la clasificación de datos

- **PÚBLICA** – Esta información se considera no sensible (es decir, que no incluye datos personales ni detalles sobre colaboraciones con terceras partes y procedimientos internos) y puede transmitirse a cualquier persona en cualquier contexto. La información está destinada al consumo público. Es posible que ya se haya informado sobre ella o que esté disponible para ello.
- **CONFIDENCIAL** – La información con la etiqueta de confidencial puede compartirse con otros equipos de [Nombre de la organización], así como con terceras partes de confianza en función del principio de la necesidad de saber ("need-to-know"), es decir, únicamente si para abordar un caso específico esa información es imprescindible. Por defecto, la información solo se comparte entre el personal de [Nombre de la organización]. No se debe dar nunca por hecho que la información se comparte con terceras partes: de hecho, este tipo de información solo debe compartirse con terceras partes bajo el principio de la necesidad de saber y en tanto en cuanto hayan firmado un acuerdo de no divulgación que incluya unas normas mínimas sobre el almacenamiento y la conservación de datos ajustadas a la Política de conservación de [Nombre de la organización] (véase más abajo).
- **RESTRINGIDA: [grupo / entidad / lista de personas]** – Toda información con la etiqueta de restringida debe incluir también un grupo, entidad o lista de personas que no tengan restringido el acceso a la información. Cuando el acceso a la información está restringido a ciertos grupos o entidades, la pertenencia de cualquier persona a ese grupo o entidad implica que esa persona tiene acceso a la información. La información restringida también puede compartirse con terceras partes bajo el principio de necesidad de saber. De modo que, en los casos de información con la etiqueta "RESTRINGIDA: Equipo técnico de [Nombre de organización]", las otras partes pueden ser la persona beneficiaria, además de otra tercera parte que sea preciso incluir para poder resolver el caso. Alguna información es tan sensible que solo debe compartirse con personas que necesiten absolutamente conocerla. En ese caso, se etiquetará como "RESTRINGIDA: [lista de personas]". Esta información nunca se comparte con los grupos, por lo que la pertenencia a un grupo nunca concede acceso a esta clasificación de la información. Cuando se envía un correo electrónico, se considera personas que "necesitan saber" únicamente a las personas destinatarias y en ningún caso se debe compartir la información más allá.

| Color	| Clasificación	| Alcance	| Ejemplos |
|-------|----------------|--------|----------|
| ROJO	| RESTRINGIDA [lista de personas]	| Personas citadas	| <ul><li>solicitudes de carácter jurídico</li><li>cuestiones de confianza</li></ul> |
| ÁMBAR	| RESTRINGIDA [entidad]	| Integrante de [entidad] y terceras partes que necesitan saber |	<ul><li>solicitudes de personas beneficiarias</li><li>datos del historial del caso (que pueden incluir datos personales necesarios para la prestación de servicios)</li><li>contactos personales (si es necesario y está justificado por una solicitud de integrantes de la entidad y de terceras partes)</li><li>documentación sobre infraestructura interna</li><li>documentación sobre procesos internos</li></ul> |
| VERDE	| CONFIDENCIAL	| [Nombre de la organización] integrantes; terceras partes que necesitan saber | <ul><li>inteligencia sobre amenazas a la sociedad civil</li><li> documentación sobre elevaciones y procedimientos </li></ul> |
| BLANCO	| PÚBLICA	| Toda	| <ul><li>recomendaciones de seguridad general</li><li>entradas de blog</li><li>contenido de sitio web</li><li>entradas de redes sociales |

### 2. Protección de la información

- La **información pública** se puede difundir ampliamente, ya sea mediante boletines informativos, contenidos de sitios web, publicaciones en redes sociales, información en plataformas públicas de intercambio de información sobre malware, etc. La infraestructura de [Nombre de la organización] que aloja la información pública está reforzada y protegida contra el riesgo de daño a su integridad. Toda la información pública está respaldada con una copia de seguridad para evitar que se pierda.
- La **información confidencial** se almacena en plataformas a las que solo puede acceder el personal de [Nombre de la organización] y que pueden compartirse con terceras partes bajo el principio de la necesidad de saber. El acceso a estas plataformas está protegido por contraseña y requiere una autentificación de dos factores siempre que es posible. Este tipo de información también se almacena en dispositivos de trabajo con cifrado de disco completo. Este tipo de información solo se transfiere a través de canales de comunicación cifrados de extremo a extremo.
- La **información restringida**
    - La **información restringida a equipos específicos de [Nombre de la organización]** solo se almacena en plataformas protegidas con contraseña y en dispositivos de trabajo con cifrado de disco completo. Este tipo de información solo se transfiere a través de canales de comunicación cifrados de extremo a extremo.
    - La **información restringida a personas** solo se almacena en plataformas protegidas con contraseña y solo se transfiere a través de canales de comunicación cifrados de extremo a extremo.

Las claves privadas GPG del personal solo se almacenan en sus dispositivos portátiles con cifrado de disco completo o en soportes de almacenamiento externo totalmente cifrados.

Las medidas de seguridad establecidas para la protección de la información son normas de mínimos y, como están en constante evolución, pueden cambiar en el futuro.

### 3. Difusión de la información

Información que no puede ser compartida:

- La información entrante restringida a personas específicas no se compartirá más allá de las personas destinatarias citadas.

Limitaciones de esta política:

- La información puede ser compartida en cumplimiento de las obligaciones jurídicas nacionales e internacionales, incluida la respuesta a solicitudes de las autoridades policiales que obliguen a [Nombre de la organización] a presentar sus registros. [Nombre de la organización] se opondrá firmemente a los requerimientos judiciales u otras solicitudes que infrinjan los derechos humanos y empleará todos los medios a su alcance para proteger a su personal, personas y entidades beneficiarias y socias.

### 4. Acceso a la información

Los requerimientos judiciales por parte de las autoridades, las comunicaciones sobre cuestiones de confianza y otra información crítica pueden tener la etiqueta de información RESTRINGIDA, permitiendo el acceso solo a personas a nivel individual y únicamente en función de la necesidad de saber, hasta que sea posible rebajar el nivel de confidencialidad de esta información.

Cambiar la clasificación de los datos

La información que reciba la etiqueta de CONFIDENCIAL solo puede hacerse pública tras eliminar toda la información sensible o con la autorización explícita de las personas y grupos mencionados en dicha información. Si la información tiene la clasificación de confidencial solo por exclusividad de los derechos de publicación, pasará automáticamente a tener la clasificación de pública en el momento en que se publique.

### 5. Cooperación con otros equipos

Esta política define el proceso que sigue [Nombre de la organización] para cooperar formal o informalmente con otros CERT y equipos de respuesta de seguridad digital para la sociedad civil.

- [Nombre de la organización] puede compartir información con la etiqueta de "PÚBLICA" en cualquier foro al que tenga acceso un CERT o equipo de respuesta de seguridad digital para la sociedad civil.
- Si [Nombre de la organización] considera que la mejor acción a favor de una persona beneficiaria es colaborar con otro CERT específico o con un equipo de respuesta de seguridad digital para la sociedad civil, esas comunicaciones y los datos asociados deben clasificarse bajo las etiquetas de "CONFIDENCIAL" o "RESTRINGIDA: [lista de entidades]" (véase "Clasificación de la información" arriba).
- En estos casos, [Nombre de la organización] solicitará el permiso de la persona beneficiaria para tratar de resolver su caso a través de los servicios de otro CERT o equipo de respuesta de seguridad digital para la sociedad civil.
    - En algunos casos, [Nombre de la organización] no necesitará la autorización de la persona beneficiaria, pues dicha autorización puede darse por supuesta, por ejemplo:
        - Si la persona beneficiaria ha encargado al CERT o al equipo de respuesta de seguridad digital para la sociedad civil que retire un host de contenido de _phishing_ (conviene tener en cuenta que, antes de proceder a la resolución de estos casos, deben seguirse primero nuestros procesos sobre coordinación de inteligencia sobre amenazas y buscar las acciones más beneficiosas para el mayor número de partes interesadas en tales circunstancias);
        - Recuperación o desactivación de la cuenta (siempre que se haya verificado a la persona beneficiaria) con el CERT de la plataforma implicada;
        - Una queja por cierre, si ese cierre ha afectado al público en general.
- Circunstancias en las que se requiere obligatoriamente un permiso de la persona beneficiaria:
    - Desactivar un servidor de comando y control (C&C);
    - Análisis de malware, especialmente cuando se va a analizar un dispositivo;
    - Sortear la censura.

### 6. Conservación de registros
Toda la información compartida dentro de [Nombre de la organización] se almacena en los propios servidores de [Nombre de la organización], a los que se aplica la siguiente política:

#### Política de conservación

Toda la información de la infraestructura de [Nombre de la organización], incluida la información sobre amenazas, las solicitudes de las personas beneficiarias y entidades socias y la documentación interna, se almacena durante el tiempo que sea necesario en los servidores de [Nombre de la organización] con el fin de compartir la información y prestar los servicios, así como para cumplir con las obligaciones legales nacionales e internacionales, incluida la prevención de delitos penales y el curso de demandas civiles.

Toda la información de la infraestructura de [Nombre de la organización], excepto la información pública que no es sensible y no incluye ningún dato personal, se almacena en plataformas protegidas con contraseña y en dispositivos de trabajo con cifrado de disco completo. Este tipo de información solo se envía a través de canales de comunicación cifrados de extremo a extremo. Estas son las normas de mínimos vigentes, que, debido a que están en constante evolución, pueden cambiar en el futuro.

#### Política de filtración de datos

En caso de que se produzca una filtración de datos personales que pueda suponer un riesgo para los derechos y libertades de las personas a las que se refieren los datos, [Nombre de la organización] notificará a estas personas y a la autoridad de supervisión competente sin demora indebida y siempre que sea posible, a más tardar 72 horas después de haber tenido conocimiento de la filtración, de conformidad con el Reglamento General de Protección de Datos de la UE.

A la hora de tratar filtraciones de seguridad de datos, [Nombre de la organización] adoptará medidas para mitigar los daños, investigar, llevar a cabo medidas correctivas y cumplir con los requisitos normativos en materia de seguridad de la información.

### 7. Proceso de destrucción de datos

#### Documentos físicos

La impresión de documentos debe limitarse al mínimo. Los documentos impresos deben almacenarse únicamente el tiempo necesario y se recomienda no cruzar fronteras con documentos impresos confidenciales o de uso restringido.

Los documentos en papel que contengan información CONFIDENCIAL deben ser destruidos en una trituradora de tiras o de corte transversal, mientras que cualquier otro documento físico que contenga información RESTRINGIDA debe ser destruido con una trituradora de corte transversal.

#### Dispositivos de almacenamiento

Los discos duros, las memorias USB y otros dispositivos de almacenamiento portátiles que contengan información CONFIDENCIAL o RESTRINGIDA deben borrarse de forma segura con una única pasada del proceso de borrado: datos aleatorios (/dev/urandom) antes de eliminarlos. Los CD de una sola escritura deben romperse en pedazos o destruirse en la trituradora antes de tirarlos.

#### Datos digitales

Los archivos digitales que contengan información CONFIDENCIAL se pueden borrar directamente, mientras que para los datos RESTRINGIDOS se debe realizar como mínimo un proceso de borrado sobre el archivo.

### 8. Uso adecuado de los dispositivos de trabajo

Todo el personal de [Nombre de la organización] se asegura de que los dispositivos con los que acceden a información de [Nombre de la organización] estén protegidos con cifrado de disco completo y se utilicen con buen criterio, con actualizaciones periódicas del sistema y del software y otras medidas para evitar infecciones o accesos no autorizados al sistema y a las cuentas.

### 9. Política de comunicaciones y PGP de [Nombre de la organización]

La información "RESTRINGIDA" solo debe compartirse entre el personal de [Nombre de la organización] a través de canales con un alto grado de cifrado, como correos electrónicos cifrados con PGP, Signal o similares.

[Nombre de la organización] apoya las comunicaciones cifradas con PGP y se comunica a través de un canal cifrado.

Todo el personal de [Nombre de la organización] está obligado a utilizar PGP/GnuPG para cifrar todas las comunicaciones por correo electrónico con:

- otras personas del equipo
- terceras partes, cuando intercambien información confidencial y restringida

Se recomienda que los pares de claves PGP utilizados para comunicarse entre el personal de [Nombre de la organización] y con terceras partes tengan la siguiente configuración:

- Algoritmo: RSA
- Longitud de clave: 4096
- Fecha de caducidad: 5 años
- Clave privada protegida por una contraseña fuerte, compuesta por al menos 20 caracteres, incluyendo letras minúsculas y mayúsculas, números y símbolos o una frase de contraseña creada con el método _diceware_, con al menos 6 palabras.

En caso de que un dispositivo que contenga una clave privada PGP sea robado o de que un par de claves PGP se vea expuesto de otro modo, el par de claves será revocado lo antes posible y se notificará a la dirección de [Nombre de la organización].
