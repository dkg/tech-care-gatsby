---
layout: post
title: "Referencias"
date: 2022-09-06
categories: page
lang: es
order: 5
---

# Referencias

Access Now Digital Security Helpline (2018). _Documentation for FIRST Site Visit_. Confidencial.

Carnegie Mellon University (2004). _Creating and Managing Computer Security Incident Response Teams (CSIRTs)_. Carnegie Mellon University 1996-2004. [https://www.first.org/resources/papers/conference2004/t1_01.pdf](https://www.first.org/resources/papers/conference2004/t1_01.pdf).

Cichonski, Paul, Millar, Tom, Grance, Tim, & Scarfone, Karen (2021). _Computer Security Incident Handling Guide_. NIST. [https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-61r2.pdf](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-61r2.pdf).

Dufkova, Andrea (2020). _FIRST Site Visit Requirements and Assessment_. FIRST. [https://www.first.org/membership/site-visit-v3.1.pdf](https://www.first.org/membership/site-visit-v3.1.pdf).

Eguren, Enrique, Caraj, Marie (eds.) (2009). _Nuevo manual de protección para los defensores de derechos humanos_. Protection International. [https://www.protectioninternational.org/wp-content/uploads/2012/04/Nuevo_Manual_Proteccion.pdf]( https://www.protectioninternational.org/wp-content/uploads/2012/04/Nuevo_Manual_Proteccion.pdf).

ENISA (2006). _Cómo crear un CSIRT paso a paso_. [https://www.enisa.europa.eu/publications/csirt-setting-up-guide-in-spanish]( https://www.enisa.europa.eu/publications/csirt-setting-up-guide-in-spanish).

ENISA (2020). _How to set up CSIRT and SOC - Good Practice Guide_. [https://www.enisa.europa.eu/publications/how-to-set-up-csirt-and-soc](https://www.enisa.europa.eu/publications/how-to-set-up-csirt-and-soc).

FIRST (2006). _CERT-in-a-Box_. GOVCERT.NL/NCSC. [https://www.first.org/resources/guides/cert-in-a-box.zip](https://www.first.org/resources/guides/cert-in-a-box.zip).

FIRST (2019). _FIRST CSIRT Framework - Computer Security Incident Response Team (CSIRT) Services Framework_. [https://www.first.org/standards/frameworks/csirts/FIRST_CSIRT_Services_Framework_v2.1.0.pdf](https://www.first.org/standards/frameworks/csirts/FIRST_CSIRT_Services_Framework_v2.1.0.pdf).

Haché, Alexandra (2021). _Modelos de líneas de Atención Feministas orientadas a las Violencias Machistas Digitales_. Programa de Defensoras Digitales (Digital Defenders Partnership). [https://www.digitaldefenders.org/wp-content/uploads/2021/12/VMD_final.pdf](https://www.digitaldefenders.org/wp-content/uploads/2021/12/VMD_final.pdf).

Higson Smith, Craig, Ó Cluanaigh, Daniel, Ravi, Ali G., Steudtner, Peter (2016). _Holistic Security - A Strategy Manual for Human Rights Defenders_. Tactical Technology Collective. [https://holistic-security.tacticaltech.org/](https://holistic-security.tacticaltech.org/).

INHOPE (2020). _Establishing a hotline guide_, INHOPE, [https://inhope.org/EN/hotline-guide](https://inhope.org/EN/hotline-guide).

International Federation of Red Cross (2020) _Hotline in a Box_. IFRC. [https://www.communityengagementhub.org/wp-content/uploads/sites/2/2020/03/200325_Full-toolkit.pdf](https://www.communityengagementhub.org/wp-content/uploads/sites/2/2020/03/200325_Full-toolkit.pdf).

Organización Internacional para las Migraciones (2007). _The IOM Handbook on Direct Assistance for Victims of Trafficking_. OIM. [https://publications.iom.int/system/files/pdf/iom_handbook_assistance.pdf](https://publications.iom.int/system/files/pdf/iom_handbook_assistance.pdf).

Kral, Patrick (2021). _Incident Handler's Handbook_. SANS Institute. [https://www.sans.org/reading-room/whitepapers/incident/incident-handlers-handbook-33901](https://www.sans.org/reading-room/whitepapers/incident/incident-handlers-handbook-33901).

Maxigas (2014). Hacklabs and Hackerspaces: Shared Machine Workshops. _Technological Sovereignty Vol. 1_. Passerelles 11. [https://www.coredem.info/rubrique48.html](https://www.coredem.info/rubrique48.html).

Stratten, Kate, & Ainslie, Robert (2003). _Field Guide: Setting Up a Hotline. Field Guide_. Johns Hopkins Bloomberg School of Public Health - Center for Communication Programs. [https://pdf.usaid.gov/pdf_docs/PNACU541.pdf](https://pdf.usaid.gov/pdf_docs/PNACU541.pdf).

Tsunga, Arnold (ed.) (2007). _Protection Handbook for Human Rights Defenders_. Front Line Defenders. [https://www.frontlinedefenders.org/fr/file/1671/download?token=XHaqzSCK](https://www.frontlinedefenders.org/fr/file/1671/download?token=XHaqzSCK).

Fondo de Población de las Naciones Unidas (2020). _Guía técnica de servicios remotos: Atención psicosocial especializada para sobrevivientes de violencia basada en género_. UNFPA. [https://lac.unfpa.org/sites/default/files/pub-pdf/unfpa_guiavbg_web_1.pdf](https://lac.unfpa.org/sites/default/files/pub-pdf/unfpa_guiavbg_web_1.pdf).

Van der Heide, Martijn (2017). _Establishing a CSIRT_. ThaiCERT, ETDA. [https://www.thaicert.or.th/downloads/files/Establishing_a_CSIRT_en.pdf](https://www.thaicert.or.th/downloads/files/Establishing_a_CSIRT_en.pdf).

West-Brown, Moira J., Stikvoort, Don, Kossakowski, Klaus-Peter, Killcrece, Georgia, Ruefle, Robin, & Zajicek, Mark (2003). _Handbook for Computer Security Incident Response Teams (CSIRTs)_. Carnegie Mellon University. [https://resources.sei.cmu.edu/asset_files/Handbook/2003_002_001_14102.pdf](https://resources.sei.cmu.edu/asset_files/Handbook/2003_002_001_14102.pdf).

Zimmerman, Carson (2014). _Ten Strategies of a World-Class Cybersecurity Operations Center_. MITRE. [https://www.mitre.org/sites/default/files/publications/pr-13-1028-mitre-10-strategies-cyber-ops-center.pdf](https://www.mitre.org/sites/default/files/publications/pr-13-1028-mitre-10-strategies-cyber-ops-center.pdf).
