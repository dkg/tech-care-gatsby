---
layout: post
title: "Plantilla de marco de trabajo"
date: 2022-09-06
categories: templates
lang: es
---

# Nombre de la organización
# Plantilla de marco de trabajo

## 1. Nuestro público

- ¿Quiénes son las beneficiarias de su LASDSC? Especificar actividad, ámbito, sexo, edad, etc. Cuanto más precisa sea la definición del público, mayor será la capacidad de identificar sus necesidades y aumentar la calidad del servicio prestado.

- ¿Cuál es el ámbito geográfico de su público?

- ¿Existen otras LASDSC que presten servicios al mismo público en su región? En tal caso, ¿qué necesidades no atendidas cubrirá su LASDSC?    

## 2. Las necesidades de nuestro público

Si deciden, por ejemplo, apoyar a comunidades indígenas de una determinada región, se puede hacer un análisis DAFO para analizar las características. Por ejemplo:

||Útil | Dañino |
| --- | --- | --- |
| **Interno** | ***Fortalezas*** <br> _Están muy bien organizadas y tienen acceso a una amplia variedad de recursos._ | ***Debilidades*** <br> _La estructura vertical centraliza la toma de decisiones._ |
| **Externo** | ***Oportunidades*** <br> _ Muchas agencias de cooperación internacional están destinando fondos a comunidades indígenas. | ***Amenazas*** <br> _Aumento de la criminalización de las comunidades indígenas._ |

Ahora intenten usar la misma tabla para sus beneficiarias:

||Útil | Dañino |
| --- | --- | --- |
| **Interno** | ***Fortalezas*** <br> _Describan las fortalezas de su público_ | ***Debilidades*** <br> _Describan las debilidades de su público_ |
| **Externo** | ***Oportunidades*** <br> _Describan las oportunidades de su público_ | ***Amenazas*** <br> _Describan las amenazas que afectan a su público_ |

Pueden complementar el marco DAFO con un análisis PESTEL del contexto en el que se mueve su público.

| Público | Político | Económico | Sociocultural | Tecnológico | Ecológico | Legal |
| --- | --- | --- | --- | --- | --- | --- |
| _Comunidades indígenas de X región_ | ... | ... | ... | ... | ... | ... |

### Nuestro modelo de amenazas

#### Matriz de amenazas

| Probabilidad / Impacto | Bajo | Medio | Alto |
| --- | --- | --- | --- |
| **Probable** | ... | ... | ... |
| **Poco probable** | ... | ... | ... |
| **Improbable** | ... | ... | ... |

#### Inventario de amenazas

Completen una tabla para cada amenaza identificada en la matriz de amenazas.

| Título |   |
| --- | --- |
| **Descripción**| _Breve caracterización de la amenaza._|

<br />

| Qué | Objetivo | Atacante | Cómo | Dónde |
| --- | --- | --- | --- | --- |
| _Los efectos que causaría la amenaza_ | _Qué o quién es el objetivo_ | _¿Quién creen que está detrás de la amenaza?_ | _Los medios por los que la amenaza puede llegar a materializarse_ | _¿Cuáles son los espacios físicos en los que se puede manifestar la amenaza?_ |
| ... | ... | ... | ... | ... |

## 3. Nuestra misión

¿Cuáles son los objetivos de la línea de ayuda? En la declaración de misión debe definirse el público al que se dirigen, la situación que se quiere mejorar y la forma en que piensan hacerlo, así como los servicios que prestará la línea de ayuda.

<br />

| Nombre de la organización - Misión |
|-----------------------------|
| *Describan su misión aquí:* <br /><br /> .........<br /> .........<br /> ......... <br /> ......... |



## 4. Diseño organizacional

- ¿Formará parte de una organización más grande o se trata de un proyecto independiente?

- ¿Será un equipo de personas voluntarias o se contratará al personal?

- ¿Cómo se financiará la LASDSC?


## 5. Servicios básicos

| Tipo de servicio | Servicio | Requisitos |
| --- | --- | ---- |
| _Reactivo_ | _Reposición de equipo_ | _Acceso a financiación. Quizá podamos recuperar equipos antiguos para emergencias._ |
| _Preventivo_ | _Formación presencial en seguridad digital_ | _Es preciso encontrar personas que puedan impartir las formaciones en la zona donde se realizarán._ |
| ... | ... | ... |
| ... | ... | ... |
| ... | ... | ... |

## 6. Comunicación con su público

### Decidan cómo se puede poner en contacto su público

| Canal | Ventajas | Desventajas | Accesibilidad para nuestro público | ¿Vamos a utilizarlo? |
| --- | --- | --- | --- | --- |
| _Formulario en el sitio web_| _Fácil de instalar. Se puede encriptar_ | _Riesgo de spam_ | _Accesible_ | _Sí_ |
| _Teléfono_ | _Todo el mundo puede acceder a un teléfono para llamarnos_ | _Las tarjetas SIM tienen que estar registradas con una ID._ | _Accesible_ | _No_ |
| ... | ... | ... | ... | ... |
| ... | ... | ... | ... | ... |
| ... | ... | ... | ... | ... |

### Declaren su disponibilidad y tiempo de respuesta

- Horario de atención:

- ¿Cómo se atenderán las solicitudes de apoyo fuera del horario de atención?

- ¿Cómo evitará la LASDSC el desgaste del equipo de asistencia?


### Decidan cómo comunicarse con su público

- ¿El personal que responda las llamadas tendrá un seudónimo individual o utilizarán uno colectivo?

- ¿Lleva siempre una misma persona la comunicación con la persona implicada en un caso? Y si es compartida, ¿se mantiene la conversación siempre bajo el mismo seudónimo o se cambia con cada persona?

- ¿La LASDSC utilizará un tono informal o se mantendrá una distancia?
    

## 7. Políticas

| Política | Descripción | Desarrollo e implementación | Responsable | Fecha  |
| --- |  --- |  --- |  --- |  --- |
| _1. Política de gestión de la información_ | _Procedimientos para gestionar y proteger la información._ |  _Sí_ |  ... | ... |
| _2. Plan de respuesta a incidentes_ | _Hoja de ruta para implementar la capacidad de respuesta a incidentes de la LASDSC._  |  _Sí_ |  ... | ... |
| _3. Política de verificación_ |  _Pasos para verificar a nuevas beneficiarias._ |  Sí |  ... | ... |
| _4. Código de conducta_ |  _Descripción del comportamiento que se espera de las operadoras._ |  _Sí, pero en una segunda fase_ |  ... | ... |
| _5. Procedimientos estándar de funcionamiento_ |  _Pasos para responder las solicitudes, realizar derivaciones, etc._ | _Sí_ |  ... | ... |
| _6. Política de financiación_ |  ... |  ... |  ... | ... |
| _7. ..._ |  ... |  ... |  ... | ... |
