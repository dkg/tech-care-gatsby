---
layout: post
title: "Plantilla de proceso de verificación"
date: 2022-09-06
categories: templates
lang: es
order: 10
---

# Plantilla de proceso de verificación

## [Nombre de la organización]
## Proceso de verificación

### Objetivo de la verificación

El objetivo de verificar a las personas beneficiarias es tratar de reducir el riesgo para [Nombre de la organización] y para las personas usuarias en situación de riesgo.

Algunos de los riesgos que mitiga la verificación son el riesgo para [Nombre de la organización] de que su reputación se vea perjudicada por trabajar con organizaciones que no defienden los derechos humanos básicos o que son controvertidas por cualquier otro motivo. También existe el riesgo de que entes atacantes nos manipulen para que divulguemos información o para que permitamos que entren en nuestras plataformas, lo que les permitiría perpetrar ataques eficaces contra nuestras actividades. Asimismo, se corre el riesgo de que consuman nuestros recursos en incidentes falsos, privando así a personas y organizaciones que realmente necesitan nuestra ayuda de esa posibilidad.

La verificación es un ejercicio de diligencia debida con las personas a las que asistimos para asegurarnos de que realmente forman parte del público al que nos dirigimos.

Para asegurarnos de que este proceso de verificación queda debidamente registrado, todas las comunicaciones requeridas para completar el proceso de verificación se graban en orden cronológico en el historial del caso.


### Proceso de verificación de [Nombre de la organización]

El proceso utilizado para verificar a todas las nuevas beneficiarias consta de los siguientes pasos:

1. Evaluación inicial
2. Identificar/contactar a las posibles personas/entidades verificadoras
3. Evaluación de respuestas
4. Validación y registro


#### 1. Evaluación inicial

Se puede realizar una investigación preliminar a través de fuentes de información como Google, Wikipedia, el propio sitio web de la persona solicitante, Whois, servidores de claves PGP, etc. para comprobar la veracidad de la organización y la persona en cuestión. Ninguna de estas fuentes por sí sola debe considerarse fiable, pero su combinación permite hacerse una idea de la legitimidad de la organización/persona.

#### 2. Identificar/contactar a posibles verificadoras

El siguiente paso es identificar a posibles verificadoras. Es preciso localizar a una persona que ya conozcamos y en quien confiemos y que esté dispuesta a avalar a la posible nueva beneficiaria.

Un buen punto de partida es buscar en el sitio web de la organización, especialmente en secciones donde figuran las personas que integran su junta directiva, que suelen ser personas de alto perfil en el ámbito de las ONG y a menudo conocidas por el personal de [Nombre de la organización] o por sus organizaciones socias, por lo que es una forma excelente de localizar a posibles avalistas.

#### 3. Evaluación de las respuestas

Lo que intentamos comprobar es que la persona beneficiaria es quien dice ser, y que actúa de forma racional, segura y respetuosa hacia otras personas. Es importante que la beneficiaria esté en condiciones de respetar la reputación de [Nombre de la organización] si queremos que la organización participe prestando su ayuda. En gran medida, esto es lo que intentamos evaluar.

No se trata de una decisión definitiva sobre la "idoneidad", ya que la naturaleza de las relaciones de confianza siempre es un tanto subjetiva. Sin embargo, como norma general, podemos estimar que si alguien en quien confiamos implícitamente avala a una nueva beneficiaria, podemos considerar que está verificada. Si no logramos encontrar a nadie en quien confiemos implícitamente, para poder dar el visto bueno al proceso de verificación será necesario que dos personas conocidas de cuya reputación nos fiemos avalen a la nueva beneficiaria.

#### 4. Validación y registro

Cada proceso de verificación debe ser validado por ([funciones de las integrantes del equipo encargadas de validar los procesos de verificación en la organización]).

El hecho de que la beneficiaria haya pasado por el proceso de verificación y haya sido rechazada o aceptada se registra en el historial del caso.
