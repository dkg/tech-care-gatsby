---
layout: post
title: "Plantilla de código de conducta"
date: 2022-09-06
categories: templates
lang: es
---

# Plantilla de código de conducta
## [Nombre de la organización]
## Código de conducta

Este código de conducta se aplica a todos los espacios de [Nombre de la organización], tanto si se trata de interacciones en línea como de espacios de trabajo presenciales, eventos asociados o reuniones sociales. El personal y las personas voluntarias son responsables de conocer los valores que defiende [Nombre de la organización] que se describen en este documento, y de respetar las normas que se citan a continuación.

La misión de [Nombre de la organización] es [descripción de la misión de la organización]. [Nombre de la organización] se compromete a proporcionar un entorno seguro y acogedor para llevar a cabo esta misión. En especial, queremos erradicar la vergüenza o el estigma que rodea los errores de seguridad digital o _hacking_, por lo que animamos a todas las personas implicadas a abordar las interacciones con una actitud abierta, de escucha y de apoyo y a implicarse de forma constructiva con las demás personas en todo momento.

Más concretamente, todas las partes de [Nombre de la organización] se comprometen a promover los siguientes valores:

- **Confidencialidad:** Trataremos toda la información que recibamos de forma confidencial y no la revelaremos a terceros sin consentimiento. Trataremos la información entrante de forma responsable y la protegeremos contra la divulgación involuntaria a partes no autorizadas. El grado de seguridad de los métodos de almacenamiento y transmisión de la información dentro o fuera de [Nombre de la organización] se adecuará a la sensibilidad de la misma. Invitamos a todo el personal y a las personas voluntarias a que lean la política de [Nombre de la organización] sobre cómo se debe clasificar, almacenar, compartir y destruir la información.

Cualquier coordinación remota o iniciativa en línea se llevará a cabo a través de canales seguros que funcionen con software libre y de código abierto y, especialmente, si no están cifrados de extremo a extremo, serán gestionados y alojados por terceras partes de confianza, idealmente por la propia [nombre de la organización]. Se evitarán las herramientas comerciales o propietarias, especialmente si tienen un historial de violación de la privacidad.

- **Colaboración:** Tenemos un compromiso firme con el fomento de la solidaridad, la conexión, la cooperación y el sentido de comunidad en nuestros espacios.

- **Inclusividad:** Creemos en la importancia de la diversidad para favorecer la no discriminación, la libre expresión, la participación y la igualdad.

- **No-Hagas-Daño:** Somos conscientes de cómo nuestras acciones, comportamientos y formas de comunicación pueden tener un efecto positivo o negativo sobre las personas que nos rodean y tratamos de mitigarlo en la medida de lo posible. Somos conscientes de los factores que afectan nuestra propia situación de poder y reconocemos estas estructuras en los espacios de [Nombre de la organización]. El objetivo de [Nombre de la organización] es ofrecer una experiencia libre de acoso para todas las personas, independientemente de su género, identidad y expresión de género, edad, orientación sexual, capacidad, apariencia física, tamaño corporal, raza, etnia, religión (o ausencia de la misma), elecciones, habilidades o nivel de conocimiento tecnológico. No toleramos el acoso en ninguna de sus formas. Cualquier persona que viole este código de conducta puede ser sancionada o expulsada de estos espacios a discreción de [Nombre de la organización].

### Acoso

El acoso puede producirse en línea o de forma presencial. Ejemplos de comportamiento inaceptable:

1. Comentarios ofensivos que refuerzan las estructuras sociales de dominación o hacen alusión al género, la identidad y expresión de género, orientación sexual, discapacidad, enfermedad mental, neuro(a)tipicidad, apariencia física, tamaño corporal, edad, raza o religión.
2. Comentarios ofensivos y guerras de _flames_ sobre las elecciones de otras personas en cuanto a prácticas, destrezas, procedimientos y herramientas recomendadas.
3. Comentarios desagradables sobre las elecciones y prácticas de estilo de vida de una persona, entre otras las relacionadas con la alimentación, la salud, la crianza, las drogas y el empleo.
4. La utilización deliberada de un género inapropiado o el uso de nombres "muertos" o rechazados.
5. Imágenes o comportamientos sexuales gratuitos o fuera de lugar en espacios donde no son apropiados.
6. Contacto físico y contacto físico simulado (por ejemplo, descripciones textuales como "abrazo" o "caricia en la espalda") después de que se haya solicitado que se pare. Amenazas de violencia.
7. Incitación a la violencia hacia cualquier persona, lo que incluye animar a una persona a que se suicide o autolesione.
8. Intimidación deliberada.
9. Acosar o perseguir a alguien.
10. Usar fotos o videos para acosar, incluido el registro de la actividad en línea con fines de acoso.
11. Interrumpir de forma constante debates, charlas u otros eventos.
12. Atención sexual o contacto físico no deseados.
13. Conductas de contacto social inapropiado, como solicitar/asumir un nivel de intimidad inapropiado con otras personas.
14. Persistencia de una comunicación individual después de que se haya pedido que cese.
15. La divulgación deliberada de algún aspecto de la identidad de una persona sin su consentimiento, excepto cuando sea necesario para proteger a personas vulnerables de abuso intencionado.
16. La publicación de comunicación privada no acosadora.
17. Publicar información privada de otra persona, como su dirección física o electrónica, sin permiso explícito.
18. Defender o fomentar cualquiera de las conductas anteriores.
19. Introducir alguna droga en la comida o bebida.
20. Violar la política de privacidad de un evento con el fin de generar atención negativa hacia una persona que participa.
21. Solicitar la ayuda de otras personas, ya sea presencial o en línea, para atacar a alguien. Priorizamos la seguridad de las personas marginadas frente a la comodidad de las privilegiadas.


Nuestro equipo no actuará ante denuncias por:

- Los ismos "inversos", como el "racismo inverso", el "sexismo inverso" y la "cisfobia".
- Comunicación razonable sobre límites, como: "déjame en paz", "vete" o "no voy a hablar contigo de esto".
- Comunicación en un "tono" que no te parece agradable.
- Críticas de comportamientos o suposiciones racistas, sexistas, cisexistas o de otro tipo.

Permitir que una persona abandone una conversación que le resulte incómoda y no seguir a quien haya pedido que se le deje sola. Si se tratan temas difíciles, que pueden avivar un trauma para las personas participantes, es importante no olvidarse de advertirlo para que estas puedan abandonar la conversación o prever estrategias para afrontarla.

### Denuncias

Si estás sufriendo acoso, observas que otra persona está siendo acosada o tienes cualquier otra preocupación, notifícanoslo enviando un correo electrónico a [dirección de correo electrónico específica]. Actualmente, hay [n.] personas que reciben estos correos electrónicos: [nombres]. Las denuncias son confidenciales. No se pedirá que tomes ninguna medida que te produzca inseguridad.

Este código de conducta se aplica a los espacios de [Nombre de la organización], pero si una persona que participa en [Nombre de la organización] te acosa fuera de nuestros espacios, también queremos saberlo. Nos tomaremos en serio todas las denuncias de acoso de buena fe (tanto el acoso fuera de nuestros espacios como el que haya tenido lugar en cualquier momento).

El equipo de respuesta se pondrá en contacto con la persona acusada para informarle del proceso y ofrecerle la oportunidad de responder. El equipo de respuesta se reserva el derecho de excluir a personas de [Nombre de la organización] en función de su comportamiento pasado, lo que incluye el comportamiento fuera de los espacios de [Nombre de la organización]. Respetaremos las solicitudes de confidencialidad con el fin de proteger a las víctimas de abusos. Queda a nuestra discreción la decisión de nombrar públicamente a una persona sobre la que hayamos recibido denuncias de acoso o de advertir en privado a terceros sobre ella si creemos que hacerlo aumentará la seguridad de las socias o de quienes forman parte de [Nombre de la organización]. No nombraremos a las víctimas de acoso sin su consentimiento expreso.

El acoso y otras violaciones del código de conducta merman el valor de nuestra comunidad. Queremos que te sientas a gusto en ella, ya que gracias a gente como tú es un lugar mejor. Si la persona que te acosa forma parte del equipo de respuesta o de la dirección de [Nombre de la organización], se apartará de la gestión de tu incidente. Responderemos tan pronto como podamos.

### Consecuencias

Se espera que cualquier integrante del personal o del voluntariado a quien se le pida que cese un comportamiento de acoso lo haga inmediatamente. Si alguien en [Nombre de la organización] incurre en un patrón de acoso, el equipo de respuesta puede tomar las medidas que considere oportunas, que pueden incluir el despido o la denuncia pública.

### Licencia

Esta política está autorizada bajo la licencia Creative Commons Zero. Es de dominio público, no se requiere ningún permiso ni licencia abierta de su versión. Esta basada en el [ejemplo de política de la wiki de Geek Feminism](https://geekfeminism.fandom.com/wiki/Community_anti-harassment/Policy), creada por la [comunidad de Geek Feminism](https://geekfeminism.fandom.com/wiki/Geek_Feminism_Wiki).
