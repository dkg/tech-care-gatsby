---
layout: post
title: "Elabora un plan realista"
date: 2022-09-06
categories: guide
lang: es
order: 1
---

# 2. Elabora un plan realista

![collage suculentas documentos](/chap_2.png)

Una vez definido el marco de trabajo de la LASDSC, es el momento de establecer un plan general que describa cómo llevarlo a cabo, es decir, definir los aspectos materiales. Este plan debe incluir la elaboración de un presupuesto que cubra todas las necesidades operativas: desde el personal, el hardware y el software hasta el alquiler de una oficina si es necesario.

Es fundamental establecer cómo se van a conseguir los recursos para cubrir este presupuesto. ¿Se aceptarán fondos de entidades gubernamentales y privadas? ¿Se financiará a través de un _crowdsourcing_? ¿Se intentarán establecer acuerdos con instituciones públicas? La forma en que la LASDSC logre la sostenibilidad económica estará condicionada en gran medida por el entorno institucional definido en el marco de trabajo.

En esta fase es importante definir qué herramientas utilizará la LASDSC, especialmente para el seguimiento de los casos. Hay muchos gestores de tickets distintos, con características, costes y requisitos específicos. Hay que identificar cuál es el que mejor se adapta a las necesidades de la LASDSC. En este capítulo hacemos una comparación de los más populares para que puedan tomar una decisión informada sobre cuál les conviene.

Obviamente, el plan debe incluir una estrategia para proteger al equipo, tanto en el plano digital como en el físico. Hemos incluido algunas directrices básicas en este capítulo, aunque recomendamos encarecidamente contratar a una persona especializada para crear un plan de seguridad riguroso, especialmente si han decidido montar una oficina física y autogestionar su infraestructura.

Por último, es preciso tener una reflexión sobre cómo crear y cuidar al equipo. A la hora de planificar la LASDSC, hay que definir qué competencias básicas debe tener el personal del equipo para prestar los servicios que se quieren ofrecer: ya sea contratando a las personas adecuadas o formando al personal una vez que se hayan incorporado al equipo. También habrá que reflexionar sobre sus funciones y responsabilidades y sobre cómo cuidar su bienestar.

## 2.1 Crea un presupuesto

El coste de la línea de asistencia dependerá de las decisiones que se hayan tomado, especialmente las que tienen que ver con la estructura organizacional, el tiempo de respuesta y los servicios que se vaya a ofrecer que impliquen costes adicionales (por ejemplo, si se van a ofrecer ayudas para reemplazar equipos robados o pagar la formación del personal).

El presupuesto básico inicial, que servirá para crear la LASDSC y empezar a ofrecer servicios debería cubrir como mínimo:

1. Los sueldos iniciales del personal, a menos que se trate de una línea de atención sostenida completamente por trabajo voluntario.
2. Costes de hardware y software: dispositivos de trabajo para el personal, equipos de oficina (si va a haber una oficina), licencias de software, etc.
3. Coste de los servicios en línea, como el sitio web, una plataforma de intercambio de archivos, un gestor de tickets, etc.
4. Sueldos u honorarios de asesores jurídicos, implementación de servicios, etc.
5. Formación del personal.
6. Alquiler de la oficina (en caso de que se decida trabajar desde una oficina).

Se puede elaborar un presupuesto de mínimos, que incluya lo imprescindible para que sea viable desde el punto de vista operativo y un presupuesto de máximos, en caso de que se consigan todos los recursos necesarios.

Este presupuesto debe revisarse una vez finalizada la fase de diseño, con todas las decisiones operativas tomadas.

### Para saber más

- Lee más sobre buenas prácticas para crear un presupuesto en ENISA (2020). “High-level roadmap and budget”. ENISA, _How to set up CSIRT and SOC - Good Practice Guide_ (pp. 16-18). https://www.enisa.europa.eu/publications/how-to-set-up-csirt-and-soc

## 2.2 Decide cómo se financiará tu LASDSC

Para garantizar que la LASDSC pueda trabajar a largo plazo y convertirse en un punto de referencia para sus beneficiarias, será preciso desarrollar un modelo de financiación fiable que cubra los costes.

Si la LASDSC forma parte de una organización más grande, probablemente pueda recurrir a la organización anfitriona para estudiar la forma de cubrir los costes. Pero si es una organización independiente, conviene pensar en las siguientes cuestiones:

- ¿De dónde procederán los fondos? ¿Se solicitarán subvenciones, se pedirán donaciones, la financiará un consorcio de organizaciones o se cubrirán los gastos ofreciendo algunos servicios de pago?
- ¿Habrá que buscar varias fuentes de financiación o bastará con una para garantizar el funcionamiento a largo plazo?
- ¿Cuán seguras son sus fuentes de financiación?
- ¿La fuente de financiación es estable o terminará en algún momento?
- ¿Se limitará a buscar recursos financieros o tratará de establecer relaciones de colaboración con organizaciones e instituciones asociadas?

> Los retos que hemos detectado hasta ahora están relacionados con la sostenibilidad, porque la línea de ayuda es un proyecto financiado. ¿Y qué pasa si no hay financiación? ¿Cómo la mantenemos? ¿Cómo la gestionamos? Para ello, hemos creado algunos recursos para que la gente pueda acceder a ayuda. Uno de ellos es un portal de juristas y profesionales que pueden ofrecer servicios jurídicos gratuitos a mujeres que sufren acoso en línea, por ejemplo.

> _Digital Rights Foundation (Haché, 2021)._

### Política de financiación

Algunas organizaciones también tienen una política de financiación para determinar si una nueva fuente de financiación es coherente con su misión y objetivos. Por ejemplo, algunos grupos deciden recibir únicamente donaciones para seguir siendo completamente independientes, mientras que otros no aceptan fondos que puedan influir en sus prioridades.

Algunos ejemplos de políticas de financiación de miembros de CiviCERT:

#### Access Now

La mayor parte del apoyo que recibimos procede de fundaciones y agencias de desarrollo, el resto es de empresas, tribunales, particulares y organizaciones de la sociedad civil. Para garantizar la independencia y la integridad de nuestra organización, aceptamos apoyo sujeto a las siguientes condiciones no negociables:

- Access Now no acepta financiación que ponga en riesgo a su personal, a sus socias, a las comunidades que apoya o a su misión.
- Access Now no acepta financiación que ponga en peligro la relación con sus socias, partes interesadas o comunidades y redes que apoya.
- Access Now no acepta financiación que comprometa su independencia organizativa, como relaciones con fondos que puedan influir en sus prioridades, posturas políticas, iniciativas de incidencia política, regiones de actuación o acción directa.
- Access Now no acepta financiación que plantee un riesgo para su reputación en general o en lo que respecta a áreas de trabajo específicas.

#### Amnistía Internacional

La inmensa mayoría de nuestros ingresos procede de las donaciones de personas de todo el mundo. Estas donaciones personales y no afiliadas permiten que Amnistía Internacional (AI) mantenga una independencia plena de todo gobierno, ideología política, interés económico o religión.
Ni solicitamos ni aceptamos de gobiernos o partidos políticos fondos para nuestra actividad de investigación en derechos humanos y solo aceptamos el apoyo de empresas que han sido objeto de escrutinio previo. Una recaudación de fondos ética, principalmente de donaciones de personas particulares, es lo que nos permite mantenernos firmes e inquebrantables en nuestra defensa de derechos humanos universales e indivisibles.
El movimiento global de Amnistía está compuesto por una red de Secciones nacionales y el Secretariado Internacional.

### Para saber más

- [Política de financiación de Access Now](https://www.accessnow.org/financials/)
- [Política de financiación de Amnistía Internacional](https://www.amnesty.org/es/how-were-run/finances-and-pay/)

## 2.3 Seguridad física del personal y las oficinas

Tanto si decides montar una oficina física como si no, debes elaborar un plan para proteger al personal, a las personas voluntarias, la oficina y la infraestructura de posibles ataques físicos.

A la hora de plantear la seguridad física de la oficina, se debe tener en cuenta, por ejemplo:

- La protección del hardware que contiene información sensible: ordenadores, discos duros externos, servidores, etc.
- Asegurarse de que nadie accede a la oficina sin permiso. Por ejemplo, instalando una cámara de seguridad en la entrada de la oficina o concienciando al equipo sobre quién puede acceder a su lugar de trabajo y quién no.
- Proteger las impresoras y los documentos impresos de accesos no autorizados y destruir los documentos impresos que ya no se utilicen.
- Asegurar los _routers_ y la infraestructura de comunicaciones.
- Recordar al equipo que nunca debe dejar sus ordenadores sin supervisión. Y si lo hacen, deben dejarlo siempre bloqueado.

### Para saber más

- Pueden encontrar recomendaciones de seguridad física para su oficina o para las personas que trabajen de forma voluntaria desde su casa en [Access Now Helpline Security Policy Templates](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/tree/master/templates/organisational_Security_Policies-Templates).
- Higson Smith, Craig, Ó Cluanaigh, Daniel, Ravi, Ali G., Steudtner, Peter (2016). _Holistic Security - A Strategy Manual for Human Rights Defenders_. Tactical Technology Collective. https://holistic-security.tacticaltech.org.
- Eguren, Enrique, y Caraj, Marie (eds.) (2009). _Nuevo manual de protección para los defensores de derechos humanos_. Protection International. https://www.protectioninternational.org/wp-content/uploads/2012/04/Nuevo_Manual_Proteccion.pdf.
- Tsunga, Arnold (ed.) (2007). _Protection Handbook for Human Rights Defenders_. Front Line, The International Foundation for the Protection of Human Rights Defenders. https://www.frontlinedefenders.org/fr/file/1671/download?token=XHaqzSCK.

## 2.4 Seguridad de la red

Si el equipo trabaja en una oficina, debe tener una red exclusiva para conectarse a Internet, así como a las impresoras, los servidores físicos, etc. Si se quiere dejar que las visitas utilicen el WiFi, es preciso crear una red separada para ello.

Si es posible, valoren la posibilidad de contratar a una persona administradora de sistemas para que gestione la red de su oficina, así como su propia infraestructura, si han decidido autogestionar el alojamiento de sus herramientas y plataformas en línea.

## 2.5 Infraestructura y herramientas

En teoría, se puede crear una LASDSC con tan solo un ordenador por integrante del equipo y una plataforma de colaboración segura, como una instancia autoalojada de NextCloud, Gitlab o Discourse, para compartir información entre el equipo, organizar las tareas, hacer un seguimiento de los contactos, etc. Incluso se puede recurrir a un [proveedor de servicios afín](https://communitydocs.accessnow.org/282-Secure_file_sharing_storage.html) para que aloje dicha plataforma en un lugar compatible con el modelo de amenazas de la LASDSC.

Si el presupuesto lo permite, al planificar su línea de atención deben plantearse el uso de herramientas creadas explícitamente para la gestión de incidentes, la identificación de indicadores de compromiso, el intercambio de información y el análisis de programas maliciosos.

Muchas LASDSC utilizan las siguientes herramientas para gestionar y analizar incidentes:

- Un gestor de tickets para gestionar los casos y solicitudes de apoyo (véase más abajo la sección _Sistemas de gestión de tickets_).
- Una instancia autogestionada o federada de [MISP](https://www.misp-project.org/) para compartir, almacenar y correlacionar indicadores de compromiso.
- Un sistema de análisis de malware como [Cuckoo Sandbox](https://cuckoosandbox.org/).
- A veces, un CRM para la gestión de contactos como [CiviCRM](https://civicrm.org).

En general, a la hora de elegir una herramienta o servicio, lo mejor es seguir estas pautas:

- El software debe ser gratuito y de código abierto.
- Lo ideal es que el alojamiento sea autogestionado o que esté alojado en una entidad de confianza.
- Si el servicio lo aloja un tercero, debe estar cifrado de extremo a extremo, auditado y ofrecer un buen nivel de seguridad (por ejemplo, autentificación de 2 factores).

### Sistemas de gestión de tickets

Desde el momento en que se recibe una solicitud de ayuda, la persona encargada del caso debe empezar a anotar todos los detalles relacionados con la persona beneficiaria, el incidente y las medidas que se han tomado para gestionarlo. El registro de todas las comunicaciones con la beneficiaria, así como de todos los pasos que se han dado para detectar y abordar el incidente, puede facilitar la colaboración con el resto del equipo y la derivación a otras organizaciones, ayudar a mejorar los procedimientos de gestión de incidentes y proporcionar pruebas que pueden ser útiles en un tribunal.

Los casos se pueden documentar de muchas formas, incluso en documentos de texto o en papel, pero el uso de un gestor de tickets simplificará esta tarea. Este tipo de herramienta permite registrar y organizar cada comunicación y detalle de forma sistemática, facilitando el seguimiento de cada solicitud de apoyo, así como de las personas beneficiarias y de las terceras partes relevantes. Además permite la revisión de casos anteriores en aras de una garantía de calidad y de poder generar estadísticas... por poner algunos ejemplos de funciones importantes que pueden ser útiles para el trabajo de una LASDSC.

Debido a que las comunicaciones pasan por el gestor de tickets y se almacenan ahí, una de las cuestiones básicas que se debe valorar a la hora de elegir la herramienta que se va a utilizar para el seguimiento de las solicitudes, es la posibilidad de proteger las comunicaciones y la información de los incidentes. Por ello, es recomendable elegir un sistema que sea compatible con herramientas de cifrado, como correos electrónicos cifrados con GPG o aplicaciones de mensajería segura como Signal.

Además, un gestor de tickets debe permitir el registro de la siguiente información:

- Nombre y dirección de correo electrónico de la persona beneficiaria.
- Estado de verificación de la beneficiaria (verificado, no verificado, rechazado).
- Tipo de colectivo al que pertenece.
- Estado actual del incidente (nuevo, abierto, cerrado, etc.)
- Urgencia y prioridad del incidente.
- Un resumen del incidente.
- Tipo de servicio solicitado para resolver el incidente.
- Todas las comunicaciones con la persona beneficiaria.
- Pruebas recogidas durante la investigación del incidente.
- Indicadores relacionados con el incidente.
- Otros casos relacionados con este incidente.
- Medidas adoptadas para este incidente.
- Información de contacto de otras partes implicadas (por ejemplo, intermediarias, empresas a las que se ha recurrido para una derivación, socias que están ayudando a gestionar el caso, etc.)
- Todas las comunicaciones con otras partes implicadas.
- Comentarios de las personas que han gestionado el incidente.
- Próximos pasos.

A continuación, presentamos una visión general de los gestores de tickets más utilizados en el ámbito del apoyo a la seguridad digital para la sociedad civil.

#### Zammad y CDR Link

[Zammad](https://zammad.com) es un sistema gestión de tickets de código abierto, con interfaz web y funciones para gestionar las comunicaciones a través de varios canales como vía telefónica, Facebook, Twitter, chat y correo electrónico, entre otros.

Ante las necesidades específicas de la sociedad civil y de las personas defensoras de los derechos humanos, el [Center for Digital Resilience](https://digiresilience.org/) ha desarrollado [CDR Link](https://docs.digiresilience.org/link/about/), un gestor de tickets centrado en la privacidad y la seguridad y basado en Zammad que cuenta con _plug-ins_ de mensajería personalizados para Signal, WhatsApp y GPG, para proteger las comunicaciones. CDR Link requiere cuentas de Google para iniciar sesión y está alojado en Amazon Web Services (AWS).

Zammad también se puede integrar con NextCloud (a partir de la versión 20): la [app de integración de Zammad para NextCloud](https://apps.nextcloud.com/apps/integration_zammad) ofrece un _widget_ de panel con una visión general de los tickets de Zammad, soporte para buscar tickets de Zammad utilizando la búsqueda unificada de NextCloud y notificaciones sobre las actualizaciones de estado de los tickets.

#### Request Tracker

[Request Tracker](https://bestpractical.com/request-tracker), o RT, es una plataforma de flujos de trabajo y seguimiento de problemas de código abierto desarrollada y apoyada por [Best Practical Solutions](https://bestpractical.com/).

RT puede alojarse en un servidor propio, pero también hay opciones de alojamiento gestionado o en la nube con AWS. Puede integrarse con cifrado PGP y es uno de los gestores de tickets más utilizados por los equipos de apoyo a emergencias informáticas. Entre ellos, cabe destacar cómo las soluciones documentadas durante la gestión de un incidente pueden rápidamente [convertirse en documentación de procedimiento](https://rt-wiki.bestpractical.com/wiki/Articles#Extracting_an_Article) dentro de la propia RT.

#### Freescout

[Freescout](https://freescout.net/) es un gestor de tickets gratuito y de código abierto que puede instalarse fácilmente incluso en un alojamiento compartido. Puede alojarse en un servidor propio, pero [también está disponible el alojamiento gestionado](https://github.com/freescout-helpdesk/freescout/wiki/Cloud-Hosted-FreeScout).

Freescout ofrece [módulos de pago](https://freescout.net/modules/) que pueden ampliar sus funcionalidades, incluida la integración con [PGP](https://freescout.net/module/mail-signing/) (solo firma y cifrado), [WhatsApp](https://freescout.net/module/whatsapp/), [Telegram](https://freescout.net/module/telegram-integration/) y [Twitter](https://freescout.net/module/twitter/).

#### Trac

[Trac](https://trac.edgewall.org/) es un sistema de gestión de proyectos y seguimiento de errores de código abierto, interfaz web, que [puede utilizarse para el seguimiento de tareas, problemas e incidentes](https://trac.edgewall.org/wiki/TracTickets) y a veces lo utilizan las líneas de atención para gestionar sus casos. Puede alojarse en un servidor propio, pero [también está disponible el alojamiento gestionado](https://trac.edgewall.org/wiki/CommercialServices).

#### GLPI

[GLPI](https://glpi-project.org/) (acrónimo francés para _Gestionnaire Libre de Parc Informatique_ o "Gestor Libre de Equipos Informáticos") es un sistema de seguimiento y servicio de atención al cliente gratuito y de código abierto. Puede [alojarse en servidor propio](https://glpi-install.readthedocs.io/en/latest/) pero [también está disponible el alojamiento gestionado](https://www.glpi-network.cloud/).

#### Primero y GBVIMS

[Primero](https://www.primero.org/) es una plataforma de software de código abierto alojado en servidores propios para el seguimiento de incidentes y la gestión de casos de servicios sociales, incluida la protección de la infancia y la violencia de género.

Basado en Primero, el [Gender-Based Violence Information Management System (GBVIMS)](https://www.gbvims.com) es un sistema de gestión de datos que permite a quienes prestan servicios a supervivientes de violencia de género recopilar, almacenar, analizar y compartir de forma eficaz y segura los datos relacionados con los incidentes denunciados.

#### Otros

Hay muchas herramientas útiles que pueden integrar los tickets del proceso de gestión en su flujo de trabajo actual. Como hemos mencionado antes, si su organización ya está utilizando NextCloud hay un _[plug-in](https://apps.nextcloud.com/apps/integration_zammad)_ específico para la integración de Zammad. Si utiliza Discourse puede añadir un gestor de tickets usando los _plug-ins_ [tickets](https://meta.discourse.org/t/tickets-plugin/97914) y [assign](https://meta.discourse.org/t/discourse-assign/58044).

Una herramienta de gestión de tickets estándar puede ser excesiva para organizaciones pequeñas. Pero existen muchas herramientas de gestión de proyectos que pueden cumplir perfectamente la función y que son mucho más sencillas de utilizar. Por ejemplo, [NextCloud Deck](https://apps.NextCloud.com/apps/deck) es una forma sencilla y fácil de gestionar tickets. Su simplicidad es una gran ventaja para organizaciones pequeñas. En NextCloud Desk cada ticket es una tarea, una tarea lleva una descripción, se califica con etiquetas, tiene una fecha de inicio y de vencimiento y se puede desplazar de una columna a otra para replicar el flujo de trabajo de los tickets.

## 2.6. Gestión del equipo

Toda LASDSC tiene que contar con un equipo de personas que tengan una serie de habilidades específicas para responder a las solicitudes de apoyo de forma ágil y acertada. El tamaño y experiencia del equipo dependerá del diseño organizacional y de la situación financiera, así como de la capacidad de crear, coordinar y cuidar el desarrollo profesional y el bienestar del equipo.

### Habilidades deseadas

Una LASDSC debe definir el conjunto de habilidades necesarias para cumplir su misión. Puesto que la tarea de la línea de atención es responder a solicitudes de seguridad digital, hay una serie de habilidades básicas que el equipo debe cumplir.

La siguiente es una lista orientativa de las habilidades técnicas e interpersonales que debe reunir el personal de una LASDSC. Si el equipo no posee alguna de las habilidades necesarias, deberá identificar a una tercera parte a la que externalizar la tarea para la que se necesita esa habilidad.

#### Conjunto de habilidades técnicas

- Capacidad para administrar sistemas GNU/Linux-UNIX.
- Capacidad para administrar servidores web.
- Familiaridad con los sistemas operativos más comunes: Windows, GNU/Linux, macOS, Android e iOS.
- Capacidad para trabajar con al menos una herramienta de análisis de red como Wireshark, tcpdump, Zeek, Snort, etc.
- Conocimiento práctico de toda la gama de protocolos de red OSI o TCP/IP, incluidos los principales protocolos como IP, protocolo de mensajes de control de Internet (ICMP, por sus siglas en inglés), TCP, protocolo de datagramas de usuario (UDP, por sus siglas en inglés), protocolo simple de transferencia de correo (SMTP, por sus siglas en inglés), protocolo de oficina de correos 3 (POP3, por sus siglas en inglés), protocolo de transferencia de hipertexto (HTTP, por sus siglas en inglés), protocolo de transferencia de archivos (FTP, por sus siglas en inglés) y SSH.
- Conocimiento práctico de algoritmos y protocolos de criptografía populares como Advanced Encryption Standard (AES), Rivest, Shamir y Adleman (RSA), Message-Digest Algorithm (5) (MD5), Secure Hash Algorithm (SHA), Kerberos, Secure Socket Layer/ Transport Layer Security (SSL/TLS) y Diffie-Hellman.
- Capacidad para realizar evaluaciones de vulnerabilidad y trabajar con herramientas de pruebas penetración como Kali Linux, Metasploit, etc.
- Conocimiento de scripts con lenguajes y herramientas como Python, bash, awk, sed, grep, etc.
- Familiaridad con las técnicas y tácticas de los ataques.
- Conocimiento sólido de la gestión de personas usuarias finales de plataformas de redes sociales como Facebook, Instagram, Twitter, Youtube, etc.
- Para quienes trabajen con ingeniería inversa de malware, conocimientos de código ensamblador en Intel x86 y manejo de diversas utilidades que ayudan al análisis de malware, como SysInternals, así como suites de herramientas utilizadas para descompilar y analizar malware como IDA y Ghidra.

Sin duda, las personas con formación en informática y estudios de seguridad informática estarán seguramente mejor preparadas para dominar estas habilidades. Sin embargo, también hay quienes sienten pasión por estos campos y que podrían adquirir y demostrar estos conocimientos con rapidez. A la hora de contratar, conviene evaluar la formación, la experiencia y el nivel de entusiasmo para seleccionar a una persona adecuada que se incorpore al equipo.

Por lo general, durante la creación de una línea de atención, no siempre se logra contar con estos conocimientos con la rapidez que se desea. Por lo tanto, la dirección debe centrarse en identificar las capacidades que son absolutamente necesarias para cumplir la misión de la LASDSC y, a continuación, cubrir las carencias formando al equipo, realizando una campaña de contratación específica o subcontratando ciertas tareas a otros equipos.

#### Habilidades interpersonales

Las habilidades interpersonales son tan importantes como las técnicas, sobre todo para las personas que estarán en contacto directo con las beneficiarias.

- Buena comunicación escrita y oral en inglés, para coordinarse con otras entidades asociadas y en las lenguas locales de la región en la que centrarán su trabajo.
- Capacidad para trabajar en entornos con un ritmo de trabajo intenso y con mucho estrés.
- Gran capacidad para trabajar en equipo.
- Capacidad para impartir formación práctica y compartir conocimientos con el resto del equipo.
- Capacidad de iniciativa propia y de gestión del tiempo.
- Un profundo sentido de integridad y de identificación con la misión de la LASDSC.
- Amplia comprensión de la cultura y experiencia en la región en la que se trabaja.
- Comprensión profunda del contexto político de la LASDSC.
- Sensibilidad interseccional en el trabajo con las personas beneficiarias.

### Roles y responsabilidades

Una LASDSC conlleva muchas funciones, cada una de ellas con distintas responsabilidades. Tenerlas en cuenta puede ayudar a diseñar la estructura del equipo y a identificar qué puestos necesitan -o pueden- cubrir en función de su marco de trabajo y presupuesto.

- **El personal de primera línea que gestiona las incidencias** - Son el núcleo central del equipo: quienes estarán en contacto directo con las personas beneficiarias y coordinarán las múltiples tareas que propiciarán una respuesta satisfactoria. Además de las imprescindibles habilidades de comunicación para entender las necesidades de la beneficiaria y transmitirle las instrucciones, este personal de primera línea debe ser capaz de seguir las comunicaciones internas entre el equipo, identificar las lagunas en la documentación y manejar los diferentes recursos disponibles para llevar a cabo la gestión de incidentes, como (1) software (gestores de tickets, software para comunicaciones seguras y herramientas para realizar el triaje) y (2) recursos humanos, ya sean internos (analistas de segundo nivel) o externos (analistas externos, proveedores de servicios y otras personas o entidades asociadas).
- **Responsable del turno** - En las líneas de atención pequeñas, puede ser la dirección. Su función es coordinar a las personas que atienden las incidencias.
- **Analistas de segundo nivel** - Su función es proporcionar apoyo técnico a las personas que gestionan los incidentes cuando se trata de casos difíciles.
- **Responsable de operaciones** - Gestionan la economía del equipo y sus necesidades en términos de hardware, ubicación, logística, etc. Estas tareas podrían ser asumidas por la dirección en LASDSC pequeñas, luego se puede contratar a personal para esta función o externalizar.
- **Coordinación de documentación** - La función de la persona que coordina la documentación es gestionar la base de conocimientos de la LASDSC. Debe encargarse de mantener la documentación existente y ayudar a identificar las lagunas y oportunidades para editar y crear la documentación necesaria para la línea de atención y, especialmente, para que quienes gestionan las incidencias en primera línea puedan realizar su tarea.
- **Administración de sistema** - Su función es crear y mantener la infraestructura que la línea de atención utilizará en su funcionamiento cotidiano. Esta tarea puede externalizarse o, en líneas de atención pequeñas, lo puede asumir la dirección o las personas responsables de incidencias.
- **Asesora jurídica** - Esta persona ayudará a evaluar la seguridad jurídica de las distintas intervenciones que realice la línea de atención y se asegurará de que sus políticas cumplen el marco jurídico vigente (protección de datos personales, por ejemplo).
- Asesora psicosocial\*\* - La asesora psicosocial puede formar a las gestoras de incidentes de la LASDSC para que respondan a las solicitudes de apoyo sin revictimizar y para que adquieran las habilidades necesarias para atender a las personas con trastornos emocionales.
- **Externalización de tareas** - A la hora de externalizar cualquier tarea, se debe hacer siempre a un equipo o persona con la que existe un nivel de confianza muy alto. Teniendo en cuenta las características de una línea de atención que trabaja con la sociedad civil y con personas defensoras de los derechos humanos, los contratos y acuerdos de confidencialidad son sin duda necesarios, pero no suficientes para garantizar el nivel de seguridad que necesita el público de una LASDSC.

### Crea tu equipo

La contratación de personal para la línea de atención es un proceso decisivo para el éxito del proyecto. Además de las competencias técnicas e interpersonales, deben compartir los valores del equipo y comprometerse genuinamente a prestar apoyo a su público. A la hora de buscar nuevo personal para el equipo, es importante tener en cuenta los siguientes aspectos:

- La confianza de las comunidades a las que se apoya es más importante que las competencias puramente técnicas. La sintonía con los valores y el compromiso con la misión del equipo deben figurar entre los principales requisitos para todos los puestos.
- El desarrollo de las competencias técnicas siempre es posible, siempre y cuando se disponga de tiempo y de recursos para apoyar el proceso de formación.
- Los procesos de incorporación marcan una gran diferencia en cuanto a la rapidez con la que el personal de una línea de atención puede empezar a prestar asistencia de calidad. Siempre que sea posible, redacten unas directrices sobre todos los temas que cubre la línea y sigan una metodología de mentoría.

Se puede optar por contratar a una persona y luego formarla en las competencias que necesita para trabajar en la LASDSC, o se puede lanzar una campaña de capacitación comunitaria para formar a activistas que luego colaboren en la línea de atención. Si deciden adoptar este planteamiento, incorporen un [enfoque holístico](https://holistic-security.tacticaltech.org/news/trainers-manual.html) a sus sesiones de formación y un marco como el modelo ADIDS.

Más información sobre el modelo ADIDS en los siguientes recursos:

- [Cómo abordar el aprendizaje en adultos (inglés)](https://level-up.cc/before-an-event/levelups-approach-to-adult-learning/) – Level Up
- [Cómo diseñar sesiones usando ADIDS (inglés)](https://level-up.cc/before-an-event/preparing-sessions-using-adids/) – Level Up
- [Un módulo de formación de formadores en aprendizaje de adultos y ADIDS (inglés)](https://www.fabriders.net/tot-adids/) – Fabriders

> Entre 2019 y 2021, DDP puso en marcha un proyecto para desarrollar las capacidades de formadoras que pudieran brindar un apoyo sostenible y holístico en materia de seguridad a personas defensoras de derechos humanos en el sudeste asiático, América Latina y África. Este proceso se desarrolló en dos fases: por un lado, el objetivo era fortalecer las capacidades de las personas que brindan protección en general; por otro, se pretendía involucrar directamente a algunas de las personas participantes para llevar a cabo procesos de acompañamiento. Una vez finalizado, DDP lanzó una convocatoria para las participantes que deseasen formar parte del equipo de Facilitadoras de Protección Digital de DDP. El proyecto fue un pilar del proyecto más amplio de DDP de descentralizar y reforzar el grado de integración de su equipo en los movimientos de derechos humanos del Sur Global.

> Daniel Ó Cluanaigh, Programa de Defensoras Digitales.

### Formación y desarrollo profesional

Para desarrollar los conocimientos y habilidades del equipo de la línea de atención, deben ofrecerse distintas posibilidades, ya que una única estrategia de trabajo para todo el personal puede no ser posible o adecuada.

Cada integrante del equipo debe trabajar conjuntamente con sus responsables directas para elaborar planes de formación y desarrollo. Estos deben revisarse de forma periódica y se deben registrar los progresos durante las intervenciones o las revisiones de rendimiento.

Los planes de desarrollo personales pueden variar: desde formación en línea, adquisición de libros y revistas o asistencia a formación presencial.

Además de estos planes personales, la organización de las líneas de atención debe compartir recursos a nivel interno de forma continua, en especial sobre temas que son relevantes para todo el equipo, como cuestiones técnicas sobre seguridad digital y otras, entre ellas el autocuidado, el apoyo psicológico y la seguridad física.

#### Proporcionar materiales y fomentar el autoaprendizaje

Las responsables de las líneas de atención deben hacer circular recursos dentro del equipo para ampliar sus conocimientos técnicos sobre diferentes temas relacionados con la seguridad. Esto debe plantearse como una invitación abierta, para que el equipo consulte y utilice esos recursos, no se debe hacer un control posterior para comprobar si se están aplicando los conocimientos. El objetivo principal es proporcionar información que se ajuste a sus intereses y fomentar una cultura de autoaprendizaje.

Además de los recursos en línea, también se pueden facilitar libros y revistas que puedan ser útiles para el trabajo de la línea de atención. Las revistas y los libros se proporcionarán a petición del equipo o cuando la dirección de la línea de atención considere que pueden tener un impacto positivo en el desarrollo de este.

#### Formación externa

Cuando sea apropiado y siempre que los recursos lo permitan, la dirección de la línea de atención debe apoyar a las personas del equipo para que participen en cursos de formación técnica, de acuerdo con sus planes personales de formación.

#### Conferencias

Es importante tratar de facilitar que el personal tenga al menos una oportunidad al año para asistir a un evento relacionado con la línea de atención.

Conviene conocer las conferencias futuras y tratar de asistir a eventos que sean relevantes para el trabajo con grupos de la sociedad civil y otras organizaciones y personas que prestan estos servicios de seguridad digital.

Una de las mejores formas de localizar conferencias y eventos relevantes es preguntar a las personas beneficiarias a qué eventos acuden o les gustaría que asistiera el personal del equipo.

#### Ejemplos de conferencias que pueden ser relevantes para una línea de atención orientada a la sociedad civil

| Evento                                          | Frecuencia         | Página web                                                                                                                                             | Contenido                                                                                                                                |
| ----------------------------------------------- | ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------- |
| Bread&Net                                       | Anual              | [https://www.breadandnet.org/en/](https://www.breadandnet.org/en/)                                                                                     | Desconferencia anual que promueve y defiende los derechos digitales en países de lengua árabe.                                           |
| Chaos Communications Congress                   | Anual              | [https://events.ccc.de/](https://events.ccc.de/)                                                                                                       | Adquisición de conocimientos técnicos, conocer a nuevas personas beneficiarias o socias.                                                 |
| Dublin Platform                                 | Cada dos años      | [https://www.frontlinedefenders.org/en/programme/dublin-platform](https://www.frontlinedefenders.org/en/programme/dublin-platform)                     | Reunión bienal de personas defensoras de derechos humanos.                                                                               |
| FIFAfrica                                       | Anual              | [https://cipesa.org/fifafrica/](https://cipesa.org/fifafrica/)                                                                                         | Foro sobre la libertad de Internet en África.                                                                                            |
| Encuentro global de Rapid Response Network      | Anual              | [https://rarenet.org](http://rarenet.org)                                                                                                              | Encuentro entre miembros de Rapid Response Network.                                                                                      |
| Global Internet Governance Forum                | Anual              | [https://www.intgovforum.org](https://www.intgovforum.org)                                                                                             | Plataforma global de múltiples partes interesadas que facilita el debate sobre cuestiones de política pública relacionadas con Internet. |
| ILGA World (& conferencias regionales)          | Anual              | [https://ilga.org/es/conferencias-mundiales](https://ilga.org/es/conferencias-mundiales)                                                               | Encuentros mundiales y regionales de activistas LGTBQIA+.                                                                                |
| MozFest                                         | Anual              | [https://www.mozillafestival.org/](https://www.mozillafestival.org/)                                                                                   | Conferencia sobre tecnología organizada por Mozilla.                                                                                     |
| Regional Internet Governance Forums             | Anual              | [https://www.intgovforum.org/multilingual/content/regional-igf-initiatives](https://www.intgovforum.org/multilingual/content/regional-igf-initiatives) | Conferencias regionales para facilitar el debate sobre cuestiones de política pública relacionadas con Internet.                         |
| Encuentros regionales de Rapid Response Network | A lo largo del año | [https://rarenet.org](https://rarenet.org)                                                                                                             | Encuentros entre miembros de Rapid Response Network.                                                                                     |
| RightsCon                                       | Anual              | [https://rightscon.org](https://rightscon.org)                                                                                                         | Encuentro mundial sobre derechos digitales organizado por Access Now.                                                                    |
| Stockholm Internet Forum                        | Anual              | [https://stockholminternetforum.se/](https://stockholminternetforum.se/)                                                                               | Foro internacional que promueve una Internet libre, abierta y segura como motor de desarrollo mundial.                                   |

##### Eventos de Trusted Introducer

[CiviCERT](https://civicert.org) es un CERT acreditado por [Trusted Introducer](https://www.trusted-introducer.org/) y como tal, los miembros de CiviCERT pueden participar en encuentros y sesiones de formación para equipos de seguridad y respuesta a incidentes. [Aquí](https://www.trusted-introducer.org/events.html) puedes encontrar los próximos eventos.

#### Plataformas web

También se puede ofrecer al equipo acceso a cursos en plataformas en línea. Estos suelen ser más baratos que los presenciales y permiten que el equipo los realice a su ritmo y en función de la carga de trabajo.

Algunas de estas plataformas podrían ser:

- [Pluralsight](https://pluralsight.com/)
- [Udemy](https://www.udemy.com/)
- [Cybrary](https://www.cybrary.it/)
- [Coursera](https://www.coursera.org/)

#### Sesiones de intercambio

Es importante animar a las personas del equipo que han adquirido conocimientos que puedan ser útiles para las demás (ya sea en el desempeño de su trabajo o tras realizar una formación) a que lo documenten mediante artículos y a que ofrezcan sesiones de intercambio de conocimientos al resto del equipo. Si las sesiones de intercambio de conocimientos se graban, servirán de apoyo para el aprendizaje del resto del equipo y de las nuevas contrataciones.

### Políticas de cuidados

Quienes atienden las incidencias en una línea de atención gestionan un amplio conjunto de situaciones estresantes que pueden afectarles. Cuidar el bienestar psicoemocional del equipo evita el agotamiento, el desgaste y aumenta la calidad de la atención prestada.

Es tan importante cuidar el bienestar del equipo como el de las personas beneficiarias. Por ello, conviene asignar todo tipo de recursos (tiempo, recursos humanos, dinero, etc.) para desarrollar un enfoque de cuidado psicoemocional en sus estrategias de gestión de equipos.

No existe una forma universal de lograrlo. Depende de las necesidades del personal y de la noción que tengan de los cuidados. Algunas estrategias de cuidado para garantizar el bienestar del equipo pueden ser:

- Aunque la línea de atención sea voluntaria, ofrezcan buenas condiciones de trabajo: complementos, prestaciones, vacaciones, etc.
- Destinen un porcentaje del presupuesto (hasta el 30%) a actividades de cuidado colectivo.
- Planifiquen turnos cortos para que las personas que atienden las incidencias puedan concentrarse y prestar una mejor atención.
- Establezcan reuniones semanales para analizar los casos y aportar una visión colectiva.
- Incluyan la figura de asesora de salud mental para apoyar a las personas que gestionan los incidentes y evitar el agotamiento o el estrés.
- Al inicio de cada turno de gestión de incidentes, se debe establecer una instancia de control para evaluar si la gestora se encuentra en un buen momento para brindar apoyo. Si no lo está, puede sustituirla otra que no tenga asignado un turno específico.
- Establezcan una serie de parámetros para saber cuándo una llamada es demasiado comprometida y requiere la rápida derivación a una responsable. Por ejemplo, si la persona que llama corre un riesgo inminente. Es preciso tener en cuenta estos pasos para proteger a la gestora de incidentes de un estrés excesivo.
- Planifiquen una reunión anual de todo el equipo para debatir los retos, identificar los aprendizajes y ajustar las estrategias de cuidado de la línea de atención.

Pueden redactar una política de cuidados que establezca todas estas estrategias y a la que el equipo y la dirección puedan recurrir para crear un entorno de trabajo saludable. Asimismo, se debe prever un enfoque psicoemocional a la hora de redactar el procedimiento de gestión de incidentes (garantizando un triaje y una asignación de casos equilibrada entre el equipo), un código de conducta, un mecanismo de quejas internas y otras políticas.

> Damos prioridad a una carga de trabajo equilibrada entre las personas que atienden los casos. Rotamos la recepción de nuevos casos cada semana para distribuirlos de manera equitativa. Por otro lado, es habitual que haya semanas en las que el número de casos aumenta y cuando esto ocurre y supera las posibilidades de las personas que acompañan, buscamos estrategias para reorganizar el seguimiento de los casos y rebajar la carga.

> _Luchadoras (Entrevista, enero de 2022)._
