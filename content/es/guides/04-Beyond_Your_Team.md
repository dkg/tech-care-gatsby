---
layout: post
title: "Más allá de tu equipo: trabajo en red y control de calidad"
date: 2022-09-06
categories: guide
lang: es
order: 4
---

# 4. Más allá de tu equipo: trabajo en red y control de calidad

![collage red computadoras](/chap_4.png)

Los ataques digitales no cesan de evolucionar, ampliando su radio de acción y agravando su impacto, al igual que los contextos políticos en los que se mueven las personas que defienden los derechos humanos y activistas. Estar al día de los cambios, comprenderlos y elaborar estrategias de respuesta puede requerir mucho tiempo y recursos.

Crear y mantener una comunidad de práctica es fundamental para una línea de atención, ya que facilita la colaboración. Además, agiliza los mecanismos de derivación, fomenta el conocimiento de las realidades de todas las regiones, el intercambio de recursos, el aprendizaje de nuevos enfoques y la realización de investigaciones y estudios colaborativos. Cada línea de atención o CERT puede especializarse en un tipo de ataque o público concreto de modo que el resto de la comunidad pueda beneficiarse de esos conocimientos.

También es importante actualizarse constantemente, comprobar que los procedimientos que siguen quienes gestionan las incidencias son los adecuados y recibir observaciones y _feedback_ de forma continua de organizaciones y personas aliadas y de las beneficiarias para mejorar el flujo de trabajo de la línea de atención.

Este capítulo se centra en las derivaciones (una estrategia de colaboración común entre las líneas de atención y los CERT que permite mejorar la capacidad de respuesta) y en el control de calidad (con recomendaciones sobre diferentes enfoques y mecanismos para supervisar, mantener y mejorar la calidad de los servicios prestados).

## 4.1 Crea y cuida tu red de socias

Para tener más opciones de derivación, es importante construir una red de confianza que dé visibilidad al propio trabajo y que aprenda más sobre el trabajo de sus pares. Esto se puede conseguir, por ejemplo, participando en eventos locales o internacionales. Pueden presentar su trabajo en charlas relámpago, _tech demos_, laboratorios comunitarios, talleres, etc. Consulten el listado de conferencias en el [Capítulo 2](/es/02-make-a-realistic-plan) (sección de Formación y desarrollo profesional) para decidir a cuáles pueden asistir.

Formar parte de listas de correo como la lista de correo encriptada de [CiviCERT](https://www.civicert.org/about/) es una gran ventaja, por ejemplo, a la hora de hacer derivaciones. Esta comunidad que vela por la seguridad digital de la sociedad civil suele intercambiar conocimientos sobre derivaciones y experiencia en respuestas rápidas. Más información sobre cómo unirse a la red en su [sitio web](https://www.civicert.org/apply-to-be-a-member/).

No hace falta recordar que formar parte de una red o comunidad es un compromiso proactivo. Las comunidades necesitan que se las cuide y cultive. Es decir, requieren el mantenimiento de su infraestructura y documentación, la facilitación de encuentros, recursos, divulgación, intercambio y, sobre todo, tiempo. Es importante tenerlo en cuenta a la hora de planificar las horas y actividades del equipo.

## 4.2 Derivaciones

A veces, una LASDSC recibe una solicitud que no puede atender, ya sea porque no se ajusta a su ámbito de actuación o porque requiere una serie de conocimientos de los que carece. La LASDSC puede derivar el caso a otra entidad que pueda proporcionar el apoyo necesario en tales situaciones.

Tras verificar la identidad de la persona solicitante y en función de su solicitud, existen cuatro tipos de contactos a los que se derivar:

* **Otra ONG o entidad sin fines de lucro** - Puede ser una derivación a una organización sin fines de lucro de confianza, conocida o con la que se haya trabajado en el pasado.
* **Empresas privadas** - También se puede derivar a una empresa privada con una política de privacidad y modelo de negocio éticos, una empresa de confianza o familiarizada con los desafíos de las organizaciones sin fines de lucro y que utilice tecnologías de código abierto, transparentes y auditables.
* **Entidades gubernamentales** - Es posible que tengan que redirigir el caso a instituciones gubernamentales como un CERT nacional.
* **Especialistas independientes** - También se puede derivar a una beneficiaria a una persona de la comunidad de especialistas en seguridad digital que esté familiarizada con las necesidades específicas de las personas que defienden los derechos humanos.

> La detección de necesidades en las peticiones de apoyo que recibíamos nos llevó a crear puentes con plataformas en línea para tratar de frenar la violencia. Nos dirigimos directamente a ellas y nos abrieron sus puertas, en parte porque había información que les interesaba o porque querían conectar con organizaciones que pudieran mediar. Tuvimos que ser estratégicas y limitar la información que compartíamos porque la utilizaban en su beneficio. Si lo que nos ofrecían no servía para mejorar las condiciones de atención a las mujeres, no nos interesaba. Aunque sean grandes empresas, esperamos reciprocidad en las colaboraciones.

> _Luchadoras (Haché, 2021)._

A continuación, se ofrece una lista de posibles apoyos dentro de la comunidad de seguridad digital o cercanos a la misma, una vez identificada la necesidad de la persona solicitante. Verificar a la persona solicitante es una buena práctica cuando se deriva al sitio web de una organización afín, pero no es obligatorio en este caso.

- **Equipos CERT (instituciones gubernamentales)**
    - [Equipos FIRST](https://www.first.org/members/teams/)
- **Prevención de ataques DDoS**
    - [Deflect](https://deflect.ca/)
    - [CloudFlare Galileo](https://www.cloudflare.com/galileo/)
    - [Google Project Shield](https://projectshield.withgoogle.com/landing?hl=es)
- **Dominios y alojamiento**
    - Consejos sobre alojamiento en [Documentación para la comunidad de la línea de ayuda de Access Now](https://accessnowhelpline.gitlab.io/community-documentation/88-Advice_Hosting.html).
- **Apoyo en caso de emergencia**
    - [Kit de Primeros Auxilios Digitales](https://digitalfirstaid.org/es/support/), un mecanismo de entrada para llegar a integrantes de la comunidad CiviCERT.
    - [Committee to Protect Journalists](https://cpj.org/emergency-response/how-to-get-help/), para periodistas.
- **Financiación y licencias gratuitas**
    - [DDP Incident Emergency Fund](https://www.digitaldefenders.org/funding/incident-emergency-fund/)
    - [OTF Funds](https://www.opentech.fund/funds/)
    - [TechSoup](https://www.techsoup.org/)
    - [Google Nonprofits](https://www.google.com/nonprofits/)
- **Documentación sobre violaciones de derechos humanos**
    - [Huridocs](https://huridocs.org/contact/)
    - [Witness](https://www.witness.org/)
- **Apoyo jurídico**
    - [Media Defence](https://www.mediadefence.org/)
- **Seguridad física y reubicación**
    - [Protect Defenders](https://protectdefenders.eu/protecting-defenders/)
    - [Umbrella](https://secfirst.org/umbrella/) (app para móviles)
- **Evaluaciones de vulnerabilidad y pruebas de penetración**
    - [Red Lab de OTF](https://www.opentech.fund/labs/red-team-lab/)
- **Formación**
    - Recursos de formación y referencias en [Documentación para la comunidad de Access Now](https://accessnowhelpline.gitlab.io/community-documentation/301-Training_Resources.html).

### Proceso de derivaciones

#### Verificación

La verificación de la persona solicitante y de su organización es obligatoria cuando se deriva directamente a otro equipo. Durante este proceso, es preciso asegurarse de que se ha verificado la autenticidad de la solicitud y el trabajo de la persona beneficiaria. La verificación de una organización o persona es una oportunidad para ampliar la red de confianza de su línea y de la comunidad.

#### Triaje

Antes de derivar a la organización o persona adecuada, es importante evaluar las necesidades de la persona solicitante. Lo primero que hay que hacer es analizar la amenaza y el incidente que está sufriendo. Se puede utilizar [un enfoque de modelo de amenazas o de evaluación de riesgos](https://accessnowhelpline.gitlab.io/community-documentation/200-Lightweight_Security_Assessment.html) como punto de partida. Este triaje inicial ayudará a determinar a quién derivar a la beneficiaria.

#### Identificación de la derivación apropiada

Los criterios para elegir una derivación se deben basar en:

- Las necesidades de la persona que lo solicita.
- El idioma de la persona beneficiaria.
- El contexto geopolítico.
- Los conocimientos técnicos necesarios.
- El coste de la derivación.

#### Consentimiento para la derivación

Se recomienda informar a la persona beneficiaria desde el principio sobre la intención de derivarla a otra persona/entidad. Para cumplir con el acuerdo de confidencialidad entre la LASDSC y la persona solicitante, es preciso pedirle formalmente su aprobación para compartir información con otras personas (sobre la evaluación de vulnerabilidad, identidad y datos de contacto de la beneficiaria, etc.).

Comunicar a la persona solicitante las razones principales por las que se inclinan por una entidad concreta, su experiencia, así como las razones principales por las que no pueden satisfacer su solicitud, puede ayudarla a tomar una decisión. También es fundamental indicar si la entidad a la que se deriva el caso tiene capacidad para prestar servicios gratuitos.

#### Consentimiento de la tercera parte a la que se deriva el caso

Una vez identificada la entidad/persona adecuada para la derivación y tras el visto bueno de la persona solicitante, podrán compartir los detalles sobre:

- La evaluación de las necesidades de la solicitante.
- El modelo de amenaza de la solicitante.

El objetivo es asegurarse de que la tercera parte a la que se remite a la persona solicitante tiene suficiente información para asumir la gestión de la solicitud. Si no puede ofrecer un servicio gratuito, habrá que definir los siguientes detalles:

- Qué servicio se ofrecerá.
- El coste del servicio.

Para que la derivación funcione, habrá que aclarar estos aspectos de antemano para que las expectativas de la persona beneficiaria y de la tercera parte coincidan.

#### Establecer el contacto

A la hora de poner en contacto a la persona solicitante y a la entidad/persona a la que se deriva, intenta identificar un canal de comunicación seguro con el que ambas estén familiarizadas. Si utilizan PGP, valoren la posibilidad de que compartan sus claves PGP cuando se presenten.

#### Seguimiento

Transcurrido un tiempo (en función de la carga de trabajo, pueden ser un par de semanas o unos meses) es una buena práctica comprobar tanto con la persona beneficiaria como con la tercera parte, que el caso se ha resuelto y que sus necesidades se han satisfecho.

#### Derivar un caso abierto

Puede ocurrir que hayan empezado a ayudar a una persona en situación de riesgo pero que, por diversas razones, ya no le puedan seguir prestando asistencia.

En estas situaciones, lo mejor es derivarla a una persona/entidad de confianza. El proceso de derivación seguirá los pasos que se indican en esta sección, pero debe incluir un paso adicional a la hora de entregar el incidente y un esfuerzo adicional de comunicación para gestionar las expectativas.

- A la hora de informar a la entidad/persona de confianza la intención de derivarle una solicitud:
    - Tengan en cuenta que, en este caso, la gestión de las expectativas es fundamental.
    - Es preferible tener preparada una lista de posibles derivaciones para estos casos.
    - Aclaren los detalles económicos.
- Cuando acuerden con la solicitante la derivación:
    - Expliquen por qué es preciso derivarle.
    - Den detalles sobre la experiencia de su socia.
- Durante el proceso de derivación:
    - Entreguen a su socia toda la información que han recopilado y su evaluación técnica.

## 4.3 Intercambio de información sobre amenazas

Es una buena práctica compartir regularmente con su comunidad información anonimizada sobre los casos que han tratado. Este intercambio facilitará que otras organizaciones comprendan e identifiquen las pautas y tendencias de ataques digitales que también pueden afectar a sus beneficiarias.

En CiviCERT la información sobre el trabajo de cada integrante se comparte semestralmente en torno a las siguientes cuestiones:

- **Información sobre amenazas**
    - ¿Qué tipo de casos de respuesta rápida han atendido en el último periodo de tiempo?
    - ¿Cuál fue la naturaleza de los ataques?
    - ¿Quién era el objetivo?
    - ¿Qué tendencias observan en su trabajo?

- **Inteligencia sobre amenazas**
    - ¿Qué amenazas y/o tendencias recientes les preocupan más?
    - ¿Ha cambiado algo en los ataques que han estado supervisando/atendiendo?
    - ¿A qué deberían prestar atención otros equipos de respuesta rápida?

Recuerden que la anonimización de esta información debe hacerse de forma que sea imposible rastrear el caso.

## 4.4 Control de calidad

Este proceso describe los estándares de calidad recomendados para una línea de atención. También ofrece recomendaciones sobre distintas perspectivas y mecanismos que pueden utilizarse para supervisar, mantener y mejorar la calidad de los servicios prestados.

### Estándares de calidad

Para medir y evaluar la calidad de su trabajo es importante definir las expectativas y normas que utilizará la LASDSC. A la hora de evaluar la calidad del servicio deben tener en cuenta las siguientes pautas:

- Es importante crear un espacio donde las personas beneficiarias e intermediarias se sientan acogidas, comprendidas, seguras y protegidas.
- Una línea de atención debe ser clara, fiable y práctica. El servicio de atención debe ser excelente y debe prestarse demostrando comprensión y empatía con las necesidades de las beneficiarias y su situación.
- Las beneficiarias deben ser tratadas con dignidad, respeto y confidencialidad para que se sientan protegidas y seguras de que su problema está siendo atendido.
- Escuchar es lo primero. Sean pacientes y escuchen siempre lo que las beneficiarias tienen que decir antes de sacar conclusiones precipitadas y dar consejos técnicos.
- Utilicen un lenguaje claro, inclusivo, no sexista, no clasista, no racista y no colonialista al escribir o hablar.
- Las encargadas de gestionar los incidentes deben adoptar un enfoque basado en estrategias de aprendizaje de personas adultas. Siempre que sea posible, el objetivo de las interacciones debe ser educar y empoderar a las personas beneficiarias mediante el conocimiento y el asesoramiento.
- Las interacciones con las beneficiarias deben ser claras y concisas. Pueden proceder de entornos diversos y tener distintos niveles de conocimientos técnicos.
- Definan cuál será su [Acuerdo de nivel de servicio](https://es.wikipedia.org/wiki/Acuerdo_de_nivel_de_servicio) (ANS), que incluye un tiempo máximo de respuesta para las solicitudes entrantes. Por ejemplo: "en días laborables, responder a todas las solicitudes dentro de las dos primeras horas desde que se reciben, durante el horario laboral; en el fin de semana, en un plazo de 24 horas".
- Mientras el caso esté abierto, respondan con rapidez. Si una beneficiaria no responde, comprueben periódicamente que sus necesidades están cubiertas.
- Se debe utilizar la documentación existente, los procesos de derivación y otros mecanismos para ofrecer soluciones técnicas de calidad, razonables y bien pensadas.

### Mecanismos de control de calidad

#### Define roles y responsabilidades

Para poder garantizar la calidad del servicio, hay algunos roles que deben ser definidos por la LASDSC.

- **Acompañante**: La persona que dirige el caso y trabaja para ayudar a la beneficiaria con su solicitud.
- **Revisora**: La persona encargada de revisar la calidad del trabajo. Puede ser la misma persona siempre, o puede rotar entre el equipo, dependiendo del tamaño y la estructura de la LASDSC. En el caso de organizaciones grandes, puede haber más de una persona en encargada de la revisión.

#### Define un período de tiempo

Las revisiones de casos deben realizarse con regularidad para que sean eficaces. Definan la frecuencia con la que se realizarán estas revisiones. La periodicidad puede variar: desde revisiones semanales a mensuales, trimestrales o incluso anuales. La periodicidad dependerá del tamaño de la LASDSC, de su capacidad de personal, del número de solicitudes tramitadas, del tipo de solicitudes, etc.

#### Revisión de casos individuales

##### Proceso

La persona encargada de la revisión llevará a cabo una revisión de los casos cerrados por las personas acompañantes durante el periodo de tiempo especificado, de acuerdo con las necesidades de la LASDSC.

La revisión debe ser flexible en cuanto al momento en que se realiza, pero al final de cada periodo todos los casos cerrados deben haber sido revisados.

Para facilitar el trabajo de revisión y lograr revisiones más valiosas y útiles, las personas acompañantes deben documentar cada caso de forma exhaustiva. Valoren la posibilidad de añadir metadatos específicos a la documentación del caso para hacer un seguimiento de los comentarios y las apreciaciones de cada caso. Anoten también cualquier comentario o mejora que pueda producirse en el propio proceso de revisión.

La LASDSC debe tener en cuenta los resultados de las revisiones para mejorar sus políticas, el proceso de gestión de incidentes, los protocolos de atención y las competencias del equipo.

##### Criterios

La revisión tendrá en cuenta los siguientes aspectos:

- **Metadatos**
    - ¿Están completos todos los metadatos del caso?
    - ¿Es el contenido de los metadatos detallado y preciso?

- **Puntualidad**
    - ¿Se envió la primera respuesta dentro del ANS?
    - ¿Se ha realizado un seguimiento regular?
    - ¿Recibió la beneficiara las respuestas a tiempo?

- **Idioma**
    - ¿Fueron claras las comunicaciones con la beneficiaria?
    - ¿La estructura, ortografía y gramática de las comunicaciones es adecuada?
    - ¿Se ha utilizado un lenguaje no violento y de género neutro? ¿Se adoptó un enfoque interseccional?

- **Eficiencia**
    - ¿Se sometió a la beneficiaria a una evaluación de riesgos antes de recomendar una solución?
    - ¿Fue la solución utilizada la más adecuada para el contexto de la beneficiaria?

- **Documentación**
    - ¿Se ha documentado correctamente el caso?
    - ¿Se registró correctamente la información adicional (capturas de pantalla, análisis, archivos) del caso?

- **Servicio de atención**
    - ¿Se ha procurado validar el relato de la beneficiaria?
    - ¿Se sintió empoderada la beneficiaria cuando se resolvió el caso?

- **Recomendaciones técnicas**
    - ¿Se aplicó la solución tecnológica adecuada en este caso?
    - ¿Se comunicó la información técnica de forma que se ajustara a las capacidades y necesidades de la beneficiaria?


#### Retroalimentación

Una buena forma de prestar regularmente un buen servicio es poner en marcha algún mecanismo para recabar la opinión de la beneficiaria una vez resuelto el caso. Esta retroalimentación puede recogerse de distintas maneras, dependiendo de las necesidades y capacidades de su organización. Algunos ejemplos son:

- Formularios en línea
- Mensajes de seguimiento
- Llamadas de evaluación

Es importante tener en cuenta que la información recopilada durante esta etapa puede ser sensible, por lo que es necesario garantizar que se transfiera y almacene de forma segura.

##### Proceso de revisión de las evaluaciones

Si se recogen comentarios y opiniones, la persona encargada de la revisión debe estudiarlos y detectar posibles áreas de mejora de los procesos.

A continuación, debe encontrar una forma adecuada de informar a la persona que ha acompañado el caso sobre esta retroalimentación (ya sea positiva o negativa) y valorar si el caso en cuestión necesita más medidas.
