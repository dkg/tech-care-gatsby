---
layout: post
title: "Diseña tu marco de trabajo"
date: 2022-09-06
categories: guide
lang: es
order: 1
---

# 1. Diseña tu marco de trabajo

![collage suculentas archivos](/chap_1.png)

Creen un marco para la Línea de Atención sobre Seguridad Digital para la Sociedad Civil (LASDSC) y describan en detalle qué función tendrá, cuál será el público al que se dirigirá, qué diseño organizacional tendrá y con quién cooperará. Este ejercicio sirve también para hacerse una idea de los recursos que necesitarán para ayudar a sus beneficiarias. Es importante invertir el tiempo que sea preciso para el diseño del marco, ya que éste sentará las bases de la línea de atención.

Este capítulo describe cómo identificar a su público, analizar las necesidades de las personas beneficiarias, definir su misión y entorno, establecer los servicios básicos y los parámetros de comunicación y especificar sus políticas. Las LASDSC pueden ser muy diferentes entre sí, por lo que cada una tendrá un marco diferente, pero los elementos descritos en este capítulo podrán encontrarse en todos los proyectos.

Puedes descargar una [plantilla de marco](/es/06-framework-template) que te facilitará pensar esta etapa.

## 1.1 Define tu público

En esta guía se utilizará el término "público" para definir el conjunto de personas beneficiarias atendidas por una LASDSC. Asimismo, se denominará "beneficiaria" cuando se trate de una sola persona u organización.

A la hora de poner en marcha una LASDSC es esencial tener una visión clara de quiénes serán sus beneficiarias y el tipo de entorno para los que se van a desarrollar sus servicios. La decisión sobre a quién prestar servicio o no puede depender de la misión de su organización, de las políticas de las entidades financiadoras, de consideraciones legales o incluso de un escenario político cambiante que pone en riesgo a un grupo específico de personas.

En términos generales, se supone que las LASDSC están dirigidas a la sociedad civil o a una parte de ella. Aun así, teniendo en cuenta que la definición de "sociedad civil" varía mucho según el grupo o sector, puede resultar útil ofrecer una lista de los tipos de beneficiarias a los que actualmente prestan servicio las LASDSC que integran CiviCERT:

- Activistas
- Organizaciones no gubernamentales
- Personas defensoras de los derechos humanos
- Organizaciones de derechos humanos amenazadas
- Medios de comunicación independientes
- Organizaciones indígenas
- Periodistas
- Personas que defienden la tierra
- Grupos LGTBQIA+
- Mujeres
- Jóvenes

> Trabajamos con mujeres de todo el país. Existe un problema de acceso, que está fundamentalmente relacionado con la clase y la edad. En general, no solemos recibir peticiones de mujeres jóvenes y de clase media porque ellas mismas consiguen resolver sus problemas de violencia de género gracias a las TIC. Luego, hay personas mayores, niñas y niños y adolescentes de zonas rurales que recién comienzan su relación con las TIC. También apoyamos a personas con un perfil público a quienes se les agrede constantemente y que también necesitan estrategias específicas… periodistas, mujeres políticas o con cargos públicos y activistas. Así que, para cada uno de estos grupos, tenemos una forma distinta de abordar el trabajo y la violencia a la que se enfrentan.<br><br>_SOS Digital (Haché, 2021)._


## 1.2 Analiza las necesidades de tu público

Identificar y evaluar las necesidades y expectativas del público al que se dirigen y el contexto en el que operan será fundamental para el éxito del proyecto. Deben hablar con su público acerca del valor real que podría aportar su LASDSC, analizando las amenazas a las que podrían enfrentarse sus beneficiarias y sus necesidades respecto a los incidentes de seguridad digital y la prevención de amenazas. Esto puede hacerse de forma presencial o a distancia, en grupos o en entrevistas individuales, tanto de forma pública como privada.

Hay varios marcos de trabajo que pueden utilizarse para este tipo de análisis. Los dos más comunes son:

- DAFO: para identificar las debilidades, amenazas, fortalezas y oportunidades.
- PESTEL: para incorporar la dimensión política, económica, social, tecnológica, ecológica y legal en el análisis contextual.

Una vez hayan analizado las necesidades de su público, una buena práctica es realizar una actividad de simulación de amenazas para analizar las características del contexto político en el que van a trabajar, identificar las vulnerabilidades y amenazas de la línea de atención, así como la probabilidad de que se produzcan y especificar los requisitos de prevención y mitigación. Este modelo de amenazas debe tenerse en cuenta a la hora de desarrollar las políticas de la línea de atención, los procedimientos técnicos, la documentación y la formación del personal.

En la [plantilla del marco de trabajo](/es//06-framework-template) ofrecemos algunas claves para realizar estos análisis.

### Para saber más

- Higson Smith, Craig, Ó Cluanaigh, Daniel, Ravi, Ali G., Steudtner, Peter (2016). Overall Framework for Context Analysis. *Holistic Security - A Strategy Manual for Human Rights Defenders*. Tactical Technology Collective. [https://holistic-security.tacticaltech.org/chapters/explore/2-1-overall-framework-for-context-analysis.html](https://holistic-security.tacticaltech.org/chapters/explore/2-1-overall-framework-for-context-analysis.html).
- Front Line Defenders (2011). Understanding Your Context. *Workbook on Security: Practical Steps for Human Rights Defenders at Risk*, pp. 61-7.
- Schulte, Jennifer (2018). Gender-Based Risk Model. *Cyberwomen: Holistic Digital Security Training Curriculum for Women Human Rights Defenders*. Institute For War and Peace Reporting. [https://iwpr.net/global-voices/print-publications/cyberwomen-holistic-digital-security-training-curriculum-women](https://iwpr.net/global-voices/print-publications/cyberwomen-holistic-digital-security-training-curriculum-women).

> Estuvimos trabajando para crear conocimiento sobre cómo las mujeres estaban viviendo la violencia en México, para poder identificarla y también reconocerla. Y nuestro trabajo era hacer visible la violencia en la agenda pública, esto nos convirtió en un referente y también se abrió una posibilidad de tener un espacio de apoyo para las mujeres que estaban enfrentando esta violencia. Comienzan a llegar solicitudes y en marzo de 2020 decidimos equiparnos y acuerpar a otras mujeres que nos estaban buscando para pedirnos apoyo, acompañamiento e información. <br><br> _Luchadoras (Haché, 2021)._

## 1.3 Define tu misión

Después de analizar las necesidades y el contexto de su público, el siguiente paso de planificación debe ser la redacción de una declaración de misión que explique claramente el propósito y la función de la LASDSC y proporcione una breve descripción de las metas principales del proyecto.

Como esta será la base sobre la cuál va a trabajar la LASDSC durante algunos años, una buena práctica es no caer en ambigüedades y redactar una declaración de misión concisa, pero no demasiado breve.

He aquí dos ejemplos de declaraciones de integrantes de CiviCERT:

**Línea de Ayuda en Seguridad Digital de Access Now**

Access Now defiende y refuerza los derechos digitales de las personas usuarias en peligro en todo el mundo. Combina apoyo técnico directo, el compromiso con una política integral, incidencia política a nivel global, la concesión de subvenciones y encuentros como RightsCon, para luchar por los derechos humanos en la era digital.

La Línea de Ayuda en Seguridad Digital de Access Now proporciona soluciones tecnológicas y asesoramiento a tiempo real para personas que se encuentran en una situación de riesgo en contextos en los que las comunicaciones no son abiertas, libres o seguras. A través de nuestra línea de ayuda para la seguridad digital, que funciona 24 horas al día, 365 días al año, ofrecemos asesoramiento técnico y respuesta a incidentes para brindar información y apoyo a activistas, periodistas, personas que defienden los derechos humanos y personas o entidades de la sociedad civil sobre el terreno.

Las metas y objetivos principales de la Línea de Ayuda en Seguridad Digital de Access Now son:

- Proporcionar apoyo técnico directo a personas y organizaciones en situación de riesgo para identificar y abordar sus necesidades de seguridad digital.
- Proporcionar asistencia a personas y organizaciones en situación de riesgo para proteger sus activos digitales, comunicaciones y otras actividades en línea y ayudarles a eludir la censura y obtener acceso a los servicios que necesitan.
- Proporcionar conocimientos técnicos, apoyo y coordinación para prevenir y detener infecciones de software malicioso y abordar las vulnerabilidades de los sistemas y el software.
- Mantener a nuestro público al día sobre las nuevas amenazas y vulnerabilidades que deben abordarse urgentemente.
- Coordinar el apoyo a personas en situación de riesgo cuando este apoyo pueda ser proporcionado de forma más adecuada por otros CERT.
- Convertirse en un centro de referencia por su excelencia en seguridad de la información al que puedan recurrir organizaciones nacionales e internacionales.

**Mnemonic**

Mnemonic trabaja en todo el mundo para ayudar a las personas que defienden los derechos humanos a documentar eficazmente de forma digital las violaciones de derechos humanos y los crímenes internacionales, a fin de apoyar la incidencia política, la justicia y la rendición de cuentas.

Nuestros objetivos son:

- Archivar la información digital para garantizar que las posibles pruebas no se pierdan y permanezcan accesibles y utilizables para futuros mecanismos de imputación.
- Formar a las personas defensoras de derechos humanos para maximizar el impacto de la información digital y empoderar a quienes trabajan con ella.
- Reducir el impacto de las políticas de moderación de contenidos perjudiciales por parte de empresas de redes sociales y gobiernos, proporcionando datos exhaustivos y fiables sobre la retirada de documentación de derechos humanos de las plataformas de redes sociales.
- Crear y apoyar el desarrollo de herramientas y métodos de código abierto para aumentar la capacidad de las personas que defienden los derechos humanos de utilizar la información digital para promover la justicia social.

> Combatimos y cuestionamos la industria de la vigilancia y la persecución de activistas a través de apoyo a la seguridad digital, investigación, incidencia política y campañas. En Amnesty, cada equipo tiene que definir el problema que quiere abordar y crear una estrategia de cambio. Nos preguntamos: "¿Cómo podemos cambiar esto?" Y esta pregunta nos lleva a muchos debates, por lo que nos centramos más en la investigación. Mientras que Access Now y otras organizaciones proporcionan apoyo digital a un público amplio, aquí nos centramos en las personas que son o han sido objeto de vigilancia. Realizamos tanto investigaciones como apoyo a la seguridad digital. <br><br> _Etienne Maynier, Amnesty Tech (Entrevista, diciembre de 2021)._

## 1.4 Define tu diseño organizacional

A la hora de planificar la creación de una LASDSC, es preciso tomar decisiones sobre su diseño organizacional: ¿formará parte de una organización más grande o se gestionará de forma independiente? ¿Necesitará recaudar fondos por su cuenta o cuenta con un departamento de recaudación de fondos que le proporcionará recursos?

La LASDSC puede tener distintos diseños organizacionales. Muchas son proyectos de organizaciones sin fines de lucro más grandes o trabajan dentro de un proveedor de servicios de Internet para la sociedad civil. Otras son independientes y algunas funcionan con voluntariado. También es posible que existan múltiples capacidades de gestión de incidentes dentro de una misma organización matriz, por ejemplo, que tengan un departamento que atienda al personal de la organización y se ocupe de los incidentes de su infraestructura, mientras que otro atiende a un público externo. Algunos CERT nacionales pueden incluir entre sus servicios la gestión de incidentes de seguridad digital que afectan a organizaciones sin fines de lucro.

## 1.5 Define tus servicios básicos

Una LASDSC puede ofrecer muchos servicios diferentes, pero mientras proporcione algún tipo de respuesta a incidentes de seguridad digital no necesita hacerlo todo y puede centrarse en un pequeño conjunto de servicios básicos, por ejemplo, en la seguridad de las cuentas de las redes sociales u ofrecer recomendaciones sobre cómo sortear la censura en un área específica.

Muchas LASDSC prestan tanto servicios reactivos (que responden a incidentes de seguridad digital) como preventivos (iniciativas de educación en seguridad digital para reducir el riesgo de incidentes) pero en la mayoría de los casos limitarán su lista de servicios en función de su capacidad y derivarán a otros equipos los servicios adicionales.

A continuación, ofrecemos una lista de servicios tanto reactivos como preventivos que ofrecen los miembros de CiviCERT, incluidos algunos que no son del ámbito de la seguridad digital:

- **Reactivo**
    - Triaje inicial
    - Apoyo digital 24/7
    - Sustitución de equipos
    - Gestión de vulnerabilidades y software malicioso
    - Gestión del hackeo de cuentas
    - Mitigación del acoso en línea
    - Análisis forense
    - Elusión de censura
- **Preventivo**
    - Formación presencial
    - Consultorías de seguridad organizativa
    - Alojamiento seguro de sitios web
    - Protección de sitios web
    - Protección contra la denegación de servicio
    - Evaluación de amenazas y riesgos
    - Protección de las comunicaciones
    - Seguridad de los dispositivos
    - Seguridad para la navegación web
    - Seguridad para cuentas
- ** No estrictamente relacionado con la seguridad digital**
    - Subvenciones y financiación
    - Reubicación de personas en riesgo
    - Seguridad física
    - Apoyo jurídico
    - Apoyo psicosocial
    - Incidencia política

> La línea de ayuda actúa como puente. Cuando es necesario, derivamos nuestros casos a las plataformas de redes sociales, ya que tenemos comunicación directa con Facebook y su programa piloto “No sin mi consentimiento”. También derivamos casos al programa piloto de Internet Watch Foundation sobre pornografía infantil en línea y les trasladamos peticiones de retirada de contenido nocivo. A veces apoyamos a denunciantes que presentan una denuncia ante las autoridades en Pakistán. O ponemos en contacto a mujeres y niñas que quieren huir de familias tóxicas o de relaciones abusivas con centros de acogida en Pakistán. Todo esto va más allá de los servicios de seguridad digital y la asistencia jurídica que ofrece la línea de ayuda. <br><br>_Digital Rights Foundation (Haché, 2021)._

> Estamos operando una línea de apoyo en Luchadoras para atender principalmente a mujeres que están viviendo violencia digital en México. Esta iniciativa surge a partir de un incremento en la necesidad detectada en la recepción de solicitudes recibidas en nuestras redes sociales. La línea de apoyo tiene como acciones principales: proporcionar un acompañamiento integral; primeros auxilios psicológicos; detectar necesidades o proporcionar información: alternativas de acción, formas de reporte, contenidos sobre violencia digital, seguridad digital, contactos de posibles redes de apoyo; escalamiento de casos a través de la elaboración y seguimiento de reportes en diversas plataformas; canalización a organizaciones/instituciones especializadas para un acompañamiento y seguimiento óptimo. <br><br> _Luchadoras (Haché, 2021)._

## 1.6 Comunícate con tu público

Definir las formas en las que la LASDSC establecerá la comunicación con su público es un elemento crucial de este marco de trabajo. Tendrán que decidir cómo se pondrán en contacto su público, su disponibilidad y tiempo de respuesta y tener claro cómo se comunicarán con personas que puedan estar emocionalmente traumatizadas.

### Decide cómo podrá ponerse en contacto tu público

Tendrán que identificar los mejores canales de comunicación para que las beneficiarias obtengan apoyo de la línea de ayuda o de la línea de atención.

Entre las cosas que deben tener en cuenta, basándose en el análisis inicial de contexto y el modelo de amenazas, es si las personas beneficiarias pueden necesitar un cifrado de extremo a extremo o un mecanismo de entrada anónimo para empezar. Si este es el caso, piensen en todos los métodos posibles para intercambiar cualquier información sensible a través de un canal seguro.

Recomendamos que establezcan al menos dos formas diferentes de contactar con su línea de atención:

- Un canal accesible para cualquier persona sin conocimientos técnicos, como una dirección de correo electrónico.
- Un canal seguro para las personas que tienen los conocimientos técnicos necesarios para utilizarlo, como un correo electrónico cifrado o una app de mensajería segura como [Signal](https://signal.org/) o [Wire](https://wire.com/).

Garantizar la seguridad de las comunicaciones es importante. Sin embargo, la prioridad es garantizar que la LASDSC sea fácil de localizar. Por eso es importante ofrecer varios canales de comunicación. Multiplicar los canales de comunicación no tiene por qué ser necesariamente un problema siempre que el equipo esté adecuadamente organizado para compartir la información.

A continuación, ofrecemos una lista de todas las herramientas de comunicación que ofrecen los miembros de CiviCERT como posibles métodos para establecer un primer contacto con su LASDSC:

- Formulario web anónimo
- Correo electrónico
- Correo electrónico cifrado con PGP
- Teléfono
- Correo postal
- Signal
- Skype
- Telegram
- Formulario web
- WhatsApp

> Tenemos un correo que es de la línea apoyo. También nos contactan a través de nuestras redes, a través de nuestra página web que se vincula con nuestro correo. También a través de la vinculación de otras organizaciones que reciben los casos y nos solicitan apoyo y a través de nuestra línea telefónica y por WhatsApp. <br><br>_Luchadoras (Haché, 2021)._

También hay que tener en cuenta que algunas de las personas beneficiarias pueden haber pasado por situaciones traumáticas y pueden requerir una escucha activa y empatía, lo que se puede ofrecer mejor a través de una llamada telefónica o una videollamada.

Existen muchas posibilidades de mantener la relación con las beneficiarias y recibir sus comentarios y aportaciones, por ejemplo:

- Foros
- Listas de correo y otros espacios comunitarios
- Reuniones, conferencias, talleres, presentaciones (presenciales y en remoto)
- Boletines informativos
- Redes sociales

### Establece tu disponibilidad y tiempo de respuesta

Los tiempos de respuesta deben comunicarse con claridad a las personas beneficiarias para evitar falsas expectativas y para establecer un Acuerdo de Nivel de Servicio (ANS) adecuado con su público. Responder a tiempo durante la gestión de las incidencias es crucial, tanto para resolver el problema al que se enfrentan como para la reputación de la LASDSC.

La disponibilidad y el tiempo de respuesta de la LASDSC dependerán en gran medida de la plantilla y el horario de trabajo.

A menos que su LASDSC esté disponible las 24 horas del día, tendrán que decidir cómo se pueden notificar los incidentes fuera del horario de oficina. Se puede optar por revisar todos los mensajes entrantes el siguiente día laborable, o puede haber una persona del equipo de guardia para supervisar las solicitudes entrantes y decidir sobre su urgencia.

Para tomar esta decisión, es importante tener en cuenta el contexto: por ejemplo, si la financiación es limitada, es posible que no se pueda pagar a una persona para que trabaje fuera del horario habitual. Estas son consideraciones importantes también para la seguridad psicosocial del equipo. Si, por ejemplo, la LASDSC está gestionada por personas voluntarias, pueden estar dispuestas a aceptar solicitudes a cualquier hora del día o de la noche, pero esto podría conducir muy rápidamente a un desgaste de las más dedicadas.

### Para saber más

- Para profundizar sobre la forma de cuidar el bienestar del equipo de una LASDSC, se puede consultar la sección sobre políticas de cuidados en el [capítulo 2](/es/02-make-a-realistic-plan).

### Define los protocolos y el tono de la conversación

Además de garantizar la confidencialidad de las comunicaciones con las personas beneficiarias, a la hora de responder a las solicitudes de apoyo las LASDSC deben tener siempre presente que estas pueden estar traumatizadas emocionalmente por la agresión que han sufrido.

He aquí algunos consejos sobre cómo establecer la comunicación con una persona que ha sufrido una agresión:

- Registrar la comunicación, comprobar si ya se han registrado comunicaciones similares.
- Utilizar un enfoque respetuoso, con una perspectiva interseccional.
- Poner a la persona solicitante en el centro, desactivando cualquier sentimiento de culpabilidad y aceptando sus sentimientos.
- Evitar la revictimización.
- Aceptar y asumir la decisión que tomen en la situación que están viviendo.
- Es importante tener en cuenta distintos aspectos de contextos diferentes y escuchar los casos que llegan desde una mirada abierta y sin juicios de valor.

> En cuanto a la comunicación, lo principal para nosotras es poner sus necesidades en el centro, tratando de desactivar la culpa, acogiendo sus sentimientos, evitando la revictimización. La comunicación de Luchadoras está pensada para que sientan como si fuera una amiga que responde, que asume y secunda la decisión que toman sobre la situación por la que están pasando. <br><br> _Luchadoras (Entrevista, enero de 2022)._

> Hemos introducido los conceptos de apoyo emocional y empático en nuestra línea de ayuda. Nuestros primeros auxilios psicológicos están basados en tres preguntas: “¿Cómo llevas este estrés?” (para compartir nuestra profunda preocupación por su bienestar); “¿Puedes explicarme el problema?” (para que sean las propias personas las que controlen su relato); y “¿Qué quieres que hagamos por ti? ¿Cuál es tu deseo?” (para construir soluciones de forma conjunta con la persona que busca apoyo). <br><br> _Vita Activa (Haché, 2021)._

### Para saber más

- [Protocolo de Access Now para gestionar casos en los que parece que la persona solicitante está sufriendo paranoia.](https://communitydocs.accessnow.org/356-paranoia_protocol.html)

## 1.7 Define tus políticas

Las políticas de una LASDSC son un conjunto de acuerdos y pautas que organizan el flujo de trabajo y establecen los procedimientos y protocolos estándar. Cada LASDSC precisará de unas políticas que respondan a sus necesidades específicas en función de su misión, tamaño, estructura y servicios. En esta sección se describen las políticas básicas que han adoptado distintas LASDSC, junto con plantillas que se puede utilizar para crear una LASDSC:

- Política de gestión de la información
- Plan de respuesta a incidentes
- Política de verificación
- Código de conducta
- Procedimientos operativos estándar para LASDSC

Una vez elaboradas y puestas en práctica, las políticas deben revisarse de forma periódica para verificar que siguen siendo válidas para la estructura, los procedimientos, las necesidades y las capacidades de las LASDSC con el paso del tiempo.

<a name="information-management"></a>
### Política de gestión de la información

Cada LASDSC requiere una política de gestión y protección de la información que tenga en cuenta los procesos y procedimientos operativos y administrativos internos, así como la legislación y las normas. Por ello, es conveniente contar con la participación de una asesoría jurídica a la hora de elaborar la política de gestión de la información.

Las preguntas más básicas a las que debe responder la política de gestión de la información son:

1. **¿Cómo se "etiqueta" o "clasifica" la información?**

    La mayoría de las LASDSC, así como CiviCERT, clasifican la información basándose en el [_Information Sharing Traffic Light Protocol (TLP)_](https://www.first.org/tlp/) [Protocolo de semáforo]:

    - **TLP:ROJO** = No se puede divulgar, está restringido a las personas que participan en él.

        Las fuentes pueden utilizar TLP:ROJO cuando la información no puede ser utilizada eficazmente por otras partes y podría tener un impacto en la privacidad, la reputación o las operaciones de una parte si se utiliza de forma indebida. Las personas receptoras no deben compartir información TLP:ROJO con ninguna parte fuera del intercambio, reunión o conversación específica en la que fue expuesta originalmente. En el contexto de una reunión, por ejemplo, la información TLP:ROJO se limita a quienes estaban presentes en la misma. En casi todas las circunstancias, la información clasificada como TLP:ROJO debe intercambiarse verbalmente o de forma presencial.

    - **TLP:ÁMBAR** = Divulgación limitada, restringida a las organizaciones participantes.

        Las fuentes pueden utilizar TLP:ÁMBAR cuando la información requiera apoyo para actuar de forma eficaz pero conlleve riesgos para la privacidad, reputación o las operaciones si se comparte fuera de las organizaciones implicadas. Las personas receptoras únicamente pueden compartir información TLP:ÁMBAR con integrantes de su propia organización y con personas beneficiarias que necesiten conocer la información para protegerse a sí mismas o evitar más daños. Las fuentes tienen derecho a especificar otros límites adicionales para el intercambio de la información que deben ser respetados.

    - **TLP:VERDE** = Divulgación limitada, restringida a la comunidad.

        Las fuentes pueden utilizar TLP:VERDE cuando la información sea útil para la concienciación de todas las organizaciones participantes, así como con sus pares dentro de una comunidad o sector más amplio. Las personas o entidad receptoras pueden compartir información TLP:VERDE con sus pares y organizaciones asociadas dentro de su sector o comunidad, pero no a través de canales de acceso público. La información de esta categoría puede difundirse ampliamente dentro de una comunidad concreta. La información TLP:VERDE no puede divulgarse fuera de la comunidad.

    - **TLP:BLANCO**  = La divulgación no está limitada

        Las fuentes pueden utilizar TLP:BLANCO cuando la información suponga un riesgo mínimo o nulo de uso indebido, dentro de las reglas y procedimientos establecidos para su difusión pública. La información TLP:BLANCO puede ser distribuida sin restricciones, sujeta a controles de derechos de autor.

2. **¿Cómo se gestiona y protege la información?**

    Es preciso definir cómo se protegen sus comunicaciones y la información que se almacena en su infraestructura, así como durante cuánto tiempo se conserva la información almacenada en la infraestructura y qué sucede en caso de que se produzca una filtración de datos.

3. **¿Qué medidas se establecen para la divulgación de información, especialmente cuando se transmite información relacionada con incidentes a otros equipos o cuando la solicitan las autoridades policiales?**

4. **¿Existen aspectos jurídicos que deban tenerse en cuenta en relación con el tratamiento de la información?**

5. **¿Su política define cómo se debe proteger y utilizar su infraestructura técnica y sus equipos?**

> Amnesty tiene una política de consentimiento estricta, por lo que no podemos publicar nada sin el consentimiento de la persona beneficiaria, ni siquiera de forma anónima. Tenemos que seguir una política estricta, analizando conjuntamente los riesgos y demás. Y en este punto, si el caso es reactivo y no surge de un proyecto que hayamos iniciado antes, empezamos a pensar en la estrategia de cambio: ¿Qué sabemos? ¿Qué podemos probar? ¿Qué podemos hacer para cambiar esto? De modo que, normalmente, nos reunimos para analizar cuál sería el plan de incidencia, si hay algún margen para llevar a cabo una campaña, si hay que trabajar con el equipo del país, cuál sería el riesgo de publicar la información y cuál sería el beneficio. <br><br _Etienne Maynier, Amnesty Tech (Entrevista de diciembre, 2021)._

> Intentamos publicar todos los datos posibles: indicadores, metodología, etc. Por ejemplo, con Pegasus también publicamos rastros forenses detallados. Cuanto más publiquemos, más posibilidades habrá de que otras personas puedan desarrollar su propia investigación, pero también menos presiones recibiremos para dar más datos, por parte de los gobiernos, por ejemplo. Luego publicamos informes técnicos detallados, de modo que, si un gobierno se dirige a nuestra organización, podemos decir: "Todo es público". Esa es una de las políticas de Amnesty: publicar pruebas para tratar de evitar que nos pidan información privada. <br><br> _Etienne Maynier, Amnesty Tech (Entrevista, diciembre 2021)._

> Consideramos que es importante contar con ciertos protocolos de seguridad. Un protocolo fundamental es que todas las personas que trabajan en la línea de ayuda cumplan un acuerdo de confidencialidad para garantizar la protección de la intimidad y la privacidad y que los datos que guardamos no se puedan identificar con ninguna persona. De este modo, nadie puede entender esos datos, salvo quien los maneja o almacena. <br><br> _Digital Rights Foundation (Haché, 2021)._

[Descarga la plantilla de la Política de gestión de la información](/es/06-information-management-policy-template).

<a name="incident-response"></a>
### Plan de respuesta a incidentes

Los procedimientos de gestión de incidentes descritos en el plan de respuesta a incidentes son una de las medidas clave que deben establecerse, ya que permitirán que todo el mundo entienda qué se espera de cada quién. Describan los distintos tipos de incidentes distinguiendo entre niveles de impacto y establezcan qué pasos debe seguir su equipo. Definan a qué integrantes del equipo hay que dirigirse si surge un incidente. Enumeren las distintas posibilidades para poder elevar o derivar los casos y organícenlas con todo el equipo. Es decir, asegúrense de que las expectativas se gestionan adecuadamente dentro de la organización.

Para contar con un planteamiento documentado y coordinado para responder cuestiones de seguridad digital, el plan de respuesta a incidentes debe incluir los siguientes aspectos:

- Cómo se reciben y asignan los casos.
- Cómo se priorizan los casos.
- El flujo de trabajo de admisión, verificación y escalada de la LASDSC.
- El ciclo de respuesta a incidentes de la LASDSC.
- Cómo se cierran los casos.

[Descarga la plantilla del Plan de respuesta a incidentes](/es//06-incident-response-plan-template).

<a name="vetting"></a>
### Política de verificación

Asegurarse de que una LASDSC apoya a su verdadero público es clave para su reputación, por lo que muchas líneas de ayuda y líneas de atención verifican que las personas solicitantes son realmente quienes dicen ser antes de iniciar el proceso de gestión de incidentes.

Para definir este proceso de verificación, una buena práctica es desarrollar una política de verificación que describa las metas del proceso, así como los pasos que se deben tomar para verificar a la nueva persona beneficiaria, obtener su consentimiento informado sobre el proceso y proteger su privacidad en la medida de lo posible.

> Así que, básicamente, empezamos con gente que nos contacta por alguna razón. A menudo porque han recibido un sms o un correo electrónico extraño, algo así. Luego intentamos recabar información sobre el contexto, investigar el correo electrónico o el sms que han recibido. Pero primero tenemos que asegurarnos de que son personas defensoras de derechos humanos y eso puede suponer un reto, pues la definición de defensora de DDHH de Amnistía puede ser a veces muy estrecha y depende del equipo de país. Amnistía Internacional trabaja a dos niveles: los equipos de país y los equipos temáticos, así que, por ejemplo, en mi equipo trabajamos en el campo de la tecnología, pero cada vez que trabajamos en un país tenemos que trabajar con el equipo del país. Cuando recibimos una solicitud de ayuda tenemos que comprobar quién es la persona que la realiza y si es defensora de derechos humanos, cosa compleja porque a veces tenemos que preguntar a la gente quién son y luego comprobar con el equipo del país si efectivamente son defensoras de derechos humanos y asegurarnos de que contamos con su aprobación. Y, si es el caso (a menudo con periodistas, por ejemplo, es bastante fácil) hacemos la investigación y tratamos de entender qué pasó. <br><br> _Etienne Maynier, Amnesty Tech (Entrevista, diciembre de 2021)._

Descarga la [plantilla de la Política de verificación](/es/06-vetting-process-template).

<a name="cop"></a>
### Código de conducta

Un Código de conducta es un documento que establece cómo debe comportarse el personal de la LASDSC cuando se relaciona con las personas beneficiarias. Estos son algunos de los elementos más importantes de un código de conducta eficaz:

- Descripciones específicas de conductas comunes pero inaceptables (comentarios sexistas, etc.).
- Instrucciones para presentar una denuncia con información de contacto.
- Información sobre cómo puede aplicarse.
- Una distinción clara entre el comportamiento inaceptable (que puede ser denunciado siguiendo las instrucciones de denuncia y puede tener consecuencias graves para quien lo ha perpetrado) y las pautas comunitarias, como la resolución general de conflictos.

Los códigos de conducta que carecen de alguno de estos elementos no suelen producir el efecto que se pretende.

Descarga la [plantilla del Código de conducta](/es/06-code-of-practice-template).

Otros generadores y plantillas de códigos de conducta:

- [Código de conducta de Berlín](https://berlincodeofconduct.org/) (multilingüe).
- [Generador de códigos de conducta](https://github.com/sindresorhus/conduct).
- basado en el [Contributor Covenant](https://www.contributor-covenant.org/version/2/0/code_of_conduct/) ([multilingüe](https://www.contributor-covenant.org/translations/))
- [Repositorio de Mozilla sobre Diversidad e Inclusión de Código Abierto](https://github.com/mozilla/inclusion), que contiene recursos, modelos y normas para hacer que los proyectos abiertos sean más inclusivos.

<a name="sop"></a>
### Procedimientos operativos estándar para operadores de LASDSC

Es importante que una LASDSC cuente con Procedimientos Operativos Estándar (POE), para definir, por ejemplo, cómo se deben responder las solicitudes, comunicarse con las personas beneficiarias, garantizar la confidencialidad y realizar derivaciones, pero también para asegurar que quienes atiendan las llamadas saben cómo comportarse en situaciones de tensión y afrontar el estrés.

> Hay distintas buenas prácticas que hemos ido desarrollando a lo largo del tiempo. Empezando por aprender a cuidar de quienes acuden a nuestros proyectos y también del equipo. Sabemos que esto puede resultar agotador, pero tenemos que estar bien para acompañar mejor a otras personas. <br><br> _Tecnoresistencias (Haché, 2021)._

### Para saber más

- Access Now Digital Security Helpline Public Documentation incluye una [sección sobre sus procedimientos operativos estándar](https://communitydocs.accessnow.org/tag_helpline_procedures.html).
