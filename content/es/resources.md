---
layout: page
title: "Recursos adicionales"
date: 2022-10-13
categories: about
lang: es
order: 4
---

# Recursos adicionales

## Plantillas

- [Código de conducta](/es/06-code-of-practice-template)
- [Marco de trabajo](/es//06-framework-template)
- [Plan de respuesta a incidentes](/es//06-incident-response-plan-template)
- [Política de gestión de la información](/es/06-information-management-policy-template)
- [Política de verificación](/es/06-vetting-process-template)

## Descarga CuidaTech

- [Descarga la guía en formato PDF](/Guia_Cuidatech_es.pdf)

## Contribuye a esta guía

- [Repositorio Gitlab para crear _issues_](https://gitlab.com/rarenet/tech-care-g)
- Escríbenos un correo electrónico a tech-care@digitaldefenders.org
