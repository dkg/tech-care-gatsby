module.exports = {
  siteMetadata: {
    siteUrl: `https://tech-care.civicert.org`,
  },
  plugins: [
    "gatsby-plugin-sass",
    "gatsby-plugin-image",
    "gatsby-plugin-react-helmet",
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        icon: "src/images/logo.png",
      },
    },
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          "gatsby-remark-autolink-headers",
          "gatsby-remark-external-links",
          {
            resolve: `gatsby-remark-classes`,
            options: {
              classMap: {
                blockquote: "quote pullout float",
                "heading[depth=1]": "f1",
                "heading[depth=2]": "f2",
                "heading[depth=3]": "f3",
                "heading[depth=4]": "f4",
                "heading[depth=5]": "f5",
              },
            },
          },
        ],
      },
    },
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "pages",
        path: "./src/pages/",
      },
      __key: "pages",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "content",
        path: "./content",
      },
      __key: "content",
    },
    {
      resolve: `gatsby-plugin-react-i18next`,
      options: {
        localeJsonSourceName: "content",
        languages: ["en", "es"],
        defaultLanguage: "en",
        siteUrl: "https://tech-care.civicert.org",
        trailingSlash: "always",
        i18nextOptions: {
          interpolation: {
            escapeValue: false,
          },
          keySeparator: false,
          nsSeparator: false,
        },
        guides: [
          {
            matchPath: "/:lang?/guide/:uid",
            getLanguageFromPath: true,
          },
        ],
      },
    },
    {
      resolve: "gatsby-plugin-matomo",
      options: {
        siteId: "4",
        matomoUrl: "https://trends.digitaldefenders.org",
        siteUrl: "https://tech-care.civicert.org",
      },
    },
    `gatsby-plugin-smoothscroll`,
    "gatsby-plugin-page-progress",
  ],
};
