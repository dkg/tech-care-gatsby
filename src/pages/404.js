import * as React from "react";
import { graphql } from "gatsby";

const NotFoundPage = () => {
  return (
    <main>
      <h1>Page not found</h1>
    </main>
  );
};

export default NotFoundPage;

export const query = graphql`
  query($language: String!) {
    locales: allLocale(filter: { language: { eq: $language } }) {
      edges {
        node {
          ns
          data
          language
        }
      }
    }
  }
`;
