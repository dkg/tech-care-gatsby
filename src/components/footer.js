import React from "react";
import { useI18next } from "gatsby-plugin-react-i18next";

import civicert_logo from "../images/civicert_logo.png";

export default function Footer() {
  const { t } = useI18next();
  return (
    <footer className="w-100 center ph6-l ph3 pt4 pb flex flex-column">
      <div className="columns center w-70-l w-90 flex flex-column flex-row-l justify-between f4 items-end">
        <div className="column w-50-ns w-90 sponsor">
          <a
            href="https://civicert.org"
            target="_blank"
            rel="noreferrer nofollow"
          >
            <img
              src={civicert_logo}
              alt="Civicert"
              style={{ maxWidth: "150px" }}
            />
            <div className="pt2-l">{t("footer_civicert")}</div>
          </a>
        </div>
        <div className="column w-25-l w-90 pt4 p0-l">
          <a
            href={`/${t("footer_download_filename")}`}
            target="_blank"
            rel="noreferrer nofollow"
            alt={t("footer_download")}
          >
            {t("footer_download")}
          </a>
        </div>
        <div className="column w-25-l w-90 pt4 p0-l">
          <a
            href="https://gitlab.com/rarenet/tech-care-gatsby"
            target="_blank"
            rel="noreferrer nofollow"
            alt={t("footer_gitlab")}
          >
            {t("footer_gitlab")}
          </a>
        </div>
      </div>
      <div className="w-90 w-70-ns center pt5 flex flex-column justify-around items-center">
        <a
          rel="license noreferrer nofollow"
          href={t("footer_license_link")}
          target="_blank"
        >
          <img
            alt="Creative Commons Licence"
            style={{ borderWidth: 0 }}
            src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png"
          />
        </a>
        <a
          target="_blank"
          className=" flex link dark-gray"
          rel="license noreferrer nofollow"
          href={t("footer_license_link")}
        >
          {t("footer_license")}
        </a>
        .
      </div>
    </footer>
  );
}
