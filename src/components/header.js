import React, { useState } from "react";
import _ from "lodash";
import { useStaticQuery, graphql } from "gatsby";
import { useI18next, Link } from "gatsby-plugin-react-i18next";
// import scrollTo from "gatsby-plugin-smoothscroll";
import { FiChevronDown } from "react-icons/fi";

import logo from "../images/logo.png";
const Header = () => {
  const data = useStaticQuery(graphql`
    query {
      allLocale {
        edges {
          node {
            language
            data
          }
        }
      }
      aboutPages: allMarkdownRemark(
        filter: { frontmatter: { categories: { eq: "about" } } }
        sort: { fields: frontmatter___order }
      ) {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              lang
              title
            }
          }
        }
      }
      allMarkdownRemark: allMarkdownRemark(
        sort: { fields: frontmatter___order }
      ) {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              categories
              lang
              title
            }
            headings(depth: h2) {
              value
            }
          }
        }
      }
    }
  `);

  const { originalPath, language, t } = useI18next();
  const current_language = _.filter(
    data.allLocale.edges,
    (node) => node.node.language === language
  );
  const other_languages = _.filter(
    data.allLocale.edges,
    (node) => node.node.language !== language
  );
  const nav_items = _.filter(
    data.allMarkdownRemark.edges,
    (node) =>
      node.node.frontmatter.lang === language &&
      node.node.frontmatter.categories === "guide"
  );
  const aboutPages = _.filter(
    data.aboutPages.edges,
    (node) => node.node.frontmatter.lang === language
  );
  const [sidebarOpen, toggleSidebarOpen] = useState(false);
  return (
    <>
      <div id="top" />
      <header className="w-100 center flex justify-between ph6-l pv3-l ph4-m pv3-m pv2 ph3 bb b--custom-light-pink fixed-s">
        <Link className="brand flex" to="/" language={language}>
          <img
            src={logo}
            alt={t("siteTitle")}
            style={{ width: "60px" }}
            className="mt1 mt0-ns"
          />
          <span className="f2-l f3-m f4 b custom-white i">
            {t("siteTitle")}
          </span>
        </Link>
        <div
          className={`hamburger dn-l mt1 ${sidebarOpen ? "open" : "close"}`}
          onClick={() => toggleSidebarOpen(!sidebarOpen)}
        >
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div
          className={`flex-l justify-end items-baseline flex-none sidebar z-999 ${
            sidebarOpen ? "open" : "close"
          }`}
        >
          <div className="contents pointer ml3">
            <div className="flex items-bottom arrow pa3 f4">
              {t("about")}
              <FiChevronDown className="f3 ml2" />
            </div>
            <div className="w-100 contents-dropdown bg-custom-purple dn-l absolute ph6 pv3 left-0 right-0  z-999">
              {aboutPages.map((aboutPage, aIdx) => (
                <div
                  className={`${aboutPage.node.fields.slug} guide-item flex flex-column pr3 w-25 ph4 pv3 w-25-l`}
                  key={aIdx}
                >
                  <Link
                    className="mr4 custom-white f4"
                    to={`/${aboutPage.node.fields.slug}`}
                    language={language}
                  >
                    {aboutPage.node.frontmatter.title}
                  </Link>
                </div>
              ))}
            </div>
          </div>
          <div className="contents pointer ml3 mr5">
            <div className="flex items-bottom arrow pa3 f4">
              {t("contents")}
              <FiChevronDown className="f3 ml2" />
            </div>
            <div className="w-100 contents-dropdown bg-custom-purple dn-l absolute ph6 pv3 left-0 right-0  z-999">
              {nav_items.map((item, idx) => {
                const headings = _.map(item.node.headings, "value");
                return (
                  <div
                    key={idx}
                    className={`${item.node.fields.slug} guide-item flex flex-column pr3 w-25 ph4 pv3`}
                  >
                    <div className="flex flex-column-l items-baseline">
                      <div className="f2 b mb2 custom-white">{idx + 1}.</div>
                      <Link
                        to={`/${item.node.fields.slug}`}
                        language={language}
                        className="mb3 f4 custom-white b"
                      >
                        {item.node.frontmatter.title}
                      </Link>
                    </div>
                    <div className="flex flex-column">
                      {_.map(headings, (heading, hIdx) => {
                        return (
                          <Link
                            key={hIdx}
                            to={`${
                              item.node.fields.slug === originalPath
                                ? ""
                                : `/${item.node.fields.slug}`
                            }#${_.kebabCase(_.replace(heading, ".", ""))}`}
                            language={language}
                            className=" mb2 custom-white"
                          >
                            {heading}
                          </Link>
                        );
                      })}
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <nav className="languages relative dib pointer-l ml4">
            <span className="flex items-bottom arrow">
              {JSON.parse(current_language[0].node.data).name}
              <FiChevronDown className="f3 ml2" />
            </span>
            <div className="languages-choices absolute-l dn-l flex flex-column">
              {_.map(other_languages, (node, lIdx) => {
                return (
                  <Link
                    key={lIdx}
                    className={`flex ttu ${node.node.language}`}
                    to={originalPath}
                    language={node.node.language}
                  >
                    {JSON.parse(node.node.data).name}
                    <FiChevronDown className="f3 ml1" />
                  </Link>
                );
              })}
            </div>
          </nav>
        </div>
      </header>
    </>
  );
};

export default Header;
