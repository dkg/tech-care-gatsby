import React from "react";
import Header from "./header";
import Footer from "./footer";

import "@fontsource/noto-sans";
import "../sass/main.scss";

export default function Layout({ children }) {
  return (
    <>
      <Header />
      <main className="w-70-l w-90-m w-100 center">{children}</main>
      <Footer />
    </>
  );
}
